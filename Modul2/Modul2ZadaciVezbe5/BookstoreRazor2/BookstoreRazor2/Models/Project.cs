﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookstoreRazor2.Models
{
    public class Project
    {
        public static List<Genre> Genres = new List<Genre>()
            {
                new Genre { Id = 1, Name = "Science" },
                new Genre { Id = 2, Name = "Comedy" },
                new Genre { Id = 3, Name = "Horror" }
            };

        public static List<Book> Books = new List<Book>()
            {
              new Book {Id=1 , Name = "Moja knjiga", Price = 76.78 , Genre = Genres[0], IsDeleted = false },
              new Book {Id=2 , Name = "Tvoja knjiga", Price = 86.78 , Genre = Genres[1], IsDeleted = false },
              new Book {Id=3 , Name = "Nasa knjiga", Price = 96.78 , Genre = Genres[2], IsDeleted = false }
            };
    }
}