﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookstoreRazor2.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public Genre  Genre { get; set; }
        public bool IsDeleted { get; set; }
    }
}