﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;

namespace BookstoreRazor2.Controllers
{
    public class GenreController : Controller
    {
        // GET: Genre
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(BookstoreBookGenreViewModel bookstoreBookGenreViewModel)
        {
            Genre genre = new Genre();
            int poslednjiGenre = Project.Genres.Count();

            genre.Id = poslednjiGenre + 1;
            genre.Name = bookstoreBookGenreViewModel.Genre.Name;
            Project.Genres.Add(genre);

            return RedirectToAction("AddBook", "Bookstore");
        }

        public ActionResult Delete()
        {
            var vm = new BookstoreBookGenreViewModel();
            vm.GenresList = Project.Genres;
            return View(vm);
        }


        // OVO NISAM USPEO - URADIO MALO PRE
        [HttpPost]
        public ActionResult Delete(int SelectedGenreId)
        {
            List<Genre> listGenres = Project.Genres;

            for (int i = 0; i < listGenres.Count; i++)
            {

                if (SelectedGenreId == listGenres[i].Id)
                {
                    listGenres.RemoveAt(i);
                }
                
            }
            
            return RedirectToAction("AddBook", "Bookstore");
        }
    }
}