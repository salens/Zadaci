﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;

namespace BookstoreRazor2.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            Book foundedBook = null;
            var vm = new BookstoreBookGenreViewModel();

            foreach (var item in Project.Books)
            {
                if(item.Id == id)
                {
                    foundedBook = item;
                }
            }

            vm.Book = foundedBook;
            vm.GenresList = Project.Genres;
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(BookstoreBookGenreViewModel vm)
        {
            Genre newGenre = null;
            foreach (var genre in Project.Genres)
            {
                if(genre.Id == vm.SelectedGenreId)
                {
                    newGenre = genre;
                }

            }

            foreach (var book in Project.Books)
            {
                if(book.Id == vm.Book.Id)
                {
                    book.Name = vm.Book.Name;
                    book.Price = vm.Book.Price;
                    book.Genre = newGenre;
                }
            }

            return RedirectToAction("List", "Bookstore");
        }


        public ActionResult Delete(int id)
        {
            Book book;

            foreach (var item in Project.Books)
            {
                if(id == item.Id)
                {
                    item.IsDeleted = true;
                    book = item;
                }
            }

            return RedirectToAction("ListDeletedBooks");
        }


        public ActionResult ListDeletedBooks()
        {
            return View(Project.Books);
        }
    }
}