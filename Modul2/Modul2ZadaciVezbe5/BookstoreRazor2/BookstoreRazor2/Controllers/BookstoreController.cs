﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;

namespace BookstoreRazor2.Controllers
{
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            return RedirectToAction("AddBook");
        }

        public ActionResult AddBook()
        {
            BookstoreBookGenreViewModel bookstoreBookGenreViewModel = new BookstoreBookGenreViewModel();
            bookstoreBookGenreViewModel.GenresList = Project.Genres;
            return View(bookstoreBookGenreViewModel);
        }

        [HttpPost]
        public ActionResult AddBook(BookstoreBookGenreViewModel bookstoreBookGenreViewModel)
        {

            Book book = bookstoreBookGenreViewModel.Book;
            Genre genre;

            foreach (var genreItem in Project.Genres)
            {
                if(genreItem.Id == bookstoreBookGenreViewModel.SelectedGenreId)
                {
                    genre = genreItem;
                    book.Genre = genre;
                }
            }

            book.Id = GetHashCode();
            Project.Books.Add(book);

            return RedirectToAction("List");
        }

        public ActionResult List(string sortOrder, string searchString, string FooBarDropDown)
        {


            ViewBag.NazivSortParm = String.IsNullOrEmpty(sortOrder) ? "Naziv" : "";
            ViewBag.CenaSortParm = String.IsNullOrEmpty(sortOrder) ? "Cena" : "";

            var book = from s in Project.Books select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                if (!String.IsNullOrEmpty(FooBarDropDown))
                {
                    switch (FooBarDropDown)
                    {
                        case "Naziv":
                            book = book.Where(s => s.Name.Contains(searchString));
                            break;

                        case "Cena":
                            book = book.Where(s => s.Price.Equals(Convert.ToDouble(searchString)));
                            break;

                    }
                }


            }

            switch (sortOrder)
            {
                case "Naziv":
                    book = book.OrderBy(s => s.Name);
                    break;
                case "Cena":
                    book = book.OrderBy(s => s.Price);
                    break;
                default:
                    book = book.OrderByDescending(s => s.Name);
                    break;
            }

            return View(book);
        }

        public ActionResult ListByGenre()
        {

            var vm = new BookstoreBookGenreViewModel();
            vm.booksList = Project.Books;
            vm.GenresList = Project.Genres;
            return View(vm);
        }
        [HttpPost]
        public ActionResult ListByGenre(int SelectedGenreId)
        {
            var vm = new BookstoreBookGenreViewModel();
            var book = from s in Project.Books.Where(s => s.Genre.Id.Equals(SelectedGenreId)) select s;
            vm.booksList = book.ToList();
            vm.GenresList = Project.Genres;
            return View(vm);
        }
    }
}