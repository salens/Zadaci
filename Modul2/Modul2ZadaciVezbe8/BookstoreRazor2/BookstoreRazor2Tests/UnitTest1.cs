﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BookstoreRazor2.Controllers;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BookstoreRazor2.Repository.Interfaces;
using BookstoreRazor2.Repository;
using BookstoreRazor2.Models;
namespace BookstoreRazor2Tests
{
    [TestClass]
    public class UnitTest1
    {   
        private IBookRepository bookRepo = new BookRepo();
        private IGenreRepository genreRepo = new GenreRepo();

        [TestMethod]
        public void GETCreateNewBookTest()
        {
            Book book = new Book();

            book.Name = "Test1";
            book.Price = 34.45;
            book.Genre.Id = 2007;
            book.IsDeleted = false;
     
            Assert.IsTrue(bookRepo.Create(book));
        }
    }
}
