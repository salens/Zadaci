﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;
using BookstoreRazor2.Repository;
using BookstoreRazor2.Repository.Interfaces;
using System.Configuration;
using System.Data.SqlClient;

namespace BookstoreRazor2.Controllers
{
    public class GenreController : Controller
    {

        private IGenreRepository genreRepo = new GenreRepo();

        // GET: Genre
        public ActionResult Index()
        {
            var genres = genreRepo.GetAll();

            return View(genres);
        }

        public ActionResult Create()
        {
            return View(new Genre());
        }
        [HttpPost]
        public ActionResult Create(Genre genre)
        {
            if (ModelState.IsValid)
            {
                if (genreRepo.Create(genre))
                {
                    return RedirectToAction("ListaKnjiga", "Knjizara");   // nakon uspesnog dodavanja daj listing svih knjiga
                }
            }
            return View(genre);
        }

        // GET: ProductCategory/Details/5
        public ActionResult Details(int id)
        {
            var genreDetail = genreRepo.GetById(id);

            return View(genreDetail);   // TODO: 0. dodati link koji vodi ka detaljnom prikazu za svaki od product kategorija u listingu kategorija 
        }

        // GET: Genre/Edit/5
        public ActionResult Edit(int id)
        {
            var genre = genreRepo.GetById(id);

            return View(genre);
        }


        // POST: Genre/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                int exisingId = int.Parse(collection["Id"]);
                string newName = collection["Name"];

                var updatedGenre = new Genre()
                {
                    Id = exisingId,
                    Name = newName
                };

                genreRepo.Update(updatedGenre);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        public ActionResult Delete(int id)
        {
            var genre = genreRepo.GetById(id);
            return View(genre);
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                genreRepo.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}