﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BookstoreRazor2.Models
{
    public class Genre
    {
        public int Id { get; set; }

        [Required] //Ime je obavezan parametar
        [RegularExpression("[A-Z][A-Za-z]*", ErrorMessage = "First letter {0} must be capital!")] //Ime mora da počne velikim slovom
        [StringLength(25, MinimumLength = 5)] //Minimum 5 karaktera, a Maksimum 25
        [Display(Name = "MyGenreName")]
        public string Name { get; set; }
        
    }
}
