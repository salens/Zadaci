﻿using BookstoreRazor2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BookstoreRazor2.Models
{
    public class Project
    {


        static public List<Genre> Genres()
        {
            string query = "SELECT * FROM Genre";  // slektuj mi sve proizvode i njima odgovarajuce kategorije
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    dadapter.Fill(ds, "Genre");
                    dt = ds.Tables["Genre"];
                    con.Close();
                }
            }

            List<Genre> genres = new List<Genre>();

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
            
                int genreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string genreName = dataRow["GenreName"].ToString();
                
                genres.Add(new Genre() { Id = genreId, Name = genreName });

            }

            return genres;
        }



      static public List<Book> Books()
        {
            string query = "SELECT * FROM Book p INNER JOIN Genre pc ON p.BookGenreId = pc.GenreId";  // slektuj mi sve proizvode i njima odgovarajuce kategorije
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }
            }

            List<Book> books = new List<Book>();

            Genre genre;
            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                genre = new Genre();

                int bookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string bookName = dataRow["BookName"].ToString();
                double bookPrice = double.Parse(dataRow["BookPrice"].ToString());
                genre.Name = dataRow["GenreName"].ToString();
                genre.Id = int.Parse(dataRow["GenreId"].ToString());
                bool bookIsDeleted = bool.Parse(dataRow["BookIsDeleted"].ToString());
                books.Add(new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genre, IsDeleted = bookIsDeleted });

            }

            return books;
        }
    }
}