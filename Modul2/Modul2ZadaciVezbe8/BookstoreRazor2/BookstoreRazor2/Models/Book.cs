﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BookstoreRazor2.Models
{
    public class Book
    {
        public int Id { get; set; }

        [Required] //Ime je obavezan parametar
        [StringLength(20, MinimumLength = 3)] //Minimum 3 karaktera, a Maksimum 20
        [RegularExpression("[A-Z][A-Za-z]*")] //Ime mora da počne velikim slovom
        [Display(Name = "MyBooktName")]
        public string Name { get; set; }

        [Required] //Ime je obavezan parametar
        [Range(1, 1000)]
        public double Price { get; set; }
        public Genre  Genre { get; set; }
        public bool IsDeleted { get; set; }
    }
}