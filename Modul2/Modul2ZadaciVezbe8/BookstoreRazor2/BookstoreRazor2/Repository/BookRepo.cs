﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using BookstoreRazor2.Models;
using BookstoreRazor2.Repository.Interfaces;
namespace BookstoreRazor2.Repository
{
    public class BookRepo : IBookRepository
    {

        private GenreRepo genreRepo = new GenreRepo();
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ToString();
            con = new SqlConnection(constr);
        }

        public bool Create(Book book)
        {
            try
            {
                string query = "INSERT INTO Book (BookGenreId,BookName,BookPrice,BookIsDeleted) VALUES (@BookGenreId,@BookName,@BookPrice,@BookIsDeleted);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();   // inicijaizuj novu konekciju


                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@BookGenreId", book.Genre.Id);
                    cmd.Parameters.AddWithValue("@BookName", book.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", book.Price);
                    cmd.Parameters.AddWithValue("@BookIsDeleted", book.IsDeleted);

                    con.Open();                                         // otvori konekciju
                    var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju

                    if (newFormedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }                                              // zatvori konekciju
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa nove knjige. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }
        }

        public void Delete(int id)
        {
            string query = "UPDATE Book SET BookIsDeleted = @BookIsDeleted WHERE BookId = @BookId";

            Connection();   // inicijaizuj novu konekciju

            using (SqlCommand cmd = con.CreateCommand())
            {

                cmd.Parameters.AddWithValue("@BookId", id);
                cmd.Parameters.AddWithValue("@BookIsDeleted", true);
                cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                con.Open();
                var a = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                con.Close();                                                        // zatvori konekciju
            }
        }

        public IEnumerable<Book> GetAll()
        {

            List<Book> books = new List<Book>();

            try
            {
                string query = "SELECT * FROM Book;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Book"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Book"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }

                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {

                    int bookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string bookName = dataRow["BookName"].ToString();
                    double bookPrice = double.Parse(dataRow["BookPrice"].ToString());
                    int genreId = int.Parse(dataRow["BookGenreId"].ToString());
                    bool bookIsDeleted = bool.Parse(dataRow["BookIsDeleted"].ToString());
                    books.Add(new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genreRepo.GetById(genreId), IsDeleted = bookIsDeleted });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja knjiga. {ex.StackTrace}");
                throw ex;
            }

            return books;
        }

        public IEnumerable<Book> GetAllByGenre(int selectedGenreId)
        {
            List<Book> books = new List<Book>();

            try
            {
                string query = "SELECT * FROM Book where BookGenreId = @selectedGenreId;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("@selectedGenreId", selectedGenreId);
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Book"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Book"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }

                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {

                    int bookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string bookName = dataRow["BookName"].ToString();
                    double bookPrice = double.Parse(dataRow["BookPrice"].ToString());
                    bool bookIsDeleted = bool.Parse(dataRow["BookIsDeleted"].ToString());
                    books.Add(new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genreRepo.GetById(selectedGenreId), IsDeleted = bookIsDeleted });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja knjiga. {ex.StackTrace}");
                throw ex;
            }

            return books;
        }

        public Book GetById(int id)
        {
            Book book = null;

            try
            {
                string query = "SELECT * FROM Book pc WHERE pc.BookId = @BookId;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Book"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Book"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }


                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int bookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string bookName = dataRow["BookName"].ToString();
                    double bookPrice = double.Parse(dataRow["BookPrice"].ToString());
                    int genreId = int.Parse(dataRow["BookGenreId"].ToString());
                    bool bookIsDeleted = bool.Parse(dataRow["BookIsDeleted"].ToString());

                    book = new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genreRepo.GetById(genreId), IsDeleted = bookIsDeleted };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja knjige sa id-om: {book.Id}. {ex.StackTrace}");
                throw ex;
            }

            return book;
        }

        public void Update(Book book)
        {
            try
            {

                string query = "UPDATE Book SET BookGenreId = @BookGenreId, BookName = @BookName, BookPrice = @BookPrice, BookIsDeleted = @BookIsDeleted WHERE BookId = @BookId";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {

                    cmd.Parameters.AddWithValue("@BookId", book.Id);
                    cmd.Parameters.AddWithValue("@BookName", book.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", book.Price);
                    cmd.Parameters.AddWithValue("@BookGenreId", book.Genre.Id);
                    cmd.Parameters.AddWithValue("@BookIsDeleted", false);
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    con.Open();
                    var a = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                                        // zatvori konekciju
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom updateovanja zanra. {ex.StackTrace}");
                throw ex;
            }


        }

    }
}