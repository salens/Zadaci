﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookstoreRazor2.Models;
namespace BookstoreRazor2.Repository.Interfaces
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetAll();
        IEnumerable<Book> GetAllByGenre(int id);
        Book GetById(int id);
        bool Create(Book book);
        void Update(Book book);
        void Delete(int id);
    }
}
