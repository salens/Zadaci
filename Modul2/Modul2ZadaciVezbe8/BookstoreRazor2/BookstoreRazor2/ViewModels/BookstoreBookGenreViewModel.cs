﻿using BookstoreRazor2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookstoreRazor2.ViewModels
{
    public class BookstoreBookGenreViewModel
    {
        public Book Book { get; set; }
        public Bookstore Bookstore { get; set; }
        public Genre Genre { get; set; }
        public List<Book> booksList { get; set; }
        public IEnumerable<Genre> GenresList { get; set; }
        public int SelectedGenreId { get; set; }
    }
}