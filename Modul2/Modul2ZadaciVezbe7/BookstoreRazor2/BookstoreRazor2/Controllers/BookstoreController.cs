﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookstoreRazor2.ViewModels;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BookstoreRazor2.Models;
using BookstoreRazor2.Repository;
using BookstoreRazor2.Repository.Interfaces;
namespace BookstoreRazor2.Controllers
{
    [RoutePrefix("Knjizara")]
    public class BookstoreController : Controller
    {
        private IBookRepository bookRepo = new BookRepo();
        private IGenreRepository genreRepo = new GenreRepo();

        // GET: Bookstore
        public ActionResult Index()
        {
            return RedirectToAction("AddBook");
        }

        [Route("Napravi")]
        public ActionResult AddBook()
        {
            var vm = new BookstoreBookGenreViewModel();
            vm.GenresList = genreRepo.GetAll().ToList();
            return View(vm);
        }

        [Route("Napravi")]
        [HttpPost]
        public ActionResult AddBook(BookstoreBookGenreViewModel vm)
        {

            try
            {
                var newBook = vm.Book;
                var bookGenre = genreRepo.GetById(vm.SelectedGenreId);

                newBook.Genre = bookGenre;
                bookRepo.Create(newBook);

                return RedirectToAction("ListaKnjiga", "Knjizara");  // izlistaj ponovo spisak svih
            }
            catch
            {
                return View();
            }

        }
        [Route("ListaKnjiga")]
        public ActionResult List(string sortOrder, string searchString, string FooBarDropDown)
        {
            var books = bookRepo.GetAll().ToList();

            ViewBag.NazivSortParm = String.IsNullOrEmpty(sortOrder) ? "Naziv" : "";
            ViewBag.CenaSortParm = String.IsNullOrEmpty(sortOrder) ? "Cena" : "";

            var book = from s in books select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                if (!String.IsNullOrEmpty(FooBarDropDown))
                {
                    switch (FooBarDropDown)
                    {
                        case "Naziv":
                            book = book.Where(s => s.Name.Contains(searchString));
                            break;

                        case "Cena":
                            book = book.Where(s => s.Price.Equals(Convert.ToDouble(searchString)));
                            break;

                    }
                }

            }

            switch (sortOrder)
            {
                case "Naziv":
                    book = book.OrderBy(s => s.Name);
                    break;
                case "Cena":
                    book = book.OrderByDescending(s => s.Price);
                    break;
                default:
                    book = book.OrderByDescending(s => s.Name);
                    break;
            }

            return View(book);

        }

        public ActionResult ListByGenre()
        {

            var vm = new BookstoreBookGenreViewModel();
            vm.booksList = Project.Books();
            vm.GenresList = Project.Genres();
            return View(vm);
        }

        [HttpPost]
        public ActionResult ListByGenre(int SelectedGenreId)
        {
            var vm = new BookstoreBookGenreViewModel();

            var books = bookRepo.GetAllByGenre(SelectedGenreId).ToList();
            vm.booksList = books;
            vm.GenresList = Project.Genres();
            return View(vm);
        }
    }
}