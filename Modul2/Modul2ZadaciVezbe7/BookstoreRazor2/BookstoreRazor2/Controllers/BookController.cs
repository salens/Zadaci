﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;
using BookstoreRazor2.Repository;
using BookstoreRazor2.Repository.Interfaces;
namespace BookstoreRazor2.Controllers
{
    public class BookController : Controller
    {
        private IBookRepository bookRepo = new BookRepo();
        private IGenreRepository genreRepo = new GenreRepo();

        // GET: Book
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            Book foundedBook = null;
            var vm = new BookstoreBookGenreViewModel();

            foreach (var item in bookRepo.GetAll())
            {
                if (item.Id == id)
                {
                    foundedBook = item;
                }
            }

            vm.Book = foundedBook;
            vm.GenresList = genreRepo.GetAll().ToList();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
           
            try
            {
                int exisingId = int.Parse(collection["Book.Id"]);
                string newName = collection["Book.Name"];
                double newPrice = double.Parse(collection["Book.Price"]);
                int genreId = int.Parse(collection["SelectedGenreId"]);
                var updatedBook = new Book()
                {
                    Id = exisingId,
                    Name = newName,
                    Price = newPrice,
                    Genre = genreRepo.GetById(genreId)
                
                };

                bookRepo.Update(updatedBook);

                return RedirectToAction("List", "Bookstore");
            }
            catch
            {
                return View("Error");
            }
          
        }

        public ActionResult Delete(int id)
        {
            var book = bookRepo.GetById(id);
            return View(book);
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                bookRepo.Delete(id);

                return RedirectToAction("ListDeletedBooks");
            }
            catch
            {
                return View("Error");
            }

        }

        public ActionResult ListDeletedBooks()
        {
            var books = bookRepo.GetAll().ToList();
            return View(books);
        }
    }
}