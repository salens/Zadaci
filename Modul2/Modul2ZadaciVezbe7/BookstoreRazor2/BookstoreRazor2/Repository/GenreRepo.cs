﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using BookstoreRazor2.Models;
using BookstoreRazor2.Repository.Interfaces;
namespace BookstoreRazor2.Repository
{
    public class GenreRepo : IGenreRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ToString();
            con = new SqlConnection(constr);
        }

        public bool Create(Genre genre)
        {
            try
            {
                string query = "INSERT INTO Genre (GenreName) VALUES (@GenreName);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
              
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@GenreName", genre.Name);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju

                    if (newFormedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa novog zanra. " + ex.StackTrace);
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM Book WHERE BookGenreId = @GenreId;";
                       query += "DELETE FROM Genre WHERE GenreId = @GenreId;";
                       

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@GenreId", id);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja zanra. {ex.StackTrace}");
                throw ex;
            }
        }

        public IEnumerable<Genre> GetAll()
        {
            List<Genre> genres = new List<Genre>();

            try
            {
                string query = "SELECT * FROM Genre;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Genre"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Genre"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }

                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int genreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string genreName = dataRow["GenreName"].ToString();

                    genres.Add(new Genre() { Id = genreId, Name = genreName });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja zanrova. {ex.StackTrace}");
                throw ex;
            }

            return genres;
        }

        public Genre GetById(int id)
        {
            Genre genre = null;

            try
            {
                string query = "SELECT * FROM Genre pc WHERE pc.GenreId = @GenreId;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Genre"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Genre"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }


                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int genreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string genreName = dataRow["GenreName"].ToString();

                    genre = new Genre() { Id = genreId, Name = genreName };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja zanra sa id-om: {genre.Id}. {ex.StackTrace}");
                throw ex;
            }

            return genre;
        }

        public void Update(Genre genre)
        {
            try
            {
                string query = "UPDATE Genre SET genreName = @genreName WHERE genreId = @genreId;";

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@genreId", genre.Id);  // stitimo od SQL Injection napada
                    cmd.Parameters.AddWithValue("@genreName", genre.Name);

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom updateovanja zanra. {ex.StackTrace}");
                throw ex;
            }
        }
    }
}