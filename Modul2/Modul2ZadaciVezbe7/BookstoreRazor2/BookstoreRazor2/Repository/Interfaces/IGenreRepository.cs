﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookstoreRazor2.Models;
namespace BookstoreRazor2.Repository.Interfaces
{
   public interface IGenreRepository
    {
        IEnumerable<Genre> GetAll();
        Genre GetById(int id);
        bool Create(Genre genre);
        void Update(Genre genre);
        void Delete(int id);
    }
}
