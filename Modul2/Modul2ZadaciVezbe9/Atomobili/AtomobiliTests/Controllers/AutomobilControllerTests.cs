﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Atomobili.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Atomobili.Repository;
using Atomobili.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;

namespace Atomobili.Controllers.Tests
{
    [TestClass()]
    public class AutomobilControllerTests
    {

        [TestMethod()]
        public void CreateTest()
        {
            // Arrange
            AutomobilController controller = new AutomobilController();

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNull(result);

        }

        [TestMethod()]
        public void ListByMotorTypeTest()
        {
            // Arrange
            AutomobilController controller = new AutomobilController();

            // Act
            ViewResult result = controller.ListByMotorType() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void DetailsTest()
        {
            var controller = new AutomobilController();
            var result = controller.Details(1) as ViewResult;

            AutomobilRepository automobilRepository = new AutomobilRepository();
            var repo = automobilRepository.GetById(1);
            var automobil = (Automobil)result.ViewData.Model;

            Assert.AreEqual(repo.Proizvodjac, automobil.Proizvodjac);
        }
    }
}