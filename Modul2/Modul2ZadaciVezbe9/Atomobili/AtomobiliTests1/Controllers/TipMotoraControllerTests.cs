﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Atomobili.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Atomobili.Controllers.Tests
{
    [TestClass()]
    public class TipMotoraControllerTests
    {
        [TestMethod()]
        public void IndexTest()
        {

            // Arrange
            TipMotoraController controller = new TipMotoraController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);

        }
    }
}