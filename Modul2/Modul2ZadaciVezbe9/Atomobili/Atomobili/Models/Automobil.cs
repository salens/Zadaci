﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Atomobili.Models
{
    public class Automobil
    {
        public int Id { get; set; }

        [Required] //Ime je obavezan parametar
        [StringLength(20, MinimumLength = 3)] //Minimum 3 karaktera, a Maksimum 20
        [RegularExpression("[A-Z][A-Za-z]*", ErrorMessage = "Prvo slovo {0} mora biti veliko!")] //Ime mora da počne velikim slovom
        [Display(Name = "Proizvodjac Automobila")]
        public string Proizvodjac { get; set; }

        [Required] //Ime je obavezan parametar
        [StringLength(20, MinimumLength = 3)] //Minimum 3 karaktera, a Maksimum 20
        [RegularExpression("[A-Z][A-Za-z]*", ErrorMessage = "Prvo slovo {0} mora biti veliko!")] //Ime mora da počne velikim slovom
        [Display(Name = "Model Automobila")]
        public string Model { get; set; }

        [Required] //Ime je obavezan parametar
        [Range(1960, 2017)]
        public int GodinaProizvodnje { get; set; }

        [Required] //Ime je obavezan parametar
        [StringLength(20, MinimumLength = 3)] //Minimum 3 karaktera, a Maksimum 20
        [RegularExpression("[A-Z][A-Za-z]*", ErrorMessage = "Prvo slovo {0} mora biti veliko!")] //Ime mora da počne velikim slovom
        [Display(Name = "Boja Automobila")]
        public string Boja { get; set; }

        [Required] //Ime je obavezan parametar
        [Range(750, 2500)]
        public int Kubikaza { get; set; }


        public bool IsDeleted { get; set; }
        public TipMotora TipMotora { get; set; }

    }
}