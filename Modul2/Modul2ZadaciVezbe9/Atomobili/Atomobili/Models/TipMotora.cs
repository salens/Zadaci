﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Atomobili.Models
{
    public class TipMotora
    {
        public int Id { get; set; }

        [Required] //Ime je obavezan parametar
        [StringLength(20, MinimumLength = 3)] //Minimum 3 karaktera, a Maksimum 20
        [RegularExpression("[A-Z][A-Za-z]*", ErrorMessage = "Prvo slovo {0} mora biti veliko!")] //Ime mora da počne velikim slovom
        [Display(Name = "Tip Motora Naziv")]
        public string TipMotoraNaziv { get; set; }
    }
}