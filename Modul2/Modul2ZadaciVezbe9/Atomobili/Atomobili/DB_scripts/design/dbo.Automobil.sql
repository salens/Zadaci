﻿CREATE TABLE Automobil
(
	AutomobilId INT IDENTITY(1,1) PRIMARY KEY,
    AutomobilProizvodjac NVARCHAR(80) NULL, 
    AutomobilModel NVARCHAR(100) NULL, 
    AutomobilGodinaProizvodnje INT NULL, 
	AutomobilBoja NVARCHAR(30) NULL, 
    AutomobilKubikaza INT NULL, 
	AutomobilIsDeleted BIT default 'FALSE',
	AutomobilTipMotoraId INT NULL, 
	FOREIGN KEY (AutomobilTipMotoraId) REFERENCES dbo.TipMotora(MotorId)
)
