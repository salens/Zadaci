﻿$(document).ready(function () {
    $("form").submit(function (event) {

        //Validacija za Tip Motora
        if ($("#TipMotoraNaziv").val() == '') {
            event.preventDefault();
            $("span[data-valmsg-for='TipMotoraNaziv']").text("Morate popuniti polje").css("color", "red")
        }

        //Validacija za Automobil
        if ($("#automobil_Proizvodjac").val().length < 3 || $("#automobil_Proizvodjac").val().length > 15) {
            event.preventDefault();
            $("span[data-valmsg-for='automobil.Proizvodjac']").text("Mora biti vise od 3 slova a manje od 15").css("color", "red").show();
        }
        else {
            $("span[data-valmsg-for='automobil.Proizvodjac']").hide();
        }
        if (!$("#automobil_Proizvodjac").val()) {
            event.preventDefault();
            $("span[data-valmsg-for='automobil.Proizvodjac']").text("Morate popuniti polje").css("color", "red")
        }

        if (($("#automobil_GodinaProizvodnje").val() >= 1960) && ($("#automobil_GodinaProizvodnje").val() =< 2018)) {
            event.preventDefault();
            $("span[data-valmsg-for='automobil.GodinaProizvodnje']").text("Morate staviti izmedju 1960 i 2017").css("color", "red")
        }
        else {
            $("span[data-valmsg-for='automobil.GodinaProizvodnje']").hide();
        }

        if ($("#automobil_Model").val() == '') {
            event.preventDefault();
            $("span[data-valmsg-for='automobil.Model']").text("Morate uneti nesto!").css("color", "red").show();
        } else {
            $("span[data-valmsg-for='automobil.Model']").hide();
        }
        
        if ($.isNumeric( $("#automobil_Boja").val())) {
            event.preventDefault();
            $("span[data-valmsg-for='automobil.Boja']").text("Morate uneti boju a ne broj!").css("color", "red").show();
        }
        else {
            $("span[data-valmsg-for='automobil.Boja']").hide();
        }

        if ($("#automobil_Boja").val() =='') {
            event.preventDefault();
            $("span[data-valmsg-for='automobil.Boja']").text("Morate uneti nesto!").css("color", "red")
        }

        if (!$("#automobil_Kubikaza").val()) {
            event.preventDefault();
            $("span[data-valmsg-for='automobil.Kubikaza']").text("Morate popuniti polje").css("color", "red")
        }
        if ($("#automobil_Kubikaza").val() < 2) {
            event.preventDefault();
            $("span[data-valmsg-for='automobil.Kubikaza']").text("Morate biti vece od 3").css("color", "red")
        }
      
    })
})