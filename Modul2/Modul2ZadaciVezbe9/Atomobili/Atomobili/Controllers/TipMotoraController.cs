﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Atomobili.Models;
using Atomobili.ViewModels;
using Atomobili.Repository;
using Atomobili.Repository.Interfaces;
namespace Atomobili.Controllers
{
    public class TipMotoraController : Controller
    {

        IAutomobilRepository automobilRepository = new AutomobilRepository();
        ITipMotoraRepository tipMotoraRepository = new TipMotoraRepository();

        // GET: TipMotora
        public ActionResult Index()
        {
            var tipoviMotora = tipMotoraRepository.GetAll();

            return View(tipoviMotora);
        }

        // GET: TipMotora/Details/5
        public ActionResult Details(int id)
        {
            var tipoviMotora = tipMotoraRepository.GetById(id);

            return View(tipoviMotora);
        }

        public ActionResult Create()
        {
            return View(new TipMotora());
        }

        // POST: TipMotora/Create
        [HttpPost]
        public ActionResult Create(TipMotora tipMotora)
        {
            if (ModelState.IsValid)
            {
                if (tipMotoraRepository.Create(tipMotora))
                {
                    return RedirectToAction("Index");   // nakon uspesnog dodavanja daj listing svih tipova motora
                }
            }

            return View(tipMotora);
        }

        // GET: TipMotora/Edit/5
        public ActionResult Edit(int id)
        {
            var tipMotora = tipMotoraRepository.GetById(id);

            return View(tipMotora);
        }

        // POST: TipMotora/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var tipMotora = new TipMotora();

            try
            {

                UpdateModel(tipMotora);

                tipMotoraRepository.Update(tipMotora);
                return RedirectToAction("Index");

            }
            catch
            {
                return View(tipMotora);
            }
            return View();
        }

        // GET: TipMotora/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                tipMotoraRepository.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return RedirectToAction("Index");
        }
    }
}