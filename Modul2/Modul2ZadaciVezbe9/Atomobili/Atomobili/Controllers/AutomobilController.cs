﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Atomobili.Models;
using Atomobili.ViewModels;
using Atomobili.Repository;
using Atomobili.Repository.Interfaces;
namespace Atomobili.Controllers
{
    public class AutomobilController : Controller
    {
        IAutomobilRepository automobilRepository = new AutomobilRepository();
        ITipMotoraRepository tipMotoraRepository = new TipMotoraRepository();

        // GET: Automobil
        public ActionResult Index(string sortOrder, string searchString, string FooBarDropDown)
        {
            var automobili = automobilRepository.GetAll();


            ViewBag.ProizvodjacSortParm = String.IsNullOrEmpty(sortOrder) ? "Proizvodjac" : "";
            ViewBag.ModelSortParm = String.IsNullOrEmpty(sortOrder) ? "Model" : "";
            ViewBag.GodinaProizvodnjeSortParm = String.IsNullOrEmpty(sortOrder) ? "GodinaProizvodnje" : "";
            ViewBag.KubikazaSortParm = String.IsNullOrEmpty(sortOrder) ? "Kubikaza" : "";

            var automobil = from s in automobili select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                if (!String.IsNullOrEmpty(FooBarDropDown))
                {
                    switch (FooBarDropDown)
                    {
                        case "Proizvodjac":
                            automobil = automobil.Where(s => s.Proizvodjac.Contains(searchString));
                            break;

                        case "Model":
                            automobil = automobil.Where(s => s.Model.Contains(searchString));
                            break;

                        case "GodinaProizvodnje":
                            automobil = automobil.Where(s => s.GodinaProizvodnje.Equals(Convert.ToInt32(searchString)));
                            break;

                        case "Kubikaza":
                            automobil = automobil.Where(s => s.Kubikaza.Equals(Convert.ToInt32(searchString)));
                            break;

                    }
                }

            }

            switch (sortOrder)
            {
                case "Proizvodjac":
                    automobil = automobil.OrderByDescending(s => s.Proizvodjac);
                    break;
                case "Model":
                    automobil = automobil.OrderByDescending(s => s.Model);
                    break;
                case "GodinaProizvodnje":
                    automobil = automobil.OrderByDescending(s => s.GodinaProizvodnje);
                    break;
                case "Kubikaza":
                    automobil = automobil.OrderByDescending(s => s.Kubikaza);
                    break;

            }

            return View(automobil);
        }

        // GET: Automobili/Details/5
        public ActionResult Details(int id)
        {
            var automobil = automobilRepository.GetById(id);
            return View(automobil);
        }

        public ActionResult Create()
        {
            var tipMotora = tipMotoraRepository.GetAll();

            var pcViewModel = new AutomobilTipMotoraViewModels
            {
                //Ovde smo ubacili i empty Automobil zbog validacije u value tag-u
                automobil = new Automobil(),
                TipMotora = tipMotora
            };

            return View(pcViewModel);
        }

        // POST: Automobil/Create
        [HttpPost]
        public ActionResult Create(AutomobilTipMotoraViewModels vm)
        {
            // Ovo je obavezno da ne bi validirao i ProductCategory.Name
            ModelState["automobil.TipMotora.TipMotoraNaziv"].Errors.Clear();
            //  a moze i ovako dole Ovo je obavezno da ne bi validirao i ProductCategory.Name
            //ModelState.Remove("Product.ProductCategory.Name");
            if (ModelState.IsValid)
            {
                automobilRepository.Create(vm.automobil);
                return RedirectToAction("Index");
            }

            var tipMotora = tipMotoraRepository.GetAll();

            vm.automobil = vm.automobil;
            vm.TipMotora = tipMotora;

            return View(vm);
        }

        public ActionResult Edit(int id)
        {
            var automobil = automobilRepository.GetById(id);
            var tipMotora = tipMotoraRepository.GetAll();

            var pcViewModel = new AutomobilTipMotoraViewModels
            {
                automobil = automobil,
                TipMotora = tipMotora
            };

            return View(pcViewModel);
        }

        // POST: Automobil/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var vm = new AutomobilTipMotoraViewModels();

            try
            {  // radimo Bindovanje sa FormCollection u ovom slucaju moze i umesto forme za objekat

                if (ModelState.IsValid)
                {
                    int exisingId = id;
                    string newProizvodjac = collection["automobil.Proizvodjac"];
                    string newModel = collection["automobil.Model"];
                    int newGodinaProizvodnje = int.Parse(collection["automobil.GodinaProizvodnje"]);
                    string newBoja = collection["automobil.Boja"];
                    int newKubikaza = int.Parse(collection["automobil.Kubikaza"]);
                    int tipMotoraId = int.Parse(collection["automobil.TipMotora.Id"]);

                    var updatedAutomobil = new Automobil()
                    {
                        Id = exisingId,
                        Proizvodjac = newProizvodjac,
                        Model = newModel,
                        GodinaProizvodnje = newGodinaProizvodnje,
                        Boja = newBoja,
                        Kubikaza = newKubikaza,
                        TipMotora = tipMotoraRepository.GetById(tipMotoraId)

                    };

                    // Moze i sama UpdateModel metoda

                    //UpdateModel(automobil);

                    automobilRepository.Update(updatedAutomobil);
                }
            }
            catch (Exception e)
            {
                //return RedirectToAction("Index");
                return View("Error");
            }

            vm.automobil = automobilRepository.GetById(id);
            vm.TipMotora = tipMotoraRepository.GetAll();
            return View(vm);
        }

        public ActionResult Delete(int id)
        {
            automobilRepository.Delete(id);
            return RedirectToAction("Index");
        }

        public ActionResult ListByMotorType()
        {
            var vm = new AutomobilTipMotoraViewModels();
            vm.automobili = automobilRepository.GetAll();
            vm.TipMotora = tipMotoraRepository.GetAll();
            return View(vm);
        }

        [HttpPost]
        public ActionResult ListByMotorType(int SelectedTipMotoraId)
        {
            var vm = new AutomobilTipMotoraViewModels();

            var automobili = automobilRepository.GetAllByMotorType(SelectedTipMotoraId).ToList();
            vm.automobili = automobili;
            vm.TipMotora = tipMotoraRepository.GetAll().ToList();
            return View(vm);
        }

        public ActionResult ListDeletedAutomobil()
        {
            var automobili = automobilRepository.GetAll().ToList();
            return View(automobili);
        }

    }
}