﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Atomobili.Models;
using Atomobili.Repository.Interfaces;
namespace Atomobili.Repository
{
    public class AutomobilRepository : IAutomobilRepository
    {
        private SqlConnection connection;
        private ITipMotoraRepository tipMotoraRepository = new TipMotoraRepository();

        private void CreateConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AutomobilDbContext"].ToString();
            connection = new SqlConnection(constr);
        }

        //CREATE
        public bool Create(Automobil automobil)
        {
            string query = "INSERT INTO Automobil (AutomobilProizvodjac, AutomobilModel, AutomobilGodinaProizvodnje,AutomobilBoja,AutomobilKubikaza,AutomobilIsDeleted,AutomobilTipMotoraId) VALUES (@AutomobilProizvodjac,@AutomobilModel,@AutomobilGodinaProizvodnje,@AutomobilBoja,@AutomobilKubikaza, @AutomobilIsDeleted, @TipMotora);";
            query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

            CreateConnection();   // inicijaizuj novu konekciju

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                cmd.Parameters.AddWithValue("AutomobilProizvodjac", automobil.Proizvodjac);  // stitimo od SQL Injection napada
                cmd.Parameters.AddWithValue("AutomobilModel", automobil.Model);
                cmd.Parameters.AddWithValue("AutomobilGodinaProizvodnje", automobil.GodinaProizvodnje);
                cmd.Parameters.AddWithValue("AutomobilBoja", automobil.Boja);
                cmd.Parameters.AddWithValue("AutomobilKubikaza", automobil.Kubikaza);
                cmd.Parameters.AddWithValue("AutomobilIsDeleted", "FALSE");
                cmd.Parameters.AddWithValue("TipMotora", automobil.TipMotora?.Id);

                connection.Open();                                         // otvori konekciju
                var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                connection.Close();                                        // zatvori konekciju

                if (newFormedId != null)
                {
                    return true;    // upis uspesan, generisan novi id
                }
            }
            return false;   // upis bezuspesan
        }

        //DELETE
        public void Delete(int id)
        {    
            string query = "UPDATE Automobil SET AutomobilIsDeleted = @AutomobilIsDeleted WHERE AutomobilId = @AutomobilId";

            CreateConnection();   // inicijaizuj novu konekciju

            using (SqlCommand cmd = connection.CreateCommand())
            {

                cmd.Parameters.AddWithValue("@AutomobilId", id);
                cmd.Parameters.AddWithValue("@AutomobilIsDeleted", true);
                cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                connection.Open();
                var a = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                connection.Close();                                                        // zatvori konekciju
            }
        }

        //GET ALL
        public IEnumerable<Automobil> GetAll()
        {
            string query = "SELECT * FROM Automobil a, TipMotora tm where a.AutomobilTipMotoraId = tm.MotorId;";
            CreateConnection();   // inicijaizuj novu konekciju

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                dadapter.Fill(ds, "Automobil"); // 'ProductCategory' je naziv tabele u dataset-u
                dt = ds.Tables["Automobil"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                connection.Close();                  // zatvori konekciju
            }

            List<Automobil> automobili = new List<Automobil>();

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int automobilId = int.Parse(dataRow["AutomobilId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string automobilProizvodjac = dataRow["AutomobilProizvodjac"].ToString();
                string automobilModel = dataRow["AutomobilModel"].ToString();              
                int automobilGodinaProizvodnje = int.Parse(dataRow["AutomobilGodinaProizvodnje"].ToString());
                string automobilBoja = dataRow["AutomobilBoja"].ToString();
                int automobilKubikaza = int.Parse(dataRow["AutomobilKubikaza"].ToString());
                bool automobilIsDeleted = bool.Parse(dataRow["AutomobilIsDeleted"].ToString());
                int tipMotoraId = int.Parse(dataRow["AutomobilTipMotoraId"].ToString());
                string tipMotoraNaziv = dataRow["TipMotoraNaziv"].ToString();

                TipMotora tm = new TipMotora
                {
                    Id = tipMotoraId,
                    TipMotoraNaziv = tipMotoraNaziv
                };

                automobili.Add(new Automobil() { Id = automobilId, Proizvodjac = automobilProizvodjac, Model = automobilModel, GodinaProizvodnje = automobilGodinaProizvodnje,Boja = automobilBoja,Kubikaza = automobilKubikaza,IsDeleted = automobilIsDeleted, TipMotora = tm });
            }

            return automobili;
        }

        //GET BY ID
        public Automobil GetById(int id)
        {
            string query = "SELECT * FROM Automobil a, TipMotora tm WHERE a.AutomobilId = @AutomobilId and tm.MotorId = a.AutomobilTipMotoraId;";
            CreateConnection();   // inicijaizuj novu konekciju

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("AutomobilId", id);

                SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                dadapter.Fill(ds, "Automobil"); // 'Product' je naziv tabele u dataset-u
                dt = ds.Tables["Automobil"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                connection.Close();                  // zatvori konekciju
            }

            Automobil automobil = null;

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int automobilId = int.Parse(dataRow["AutomobilId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string automobilProizvodjac = dataRow["AutomobilProizvodjac"].ToString();
                string automobilModel = dataRow["AutomobilModel"].ToString();
                int automobilGodinaProizvodnje = int.Parse(dataRow["AutomobilGodinaProizvodnje"].ToString());
                string automobilBoja = dataRow["AutomobilBoja"].ToString();
                int automobilKubikaza = int.Parse(dataRow["AutomobilKubikaza"].ToString());
                bool automobilIsDeleted = bool.Parse(dataRow["AutomobilIsDeleted"].ToString());
                int tipMotoraId = int.Parse(dataRow["AutomobilTipMotoraId"].ToString());
                string tipMotoraNaziv = dataRow["TipMotoraNaziv"].ToString();

                TipMotora tm = new TipMotora
                {
                    Id = tipMotoraId,
                    TipMotoraNaziv = tipMotoraNaziv
                };

                automobil = new Automobil() { Id = automobilId, Proizvodjac = automobilProizvodjac, Model = automobilModel, GodinaProizvodnje = automobilGodinaProizvodnje, Boja = automobilBoja, Kubikaza = automobilKubikaza, IsDeleted = automobilIsDeleted, TipMotora = tm };
            }

            return automobil;
        }

        //UPDATE
        public void Update(Automobil automobil)
        {
            string query = "UPDATE Automobil set AutomobilProizvodjac = @AutomobilProizvodjac, AutomobilModel = @AutomobilModel, AutomobilGodinaProizvodnje = @AutomobilGodinaProizvodnje, AutomobilBoja=@AutomobilBoja, AutomobilKubikaza=@AutomobilKubikaza , AutomobilTipMotoraId = @TipMotora  where AutomobilId = @AutomobilId;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("AutomobilId", automobil.Id);
                sqlCommand.Parameters.AddWithValue("AutomobilProizvodjac", automobil.Proizvodjac);
                sqlCommand.Parameters.AddWithValue("AutomobilModel", automobil.Model);
                sqlCommand.Parameters.AddWithValue("AutomobilGodinaProizvodnje", automobil.GodinaProizvodnje);
                sqlCommand.Parameters.AddWithValue("AutomobilBoja", automobil.Boja);
                sqlCommand.Parameters.AddWithValue("AutomobilKubikaza", automobil.Kubikaza);
                sqlCommand.Parameters.AddWithValue("TipMotora", automobil.TipMotora?.Id);

                connection.Open();
                sqlCommand.ExecuteNonQuery();
                connection.Close();
            }
        }

        //GET ALL BY MOTOR TYPE
        public IEnumerable<Automobil> GetAllByMotorType(int id)
        {
            List<Automobil> automobili = new List<Automobil>();

            try
            {
                string query = "SELECT * FROM Automobil where AutomobilTipMotoraId = @selectedTipMotoraId;";
                CreateConnection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("@selectedTipMotoraId", id);
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Automobil"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Automobil"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    connection.Close();                  // zatvori konekciju
                }

                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {

                    int automobilId = int.Parse(dataRow["AutomobilId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string automobilProizvodjac = dataRow["AutomobilProizvodjac"].ToString();
                    string automobilModel = dataRow["AutomobilModel"].ToString();
                    int automobilGodinaProizvodnje = int.Parse(dataRow["AutomobilGodinaProizvodnje"].ToString());
                    string automobilBoja = dataRow["AutomobilBoja"].ToString();
                    int automobilKubikaza = int.Parse(dataRow["AutomobilKubikaza"].ToString());
                    bool automobilIsDeleted = bool.Parse(dataRow["AutomobilIsDeleted"].ToString());
                    automobili.Add(new Automobil() { Id = automobilId, Proizvodjac = automobilProizvodjac, Model = automobilModel, GodinaProizvodnje = automobilGodinaProizvodnje, Kubikaza = automobilKubikaza, Boja = automobilBoja, TipMotora = tipMotoraRepository.GetById(id), IsDeleted = automobilIsDeleted });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja knjiga. {ex.StackTrace}");
                throw ex;
            }

            return automobili;
        }
    }
}