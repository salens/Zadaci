﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Atomobili.Models;
using Atomobili.Repository.Interfaces;
namespace Atomobili.Repository
{
    public class TipMotoraRepository : ITipMotoraRepository
    {
        private SqlConnection connection;

        private void CreateConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AutomobilDbContext"].ToString();
            connection = new SqlConnection(constr);
        }

        //CREATE
        public bool Create(TipMotora tipMotora)
        {
            string query = "INSERT INTO TipMotora (TipMotoraNaziv) VALUES (@TipMotoraNaziv);";
            query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

            CreateConnection();   // inicijaizuj novu konekciju

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                cmd.Parameters.AddWithValue("@TipMotoraNaziv", tipMotora.TipMotoraNaziv);  // stitimo od SQL Injection napada

                connection.Open();                                         // otvori konekciju
                var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                connection.Close();                                        // zatvori konekciju

                if (newFormedId != null)
                {
                    return true;    // upis uspesan, generisan novi id
                }
            }
            return false;   // upis bezuspesan
        }

        //DELETE
        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM Automobil WHERE AutomobilTipMotoraId = @TipMotoraId;";
                query += "DELETE FROM TipMotora WHERE MotorId = @TipMotoraId;";


                CreateConnection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@TipMotoraId", id);  // stitimo od SQL Injection napada

                    connection.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    connection.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja tipa motora. {ex.StackTrace}");
                throw ex;
            }
        }

        //GET ALL
        public IEnumerable<TipMotora> GetAll()
        {
            string query = "SELECT * FROM TipMotora;";
            CreateConnection();   // inicijaizuj novu konekciju

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                dadapter.Fill(ds, "TipMotora"); // 'ProductCategory' je naziv tabele u dataset-u
                dt = ds.Tables["TipMotora"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                connection.Close();                  // zatvori konekciju
            }

            List<TipMotora> tipoviMotora = new List<TipMotora>();

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int tipMotorId = int.Parse(dataRow["MotorId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string tipMotorNaziv = dataRow["TipMotoraNaziv"].ToString();

                tipoviMotora.Add(new TipMotora() { Id = tipMotorId, TipMotoraNaziv = tipMotorNaziv });
            }

            return tipoviMotora;
        }

        //GET BY ID
        public TipMotora GetById(int id)
        {

            string query = "SELECT * FROM TipMotora pc WHERE pc.MotorId = @MotorId;";
            CreateConnection();   // inicijaizuj novu konekciju

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                cmd.Parameters.AddWithValue("@MotorId", id);

                SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                dadapter.Fill(ds, "TipMotora"); // 'ProductCategory' je naziv tabele u dataset-u
                dt = ds.Tables["TipMotora"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                connection.Close();                  // zatvori konekciju
            }

            TipMotora tipMotora = null;

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int tipMotorId = int.Parse(dataRow["MotorId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string tipMotorNaziv = dataRow["TipMotoraNaziv"].ToString();

                tipMotora = new TipMotora() { Id = tipMotorId, TipMotoraNaziv = tipMotorNaziv };
            }

            return tipMotora;
        }

        //UPDATE
        public void Update(TipMotora tipMotora)
        {
            string query = "UPDATE TipMotora set TipMotoraNaziv = @Name where MotorId = @Id;";

            CreateConnection();

            using (SqlCommand sqlCommand = connection.CreateCommand())
            {
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("Name", tipMotora.TipMotoraNaziv);
                sqlCommand.Parameters.AddWithValue("Id", tipMotora.Id);

                connection.Open();
                sqlCommand.ExecuteScalar();
                connection.Close();
            }
        }
    }
}