﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atomobili.Models;
namespace Atomobili.Repository.Interfaces
{
    public interface IAutomobilRepository
    {
        IEnumerable<Automobil> GetAll();
        Automobil GetById(int id);
        IEnumerable<Automobil> GetAllByMotorType(int id);
        bool Create(Automobil automobil);
        void Update(Automobil automobil);
        void Delete(int id);
    }
}
