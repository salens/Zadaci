﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atomobili.Models;
namespace Atomobili.Repository.Interfaces
{
    public interface ITipMotoraRepository
    {
        IEnumerable<TipMotora> GetAll();
        TipMotora GetById(int id);
        bool Create(TipMotora tipMotora);
        void Update(TipMotora tipMotora);
        void Delete(int id);
    }
}
