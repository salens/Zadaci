﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Atomobili.Models;
namespace Atomobili.ViewModels
{
    public class AutomobilTipMotoraViewModels
    {
        public Automobil automobil { get; set; }
        public IEnumerable<Automobil> automobili { get; set; }
        public IEnumerable<TipMotora> TipMotora { get; set; }
    }
}