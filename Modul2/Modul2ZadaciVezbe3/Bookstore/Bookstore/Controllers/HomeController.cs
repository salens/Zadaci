﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Bookstore.Models;
namespace Bookstore.Controllers
{
    public class HomeController : Controller
    {
        List<Models.Book> sveKnjige = new List<Models.Book>();

        public ActionResult Index()
        {
            // ucitavnje metode Add koja se nalazi u ovom controleru
            return RedirectToAction("Add", "Home");
        }


        // Metoda za dodavanje nove Knjige
        public ActionResult Add()
        {
            sveKnjige = Models.Bookstore.Knjige;
            Book book = new Book();

            var poslednjiElement = sveKnjige[sveKnjige.Count - 1];
            
            int id = poslednjiElement.Id + 1;

            StringBuilder sb = new StringBuilder();

            sb.Append("<h3><b>Bookstore - Home page:</b></h3>");
            sb.Append("<form action='InsertNewBook' method='post'>");
            sb.Append("<table border='1'>");
            sb.Append("<tr><th>Add new book</th></tr>");    
            sb.Append("<tr>");
            sb.Append("<td hidden><input type='hidden' name='id' value='" + id + "' /></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>Name of book:<input type='text' name='name' value='' required/></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>Price of book:<input type='text'name='price' value='' required/></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>Genre of book:<select name='genre' value=''><option value = 'Science'> Science </option><option value = 'Comedy'> Comedy </option ><option value = 'Horror'> Horror </option ></select ></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td><input type='submit' value='Add'  style='float:right;'/></td>");
            sb.Append("</tr>");
            sb.Append("</form>");
            sb.Append("</table>");

            sb.Append("<a href='/Bookstore/List'>Show all books</a> | ");
            sb.Append("<a href='/Bookstore/ListDeleted'>Show deleted books</a>");
            return Content(sb.ToString());
        }

        // Metoda za unos nove knjige u listu
        public ActionResult InsertNewBook(int id, string name, double price, string genre)
        {
            int testId = Convert.ToInt32(id);
            Book newBook = new Book(testId, name, price, genre);
            sveKnjige = Models.Bookstore.Knjige;
            sveKnjige.Add(newBook);

            return RedirectToAction("List", "Bookstore");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}