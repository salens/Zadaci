﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Bookstore.Models;

namespace Bookstore.Controllers
{
    public class BookstoreController : Controller
    {
        List<Models.Book> sveKnjige = new List<Models.Book>();
        int brojac = 0;
        // GET: Bookstore
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<h1>Lista knjiga:</h1>");

            sveKnjige = Models.Bookstore.Knjige;

            sb.Append("<table border='1'>");
            sb.Append("<tr><th>Id</th><th>Name</th><th>Price</th><th>Genre</th></tr>");
         
            foreach (Book knjiga in sveKnjige)
            {
           
                if (knjiga.Deleted == false)
                {
                    brojac +=1;
                    sb.Append("<tr>");                
                    sb.Append("<td>" + knjiga.Id + "</td>");
                    sb.Append("<td>" + knjiga.Name + "</td>");
                    sb.Append("<td>" + knjiga.Price + "</td>");
                    sb.Append("<td>" + knjiga.Genre + "</td>");
                    sb.Append("<td>" + "<input type='hidden' name='knjiga' value='" + knjiga.Id.ToString() + "'/>" + " </td>");
                    sb.Append($"<td><a href='Delete/{knjiga.Id}'>delete</a></td>");          
                    sb.Append("</tr>");

                }
              
            }
            sb.Append("</table>");
            sb.Append("<a href='/Home/Add'>Link for homepage</a>");

            return Content(sb.ToString());
        }

        public ActionResult ListDeleted()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<h1>Lista OBRISANIH knjiga:</h1>");


            sveKnjige = Models.Bookstore.Knjige;

            sb.Append("<table border='1'>");
            sb.Append("<tr><th>Id</th><th>Name</th><th>Price</th><th>Genre</th></tr>");

            foreach (Book knjiga in sveKnjige)
            {

                if (knjiga.Deleted == true)
                {

                    sb.Append("<tr>");
                    sb.Append("<td>" + knjiga.Id + "</td>");
                    sb.Append("<td>" + knjiga.Name + "</td>");
                    sb.Append("<td>" + knjiga.Price + "</td>");
                    sb.Append("<td>" + knjiga.Genre + "</td>");
                    sb.Append("</tr>");

                }

            }
            sb.Append("</table>");
            sb.Append("<a href='/Home/Add'>Link for homepage</a>");

            return Content(sb.ToString());
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            sveKnjige = Models.Bookstore.Knjige;

            foreach (Book knjige in sveKnjige)
            {
                if(knjige.Id == id)
                {
                    sveKnjige[knjige.Id - 1].Deleted = true;
                    break;
                }
               
            }

              return RedirectToAction("List", "Bookstore");
        }
    }
}