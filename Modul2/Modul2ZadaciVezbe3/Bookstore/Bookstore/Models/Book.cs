﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookstore.Models
{
    public class Book
    {
        private int id;
        private string name;
        private double price;
        private string genre;
        private bool deleted;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public double Price { get => price; set => price = value; }
        public string Genre { get => genre; set => genre = value; }
        public bool Deleted { get => deleted; set => deleted = value; }

        public Book()
        {
        }

        public Book(int id, string name, double price, string genre)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.genre = genre;
        }

        public Book(int id, string name, double price, string genre, bool deleted)
        {
            this.name = name;
            this.price = price;
            this.genre = genre;
            Deleted = deleted;
        }

        public Book(bool deleted)
        {
            Deleted = deleted;
        }

        public override string ToString()
        {
            return "Id: " + Id + " Name: " + Name + " Price: " + Price + " Genre: " + Genre + " Deleted: " + Deleted;
        }

    }
}