﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookstore.Models
{
    public class Bookstore
    {
        private int id;
        private string name;
        private static List<Book> knjige;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public static List<Book> Knjige { get => knjige; set => knjige = value; }

        static Bookstore()
        {
            knjige = new List<Book>()
            {
              new Book {Id=1 , Name = "Moja knjiga", Price = 66.78 , Genre = "Science" },
              new Book {Id=2 , Name = "Tvoja knjiga", Price = 66.78 , Genre = "Comedy"},
              new Book {Id=3 , Name = "Nasa knjiga", Price = 66.78 , Genre = "Horror"}
            };
        }

        public Bookstore(string name)
        {
            this.name = name;
        }

    }
}