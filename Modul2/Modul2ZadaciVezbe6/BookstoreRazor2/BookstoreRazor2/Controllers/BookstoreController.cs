﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BookstoreRazor2.Controllers
{
    [RoutePrefix("Knjizara")]
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            return RedirectToAction("AddBook");
        }
        [Route("Napravi")]
        public ActionResult AddBook()
        {
            BookstoreBookGenreViewModel bookstoreBookGenreViewModel = new BookstoreBookGenreViewModel();
            bookstoreBookGenreViewModel.GenresList = Project.Genres();
            return View(bookstoreBookGenreViewModel);
        }
        [Route("Napravi")]
        [HttpPost]
        public ActionResult AddBook(BookstoreBookGenreViewModel bookstoreBookGenreViewModel)
        {
            string query = "INSERT INTO Book (BookGenreId,BookName,BookPrice,BookIsDeleted) VALUES (@BookGenreId,@BookName,@BookPrice,@BookIsDeleted);";
            query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("@BookGenreId", bookstoreBookGenreViewModel.SelectedGenreId);
                    cmd.Parameters.AddWithValue("@BookName", bookstoreBookGenreViewModel.Book.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", bookstoreBookGenreViewModel.Book.Price);                  
                    cmd.Parameters.AddWithValue("@BookIsDeleted", bookstoreBookGenreViewModel.Book.IsDeleted);
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    con.Open();
                    var a = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                                        // zatvori konekciju
                }
            }

            return RedirectToAction("/ListaKnjiga");  // izlistaj ponovo spisak svih
        }
        [Route("ListaKnjiga")]
        public ActionResult List(string sortOrder, string searchString, string FooBarDropDown)
        {

            string query = "SELECT * FROM Book p INNER JOIN Genre pc ON p.BookGenreId = pc.GenreId";  // slektuj mi sve proizvode i njima odgovarajuce kategorije
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }
            }
            List<Book> books = new List<Book>();

            Genre genre;
            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                genre = new Genre();

                int bookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string bookName = dataRow["BookName"].ToString();
                double bookPrice = double.Parse(dataRow["BookPrice"].ToString());
                genre.Name = dataRow["GenreName"].ToString();
                bool bookIsDeleted = bool.Parse(dataRow["BookIsDeleted"].ToString());
                books.Add(new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genre, IsDeleted = bookIsDeleted });

            }


            ViewBag.NazivSortParm = String.IsNullOrEmpty(sortOrder) ? "Naziv" : "";
            ViewBag.CenaSortParm = String.IsNullOrEmpty(sortOrder) ? "Cena" : "";

            var book = from s in books select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                if (!String.IsNullOrEmpty(FooBarDropDown))
                {
                    switch (FooBarDropDown)
                    {
                        case "Naziv":
                            book = book.Where(s => s.Name.Contains(searchString));
                            break;

                        case "Cena":
                            book = book.Where(s => s.Price.Equals(Convert.ToDouble(searchString)));
                            break;

                    }
                }

            }

            switch (sortOrder)
            {
                case "Naziv":
                    book = book.OrderBy(s => s.Name);
                    break;
                case "Cena":
                    book = book.OrderBy(s => s.Price);
                    break;
                default:
                    book = book.OrderByDescending(s => s.Name);
                    break;
            }

            return View(book);

        }

        public ActionResult ListByGenre()
        {

            var vm = new BookstoreBookGenreViewModel();
            vm.booksList = Project.Books();
            vm.GenresList = Project.Genres();
            return View(vm);
        }
        [HttpPost]
        public ActionResult ListByGenre(int SelectedGenreId)
        {
            var vm = new BookstoreBookGenreViewModel();

            string query = "SELECT * FROM Book p INNER JOIN Genre pc ON p.BookGenreId = pc.GenreId WHERE p.BookGenreId = @BookGenreId ";  // slektuj mi sve proizvode i njima odgovarajuce kategorije
            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.Parameters.AddWithValue("@BookGenreId", SelectedGenreId);
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }
            }

            List<Book> books = new List<Book>();

            Genre genre;
            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                genre = new Genre();

                int bookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string bookName = dataRow["BookName"].ToString();
                double bookPrice = double.Parse(dataRow["BookPrice"].ToString());
                genre.Name = dataRow["GenreName"].ToString();
                bool bookIsDeleted = bool.Parse(dataRow["BookIsDeleted"].ToString());
                books.Add(new Book() { Id = bookId, Name = bookName, Price = bookPrice, Genre = genre, IsDeleted = bookIsDeleted });

            }

            vm.booksList = books;
            vm.GenresList = Project.Genres();
            return View(vm);
        }
    }
}