﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace BookstoreRazor2.Controllers
{
    public class GenreController : Controller
    {
        // GET: Genre
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(BookstoreBookGenreViewModel bookstoreBookGenreViewModel)
        {
            string query = "INSERT INTO Genre (GenreName) VALUES (@GenreName);";
            query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
      
                    cmd.Parameters.AddWithValue("@GenreName", bookstoreBookGenreViewModel.Genre.Name);
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    con.Open();
                    var a = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                                        // zatvori konekciju
                }
            }

            return RedirectToAction("AddBook", "Bookstore");
        }

        public ActionResult Delete()
        {
            var vm = new BookstoreBookGenreViewModel();
            vm.GenresList = Project.Genres();
            return View(vm);
        }


        // OVO NISAM USPEO - URADIO MALO PRE
        [HttpPost]
        public ActionResult Delete(int SelectedGenreId)
        {
            string query = "DELETE FROM Book WHERE BookGenreId = @GenreId ";
                   query += "DELETE FROM Genre WHERE GenreId = @GenreId";

            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {

                    cmd.Parameters.AddWithValue("@GenreId", SelectedGenreId);
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    con.Open();
                    var a = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                                        // zatvori konekciju
                }
            }

            return RedirectToAction("List", "Bookstore");
        }
    }
}