﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookstoreRazor2.ViewModels;
using BookstoreRazor2.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace BookstoreRazor2.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            Book foundedBook = null;
            var vm = new BookstoreBookGenreViewModel();

            foreach (var item in Project.Books())
            {
                if(item.Id == id)
                {
                    foundedBook = item;
                }
            }

            vm.Book = foundedBook;
            vm.GenresList = Project.Genres();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(BookstoreBookGenreViewModel vm)
        {
            string query = "UPDATE Book SET BookGenreId = @BookGenreId, BookName = @BookName, BookPrice = @BookPrice, BookIsDeleted = @BookIsDeleted WHERE BookId = @BookId";

            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {

                    cmd.Parameters.AddWithValue("@BookId", vm.Book.Id);
                    cmd.Parameters.AddWithValue("@BookName", vm.Book.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", vm.Book.Price);
                    cmd.Parameters.AddWithValue("@BookGenreId", vm.SelectedGenreId);
                    cmd.Parameters.AddWithValue("@BookIsDeleted", false);
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    con.Open();
                    var a = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                                        // zatvori konekciju
                }
            }

            return RedirectToAction("List", "Bookstore");
        }


        public ActionResult Delete(int id)
        {
            string query = "UPDATE Book SET BookIsDeleted = @BookIsDeleted WHERE BookId = @BookId";

            string connectionString = ConfigurationManager.ConnectionStrings["BookstoreDbContext"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {

                    cmd.Parameters.AddWithValue("@BookId", id);
                    cmd.Parameters.AddWithValue("@BookIsDeleted", true);
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    con.Open();
                    var a = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                                        // zatvori konekciju
                }
            }

            return RedirectToAction("ListDeletedBooks");
        }


        public ActionResult ListDeletedBooks()
        {
            return View(Project.Books());
        }
    }
}