﻿CREATE TABLE [dbo].[Book]
(
	[BookId] INT IDENTITY(1,1) PRIMARY KEY,
	[BookGenreId] INT NULL, 
    [BookName] NVARCHAR(80) NULL, 
    [BookPrice] NUMERIC NULL, 
	[BookIsDeleted] BIT default 'FALSE',
	FOREIGN KEY ([BookGenreId]) REFERENCES dbo.Genre(GenreId)
)
