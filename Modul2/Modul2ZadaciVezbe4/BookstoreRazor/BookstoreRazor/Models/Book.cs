﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

public class Book
{
    public string Id { get; set; }
    public string Name { get; set; }
    public double Price { get; set; }
    public string Genre { get; set; }
    public string IsDeleted { get; set; } = "false";

    public Book()
    {

    }
    public Book(string id, string name, double price, string genre, string isDeleted) : this()
    {
        Id = id;
        Name = name;
        Price = price;
        Genre = genre;
        IsDeleted = isDeleted;
    }

    public static void AddNewBook(Book book)
    {
        StreamWriter writetext;

        string path = HttpContext.Current.ApplicationInstance.Server.MapPath("../App_Data/knjige.txt");

        using (writetext = new StreamWriter(path, true))
        {
            writetext.WriteLine(book.Id + ';' + book.Name + ';' + book.Price + ';' + book.Genre + ';' + book.IsDeleted);

        }
    }

    public static void RemoveBook(Book book)
    {
        string path = HttpContext.Current.Server.MapPath("~/App_Data/knjige.txt");

        int line_to_edit = Convert.ToInt32(book.Id);

        // Read the old file.
        string[] lines = File.ReadAllLines(path);
            
        // Write the new file over the old file.
        using (StreamWriter writer = new StreamWriter(path))
        {
         
            for (int currentLine = 1; currentLine <= lines.Length; ++currentLine)
            {
                if (currentLine == line_to_edit)
                {
                    writer.WriteLine(book.Id + ';' + book.Name + ';' + book.Price + ';' + book.Genre + ';' + book.IsDeleted);
                }
                else
                {
                    writer.WriteLine(lines[currentLine - 1]);
                }
            }
        }
    }
}
