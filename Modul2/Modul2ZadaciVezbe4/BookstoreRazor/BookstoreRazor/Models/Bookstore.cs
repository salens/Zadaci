﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

public class Bookstore
{

    public int id { get; set; }
    public string name { get; set; }
    public Dictionary<string, Book> knjige { get; set; }

    public Bookstore()
    {
    }

    public Bookstore(int id, string name)
    {
        this.id = id;
        this.name = name;
    }

    public Bookstore(string path)
    {
        path = HostingEnvironment.MapPath(path);
        knjige = new Dictionary<string, Book>();
        FileStream stream = new FileStream(path, FileMode.Open);
        StreamReader sr = new StreamReader(stream);
        string line = "";

        while ((line = sr.ReadLine()) != null)
        {
            string[] tokens = line.Split(';');
            Book b = new Book(tokens[0], tokens[1], double.Parse(tokens[2]), tokens[3], tokens[4]);
            knjige.Add(b.Id, b);
 
        }
        sr.Close();
    }

}
