﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace BookstoreRazor.Controllers
{
    public class BookstoreController : Controller
    {

        // GET: Bookstore
        public ActionResult Index()
        {
            // ucitavnje metode Add koja se nalazi u ovom controleru
            return RedirectToAction("AddKnjiga", "Bookstore");
        }

        // Add Book
        public ActionResult AddKnjiga()
        {
            Bookstore books = new Bookstore(@"~/App_Data/knjige.txt");
            ViewBag.knjige = books.knjige.Last();

            return View();

        }

        // Get All Books
        public ActionResult ListaKnjiga(string sortOrder, string searchString, string FooBarDropDown)
        {
          
            Dictionary<string, Book> sveKnjige = new Dictionary<string, Book>();
            Bookstore books = new Bookstore(@"~/App_Data/knjige.txt");

         
            sveKnjige = books.knjige;

            ViewBag.NazivSortParm = String.IsNullOrEmpty(sortOrder) ? "Naziv" : "";
            ViewBag.CenaSortParm = String.IsNullOrEmpty(sortOrder) ? "Cena" : "";
            ViewBag.ZanrSortParm = String.IsNullOrEmpty(sortOrder) ? "Zanr" : "";

            var book = from s in sveKnjige select s;




            if (!String.IsNullOrEmpty(searchString))
            {
                if(!String.IsNullOrEmpty(FooBarDropDown))
                {
                    switch (FooBarDropDown)
                    {
                        case "Naziv":
                            book = book.Where(s => s.Value.Name.Contains(searchString));
                            break;

                        case "Cena":
                            book = book.Where(s => s.Value.Price.Equals(Convert.ToDouble(searchString)));
                            break;

                        case "Zanr":
                            book = book.Where(s => s.Value.Genre.Contains(searchString));
                            break;

                    }
                }

               
            }

            switch (sortOrder)
            {
                case "Naziv":
                    book = book.OrderBy(s => s.Value.Name);
                    break;
                case "Cena":
                    book = book.OrderBy(s => s.Value.Price);
                    break;
                case "Zanr":
                    book = book.OrderBy(s => s.Value.Genre);
                    break;
                default:
                    book = book.OrderByDescending(s => s.Value.Name);
                    break;
            }

            
            return View(book.ToList());

        }

        public ActionResult InsertNewBook(string id, string name, double price, string genre)
        {
            Dictionary<string, Book> sveKnjige = new Dictionary<string, Book>();
            Bookstore books = new Bookstore(@"~/App_Data/knjige.txt");

            sveKnjige = books.knjige;

            if (name != null)
            {
                foreach (KeyValuePair<string, Book> knjige in sveKnjige)
                {
                    if (name.ToLower() == knjige.Value.Name.ToLower())
                    {
                        string poruka = "Knjiga sa nazivom: " + name + " Vec postiji u bazi!";
                        TempData["status"] = poruka;

                        return RedirectToAction("AddKnjiga", "Bookstore");
                    }

                }

                string isDel = "false";
                Book newBook = new Book(id, name, price, genre, isDel);
                Book.AddNewBook(newBook);

                return RedirectToAction("ListaKnjiga", "Bookstore");

            }

            return RedirectToAction("AddKnjiga", "Bookstore");

        }

        public ActionResult ListaDeleted()
        {
            Bookstore books = new Bookstore(@"~/App_Data/knjige.txt");
            ViewBag.knjige = books;

            return View();
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Book removeBook = new Book();
            string IdKnjiga = Convert.ToString(id);

            Dictionary<string, Book> sveKnjige = new Dictionary<string, Book>();
            Bookstore books = new Bookstore(@"~/App_Data/knjige.txt");

            sveKnjige = books.knjige;

            foreach (KeyValuePair<string, Book> knjige in sveKnjige)
            {
                if (knjige.Key == IdKnjiga)
                {
                    knjige.Value.IsDeleted = "true";
                    removeBook = knjige.Value;
                    Book.RemoveBook(removeBook);
                }

            }

            return RedirectToAction("ListaKnjiga", "Bookstore");
        }
    }

}