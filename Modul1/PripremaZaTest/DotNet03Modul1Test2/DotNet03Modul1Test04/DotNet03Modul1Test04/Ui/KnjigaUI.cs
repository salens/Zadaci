﻿using DotNet03Modul1Test04.Dao;
using DotNet03Modul1Test04.Model;
using DotNet03Modul1Test04.PDF;
using DotNet03Modul1Test04.Utils;
using DotNet03Modul1Test04.XML;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test04.Ui
{
	class KnjigaUI
	{
		public static object PacijentDAO { get; private set; }

		internal static void Menu()
		{
			int odluka = -1;
			while (odluka != 0)
			{
				IspisiMenu();
				Console.Write("opcija:");
				odluka = IO.OcitajCeoBroj();
				switch (odluka)
				{
					case 0:
						Console.WriteLine("Izlaz");
						break;
					case 1:
						IspisiSvihKnjiga();
						break;
					case 2:
						IspisiSveKnjigeINjihoveClanove();
						break;
					case 3:
						UnosNoveKnjige();
						break;
					case 4:
						IzmenaPodatakaOKnjizi();
						break;
					case 5:
						BrisanjePodatakaOKnjizi();
						break;
					case 6:
						KreiranjeIzvestajaOKnjigama();
						break;
					case 7:
						IspisSvihKnjigaPoNazivu();
						break;
					case 8:
						KreiranjeIzvestajaOKnjigamaPDF();
						break;
					default:
						Console.WriteLine("Nepostojeca komanda");
						break;
				}
			}
		}


		// MENI UI
		public static void IspisiMenu()
		{
			Console.WriteLine("Rad sa knjigama - opcije:");
			Console.WriteLine("\tOpcija broj 1 - ispis svih Knjiga");
			Console.WriteLine("\tOpcija broj 2 - ispis svih Knjiga i clanova");
			Console.WriteLine("\tOpcija broj 3 - unos nove Knjige");
			Console.WriteLine("\tOpcija broj 4 - izmena Knjige");
			Console.WriteLine("\tOpcija broj 5 - brisanje Knjige");
			Console.WriteLine("\tOpcija broj 6 - Snimanje Izvestaja Knjige u XML");
			Console.WriteLine("\tOpcija broj 7 - ispis svih Knjiga po Nazivu");
			Console.WriteLine("\tOpcija broj 8 - Snimanje svih Knjiga po Nazivu u PDF");
			Console.WriteLine("\t\t ...");
			Console.WriteLine("\tOpcija broj 0 - IZLAZ");
		}

		// ISPIH SVIH UI
		private static void IspisiSvihKnjiga()
		{
			List<Knjiga> sveKnjige = KnjigaDAO.GetAll(Program.conn);
			Clan clan;
		
			for (int i = 0; i < sveKnjige.Count; i++)
			{
				if (sveKnjige[i].ClanId == 0)
				{
					Console.WriteLine(sveKnjige[i] + " Slobodna Za Iznajmljivanje!");

					
				}
				else
				{
					int idKnjige = (int)sveKnjige[i].ClanId;
					clan = ClanDAO.GetById(Program.conn, idKnjige);
					Console.WriteLine(sveKnjige[i] + " Zauzeta Za Iznajmljivanje!");
					if(sveKnjige[i].ClanId == clan.Id)
					{
						Console.WriteLine("Nalazi se kod clana: " + clan.ToString());
					}
				
				}

			}
		}

		// ISPIS SVIH PO ODREDJENOM NAZIVU
		private static void IspisSvihKnjigaPoNazivu()
		{

			List<Knjiga> retVal = new List<Knjiga>();

			Console.Write("Unesi Naziv Knjige :");
			string naziv = IO.OcitajTekst();

			retVal = KnjigaDAO.GetByNaziv(Program.conn, naziv);

			if (retVal.Count == 0)
			{
				Console.WriteLine("Knjiga sa nazivom: " + naziv
						 + " ne postoji u evidenciji");
			}
			else
			{
				foreach (Knjiga k in retVal)
				{
					Console.WriteLine(k);
				}
			}

		}
		// ISPIS SVIH I NJIHOVIH PACIJENATA UI
		private static void IspisiSveKnjigeINjihoveClanove()
		{
			List<Knjiga> knjige = KnjigaDAO.GetAll(Program.conn);
			List<Clan> sviClanoviKodKogaSuKnjige = ClanDAO.GetAll(Program.conn);

			for (int i = 0; i < knjige.Count; i++)
			{
				if (knjige[i].ClanId != 0)
				{
					Console.WriteLine(knjige[i]);

					for (int j = 0; j < sviClanoviKodKogaSuKnjige.Count; j++)
					{
						if (knjige[i].ClanId == sviClanoviKodKogaSuKnjige[j].Id)
						{
							Console.WriteLine(sviClanoviKodKogaSuKnjige[j]);
						}
					}
				}
				

			}
		}

		//UNOS NOVOG UI
		private static void UnosNoveKnjige()
		{
			Console.Write("Unesi ime:");
			string Ime = IO.OcitajTekst();
			Console.Write("Unesi Autora:");
			string Autor = IO.OcitajTekst();
			Console.Write("Unesi Godinu Izdavanja:");
			int GodinaIzdavanja = IO.OcitajCeoBroj();


			Knjiga knjiga = new Knjiga(Ime, Autor, GodinaIzdavanja);
			KnjigaDAO.Add(Program.conn, knjiga);
		}
		// IZMENA UI
		private static void IzmenaPodatakaOKnjizi()
		{
			Knjiga knjiga = PronadjiKnjigu();
			if (knjiga != null)
			{
				Console.Write("Unesi novo Ime :");
				string ime = IO.OcitajTekst();
				knjiga.Ime = ime;

				Console.Write("Unesi Autora :");
				string prezime = IO.OcitajTekst();
				knjiga.Autor = prezime;

				Console.Write("Unesi Godinu Izdavanja:");
				int godina = IO.OcitajCeoBroj();
				knjiga.GodinaIzdavanja = godina;

				KnjigaDAO.Update(Program.conn, knjiga);
			}
		}

		// PRONADJI PO ID UI
		public static Knjiga PronadjiKnjigu()
		{
			Knjiga retVal = null;
			Console.WriteLine("Unesi Id knjige: ");
			int idKnjige = IO.OcitajCeoBroj();
			retVal = KnjigaDAO.GetById(Program.conn, idKnjige);

			if (retVal == null)
				Console.WriteLine("Knjiga sa id " + idKnjige
						 + " ne postoji u evidenciji");
			return retVal;
		}
		// BRISANJE UI
		private static void BrisanjePodatakaOKnjizi()
		{
			Knjiga knjiga = PronadjiKnjigu();
			if (knjiga != null)
			{
				KnjigaDAO.Delete(Program.conn, knjiga.Id);
			}
		}

		// XML IZVESTAJ
		private static void KreiranjeIzvestajaOKnjigama()
		{
			KreiranjeIzvestajaOKnjigama kreirajIzvestaj = new KreiranjeIzvestajaOKnjigama();
			if (kreirajIzvestaj.SnimiIzvestaj() == true)
			{
				Console.WriteLine("Uspesno snimljen izvestaj!");
			}
			else
			{
				Console.WriteLine("Izvestaj nije snimljen!");
			}
		}

		public static void KreiranjeIzvestajaOKnjigamaPDF()
		{
			KreirajIzvestajPDF kreirajIzvestaj = new KreirajIzvestajPDF();
			if (kreirajIzvestaj.SnimiIzvestaj() == true)
			{
				Console.WriteLine("Uspesno snimljen izvestaj!");
			}
			else
			{
				Console.WriteLine("Izvestaj nije snimljen!");
			}
		}
	}
}
