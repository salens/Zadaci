﻿using DotNet03Modul1Test04.Dao;
using DotNet03Modul1Test04.Model;
using DotNet03Modul1Test04.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test04.Ui
{
	class ClanUI
	{

		internal static void Menu()
		{
			int odluka = -1;
			while (odluka != 0)
			{
				IspisiMenu();
				Console.Write("opcija:");
				odluka = IO.OcitajCeoBroj();
				switch (odluka)
				{
					case 0:
						Console.WriteLine("Izlaz");
						break;
					case 1:
						IspisiSveClanove();
						break;
					case 2:
						IspisiSveClanoveIKnjigeKojeSuIznajmili();
						break;
					case 3:
						UnosNovogClana();
						break;
					case 4:
						IzmenaPodatakaOClanu();
						break;
					case 5:
						BrisanjePodatakaOClanu();
						break;
					case 6:
						IznajmljivanjeKnjige();
						break;
					default:
						Console.WriteLine("Nepostojeca komanda");
						break;
				}
			}
		}

		//ISPIS CLANOVA I KNJIGU KOJU JE IZNAJMIO
		private static void IspisiSveClanoveIKnjigeKojeSuIznajmili()
		{
			List<Knjiga> knjige = KnjigaDAO.GetAll(Program.conn);
			List<Clan> sviClanoviKodKogaSuKnjige = ClanDAO.GetAll(Program.conn);

			for (int i = 0; i < sviClanoviKodKogaSuKnjige.Count; i++)
			{
				Console.WriteLine(sviClanoviKodKogaSuKnjige[i]);

				for (int j = 0; j < knjige.Count; j++)
				{
					if (sviClanoviKodKogaSuKnjige[i].Id == knjige[j].ClanId)
					{
						Console.WriteLine("Knjiga: " + knjige[j]);
					}
				}
			}
		}

		// MENI UI
		public static void IspisiMenu()
		{
			Console.WriteLine("Rad sa clanovima - opcije:");
			Console.WriteLine("\tOpcija broj 1 - ispis svih Clanova");
			Console.WriteLine("\tOpcija broj 2 - ispis svih Clanova ukljucujuci i knjige koje su iznajmili");
			Console.WriteLine("\tOpcija broj 3 - unos novog Clana");
			Console.WriteLine("\tOpcija broj 4 - izmena Clana");
			Console.WriteLine("\tOpcija broj 5 - brisanje Clana");
			Console.WriteLine("\tOpcija broj 6 - Iznajmljivanje Knjige");
			Console.WriteLine("\t\t ...");
			Console.WriteLine("\tOpcija broj 0 - IZLAZ");
		}

		// ISPIS SVE UI
		private static void IspisiSveClanove()
		{
			List<Clan> clanovi = ClanDAO.GetAll(Program.conn);
			for (int i = 0; i < clanovi.Count; i++)
			{
				Console.WriteLine(clanovi[i]);

			}
		}

		// UNOS NOVOG UI
		private static void UnosNovogClana()
		{
			Console.Write("Unesi ime:");
			string Ime = IO.OcitajTekst();
			Console.Write("Unesi prezime:");
			string Prezime = IO.OcitajTekst();

			Clan clan = new Clan(Ime, Prezime);
			ClanDAO.Add(Program.conn, clan);

		}

		// IZMENA UI
		private static void IzmenaPodatakaOClanu()
		{
			Clan pac = PronadjiClana();
			if (pac != null)
			{
				Console.Write("Unesi novo Ime :");
				string ime = IO.OcitajTekst();
				pac.Ime = ime;

				Console.Write("Unesi Prezime :");
				string prezime = IO.OcitajTekst();
				pac.Prezime = prezime;

				ClanDAO.Update(Program.conn, pac);
			}
		}

		// PRONADJI PO ID UI
		public static Clan PronadjiClana()
		{
			Clan retVal = null;
			Console.WriteLine("Unesi Id Clana: ");
			int idPacijenta = IO.OcitajCeoBroj();
			retVal = ClanDAO.GetById(Program.conn, idPacijenta);

			if (retVal == null)
				Console.WriteLine("Pacijent sa id " + idPacijenta
						 + " ne postoji u evidenciji");
			return retVal;
		}

		// BRISANJE UI
		private static void BrisanjePodatakaOClanu()
		{
			Clan pacijent = PronadjiClana();
			if (pacijent != null)
			{
				ClanDAO.Delete(Program.conn, pacijent.Id);
			}
		}

		// IZNAJMLJIVANJE KNJIGE

		private static void IznajmljivanjeKnjige()
		{
			Clan clan;
			Knjiga knjiga;
			List<Knjiga> knjige;

			knjige = KnjigaDAO.GetAll(Program.conn);

			Console.WriteLine("Izaberite knjigu za iznajmljivanje: ");

			foreach(Knjiga k in knjige)
			{
				Console.WriteLine(k);
			}

			Console.WriteLine("Unesite id knjige koju zelite da iznajmite: ");
			int idKnjige = IO.OcitajCeoBroj();

			knjiga = KnjigaDAO.GetById(Program.conn, idKnjige);
			if ( knjiga.ClanId == null)
			{
				Console.WriteLine("Unesi Id Clana koji zeli da iznajmi knjigu: ");
				int idCLana = IO.OcitajCeoBroj();
				clan = ClanDAO.GetById(Program.conn, idCLana);

				int broj = KnjigaDAO.GetAllCount(Program.conn, idCLana);			
			
				if(broj >= 2)
				{
					Console.WriteLine("Vec ste iznajmili maksimalni broj knjiga!");
					return;
				}else
				{
					knjiga.ClanId = idCLana;
					KnjigaDAO.Update(Program.conn, knjiga);
				}

			}
			else
			{
				Console.WriteLine("Knjiga je vec iznajmljena! ");
			}

		}

	}
}
