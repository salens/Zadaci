﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet03Modul1Test04.Dao;
using DotNet03Modul1Test04.Model;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;

namespace DotNet03Modul1Test04.PDF
{
	class KreirajIzvestajPDF
	{
		// Funkcija koja trazi putanju do 'data' direktorijuma unutar projekta
		static string GetDataDirectory()
		{
			// Prvo nalazimo putanju same aplikacije (.exe fajla), sto bi trebalo da je u:
			// (folder projekta)\\bin\\Debug\\
			string trenutnaPutanja = Directory.GetCurrentDirectory();

			// Dva puta menjamo putanju na gore, da bismo izasli iz foldera 'bin\\Debug' i ostali u
			// folderu projekta
			string putanjaProjekta = new DirectoryInfo(trenutnaPutanja).Parent.Parent.FullName;

			// Posle ovoga ulazimo u folder 'data' ciju putanju trazimo
			string DataDirPath = putanjaProjekta + Path.DirectorySeparatorChar + "data" +
				Path.DirectorySeparatorChar;

			if (!Directory.Exists(DataDirPath))
			{
				// Try to create the directory.
				DirectoryInfo di = Directory.CreateDirectory(DataDirPath);
			}

			return DataDirPath;


		}
		public bool SnimiIzvestaj()
		{
			bool retVal = false;

			try
			{
				List<Knjiga> Lista = KnjigaDAO.GetAll(Program.conn);
				//kreiranje pdf doc
				PdfDocument pdfdoc = new PdfDocument();
				pdfdoc.Info.Title = "Export liste knjiga";

				//kreiranje strane u doc-u
				PdfPage pdfpage = pdfdoc.AddPage();
				pdfpage.Size = PageSize.Letter;

				//Get an XGraphics object for drawing
				XGraphics xGrap = XGraphics.FromPdfPage(pdfpage);

				////Create Fonts
				XFont titlefont = new XFont("Calibri", 20, XFontStyle.Regular);
				XFont tableheader = new XFont("Calibri", 15, XFontStyle.Bold);
				XFont bodyfont = new XFont("Calibri", 11, XFontStyle.Regular);

				//Draw the text
				double x = 250;
				double y = 50;
				double width = 600;
				double height = 25;

				//Title Binding
				XTextFormatter textformater = new XTextFormatter(xGrap);  //Used to Hold the Custom text Area
				xGrap.DrawRectangle(XBrushes.White, new XRect(x, y, width, height));
				textformater.DrawString("This is Page Header", titlefont, XBrushes.Blue, new XRect(x, y, width, height), XStringFormats.TopLeft);

				//Table Declaration (height and width of columns declaration)
				y = y + 40; //Increasing the height  from top
				XRect snoColumn = new XRect(35, y, 70, height);
				XRect snoStudentName = new XRect(100, y, 250, height);
				XRect snoSAge = new XRect(280, y, 60, height);
				XRect snoMobilno = new XRect(350, y, 150, height);

				//Draw the table Row Borders
				xGrap.DrawRectangle(XPens.DarkSeaGreen, XBrushes.DarkSeaGreen, snoColumn); //Use different Color for Colum
				xGrap.DrawRectangle(XPens.DarkSeaGreen, XBrushes.DarkSeaGreen, snoStudentName);
				xGrap.DrawRectangle(XPens.DarkSeaGreen, XBrushes.DarkSeaGreen, snoSAge);
				xGrap.DrawRectangle(XPens.DarkSeaGreen, XBrushes.DarkSeaGreen, snoMobilno);

				//Writting Table Header Text

				textformater.DrawString(" Id", tableheader, XBrushes.Black, snoColumn);
				textformater.DrawString(" Ime", tableheader, XBrushes.Black, snoStudentName);
				textformater.DrawString(" Prezime", tableheader, XBrushes.Black, snoSAge);
				textformater.DrawString(" Specijalizacija", tableheader, XBrushes.Black, snoMobilno);

				//Writting the Body Content of Table

				//y = y + 30; //increase the height of content position from top
				//XRect snoColumnVal = new XRect(35, y, 70, height);
				//XRect snoStudentNameVal = new XRect(100, y, 250, height);
				//XRect snoSAgeVal = new XRect(280, y, 60, height);
				//XRect snoMobilnoVal = new XRect(350, y, 150, height);

				foreach (var knjiga in Lista)
				{
					y = y + 30;
					XRect snoColumnVal = new XRect(35, y, 70, height);
					XRect snoStudentNameVal = new XRect(100, y, 250, height);
					XRect snoSAgeVal = new XRect(280, y, 60, height);
					XRect snoMobilnoVal = new XRect(350, y, 150, height);

					textformater.DrawString(knjiga.Id.ToString(), tableheader, XBrushes.Black, snoColumnVal);
					textformater.DrawString(knjiga.Ime, tableheader, XBrushes.Black, snoStudentNameVal);
					textformater.DrawString(knjiga.Autor, tableheader, XBrushes.Black, snoSAgeVal);
					textformater.DrawString(knjiga.GodinaIzdavanja.ToString(), tableheader, XBrushes.Black, snoMobilnoVal);
				}
				//Save the Document
				string filename = GetDataDirectory() + "izvestaj_knjiga.pdf";  //Choose the path where the file want to save
				pdfdoc.Save(filename);
				Process.Start(filename);
				retVal = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}

			return retVal;
		}
	}
}