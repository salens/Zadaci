﻿using DotNet03Modul1Test04.Dao;
using DotNet03Modul1Test04.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DotNet03Modul1Test04.XML
{
	class KreiranjeIzvestajaOKnjigama
	{
		// Funkcija koja trazi putanju do 'data' direktorijuma unutar projekta
		static string GetDataDirectory()
		{
			// Prvo nalazimo putanju same aplikacije (.exe fajla), sto bi trebalo da je u:
			// (folder projekta)\\bin\\Debug\\
			string trenutnaPutanja = Directory.GetCurrentDirectory();

			// Dva puta menjamo putanju na gore, da bismo izasli iz foldera 'bin\\Debug' i ostali u
			// folderu projekta
			string putanjaProjekta = new DirectoryInfo(trenutnaPutanja).Parent.Parent.FullName;

			// Posle ovoga ulazimo u folder 'data' ciju putanju trazimo
			string DataDirPath = putanjaProjekta + Path.DirectorySeparatorChar + "data" +
				Path.DirectorySeparatorChar;

			if (!Directory.Exists(DataDirPath))
			{
				// Try to create the directory.
				DirectoryInfo di = Directory.CreateDirectory(DataDirPath);
			}

			return DataDirPath;
		}

		public bool SnimiIzvestaj()
		{
			bool retVal = false;

			try
			{
				List<Knjiga> knjige = KnjigaDAO.GetAll(Program.conn);

				XmlWriterSettings settings = new XmlWriterSettings();
				settings.Indent = true;

				using (XmlWriter writer = XmlWriter.Create(GetDataDirectory() + "izvestaj_knjiga.xml", settings))
				{
					writer.WriteStartElement("knjige");
					foreach (var knjiga in knjige)
					{
						writer.WriteStartElement("knjiga");
						writer.WriteElementString("KnjigaId", knjiga.Id.ToString());
						writer.WriteElementString("KnjigaIme", knjiga.Ime);
						writer.WriteElementString("KnjigaAutor", knjiga.Autor);
						writer.WriteElementString("KnjigaGodinaIzdavanja", Convert.ToString(knjiga.GodinaIzdavanja));
						writer.WriteEndElement();
					}
					writer.WriteEndElement();
					writer.Flush();
				}

				retVal = true;

			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}

			return retVal;
		}
	}
}
