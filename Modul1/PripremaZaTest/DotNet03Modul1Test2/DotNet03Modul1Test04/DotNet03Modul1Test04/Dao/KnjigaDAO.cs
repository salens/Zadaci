﻿using DotNet03Modul1Test04.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test04.Dao
{
	public class KnjigaDAO
	{

		// IZLISTAJ SVE DAO
		public static List<Knjiga> GetAll(SqlConnection conn)
		{
			List<Knjiga> retVal = new List<Knjiga>();
			int clanID;

			try
			{
				string queryKnjige= "SELECT id,ime,autor,godinaIzdavanja, clanId FROM knjiga";
				SqlCommand cmd = new SqlCommand(queryKnjige, conn);

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string autor = (string)rdr["autor"];
					int godina = (int)rdr["godinaIzdavanja"];

					if (rdr["clanId"] == DBNull.Value)
					{
						 clanID = 0;

					}
					else
					{
						clanID = (int)rdr["clanId"];
					}

					Knjiga knjiga = new Knjiga(id, ime, autor, godina, clanID);
					retVal.Add(knjiga);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;

		}

		// IZLISTAJ SVE IZBROJ DAO
		public static int GetAllCount(SqlConnection conn, int clanId)
		{
		   int retVal = 0;
			try
			{
				string queryKnjige = "SELECT COUNT(clanId) FROM knjiga WHERE clanId=" + clanId;
				SqlCommand cmd = new SqlCommand(queryKnjige, conn);
				 retVal = Convert.ToInt32(cmd.ExecuteScalar());
				
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;

		}

		// DODAVANJE DAO
		public static bool Add(SqlConnection conn, Knjiga knjiga)
		{
			bool retVal = false;

			try
			{
				string add = "INSERT INTO knjiga (ime,autor,godinaIzdavanja) values(@ime,@autor,@godinaIzdavanja)";

				SqlCommand cmd = new SqlCommand(add, conn);

				cmd.Parameters.AddWithValue("@ime", knjiga.Ime);
				cmd.Parameters.AddWithValue("@autor", knjiga.Autor);
				cmd.Parameters.AddWithValue("@godinaIzdavanja", knjiga.GodinaIzdavanja);

				if (cmd.ExecuteNonQuery() == 1)
				{
					retVal = true;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		//PRETRAGA PO ID DAO
		public static Knjiga GetById(SqlConnection conn, int idKnjiga)
		{
			Knjiga knjiga = null;
			try
			{
				string query = "SELECT id, ime, autor, godinaIzdavanja  " +
								 " FROM knjiga WHERE id = "
								+ idKnjiga;
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataReader rdr = cmd.ExecuteReader();

				if (rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string autor = (string)rdr["autor"];
					int godina = (int)rdr["godinaIzdavanja"];

					knjiga = new Knjiga(id, ime, autor, godina);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return knjiga;
		}

		//PRETRAGA PO NAZIVU DAO
		public static List<Knjiga> GetByNaziv(SqlConnection conn, string naziv)
		{
			List<Knjiga> retVal = new List<Knjiga>();
			Knjiga knjiga = null;
			try
			{
				string query = "SELECT id, ime, autor, godinaIzdavanja  " +
								 " FROM knjiga WHERE ime LIKE '%" + naziv + "%'";
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataReader rdr = cmd.ExecuteReader();

				while(rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string autor = (string)rdr["autor"];
					int godina = (int)rdr["godinaIzdavanja"];

					knjiga = new Knjiga(id, ime, autor, godina);

					retVal.Add(knjiga);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		//JEDNA KNJIGA
		public static Knjiga GetKnjigaByNaziv(SqlConnection conn, string naziv)
		{
			Knjiga knjiga = null;
			try
			{
				string query = "SELECT id FROM knjiga WHERE naziv = '"
							   + naziv + "'";
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataReader rdr = cmd.ExecuteReader();

				if (rdr.Read())
				{
					int id = (int)rdr["id"];
					knjiga = new Knjiga(id, naziv);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return knjiga;
		}

		//UPDATE DAO
		public static bool Update(SqlConnection conn, Knjiga knjiga)
		{
			bool retVal = false;
			try
			{
				string update = "UPDATE knjiga SET ime=@ime, " +
						"autor=@autor, godinaIzdavanja=@godinaIzdavanja, clanId=@clanId WHERE id=@id";
				SqlCommand cmd = new SqlCommand(update, conn);

				cmd.Parameters.AddWithValue("@id", knjiga.Id);
				cmd.Parameters.AddWithValue("@ime", knjiga.Ime);
				cmd.Parameters.AddWithValue("@autor", knjiga.Autor);
				cmd.Parameters.AddWithValue("@godinaIzdavanja", knjiga.GodinaIzdavanja);

				if(knjiga.ClanId != null)
					cmd.Parameters.AddWithValue("@clanId", knjiga.ClanId);


				if (cmd.ExecuteNonQuery() == 1)
					retVal = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		//BRISANJE DAO
		public static bool Delete(SqlConnection conn, int id)
		{
			bool retVal = false;
			try
			{
				string update = "DELETE FROM knjiga WHERE " +
						"id = " + id;
				SqlCommand cmd = new SqlCommand(update, conn);

				if (cmd.ExecuteNonQuery() == 1)
					retVal = true;

			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}
	}
}
