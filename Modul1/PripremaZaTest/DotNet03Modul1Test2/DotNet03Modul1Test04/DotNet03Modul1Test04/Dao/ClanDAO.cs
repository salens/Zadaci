﻿using DotNet03Modul1Test04.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test04.Dao
{
	class ClanDAO
	{
		// IZLISTAJ SVE DAO
		public static List<Clan> GetAll(SqlConnection conn)
		{
			List<Clan> retVal = new List<Clan>();

			try
			{
				string queryClanovi = "SELECT id,ime,prezime FROM clan";
				SqlCommand cmd = new SqlCommand(queryClanovi, conn);

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string prezime = (string)rdr["prezime"];


					Clan clan = new Clan(id, ime, prezime);
					retVal.Add(clan);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;

		}

		// DODAVANJE DAO
		public static bool Add(SqlConnection conn, Clan clan)
		{
			bool retVal = false;

			try
			{
				string add = "INSERT INTO clan (ime,prezime) values(@ime,@prezime)";

				SqlCommand cmd = new SqlCommand(add, conn);

				cmd.Parameters.AddWithValue("@ime", clan.Ime);
				cmd.Parameters.AddWithValue("@prezime", clan.Prezime);

				if (cmd.ExecuteNonQuery() == 1)
				{
					retVal = true;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		//PRETRAGA PO ID DAO
		public static Clan GetById(SqlConnection conn, int idClan)
		{
			Clan clan = null;
			try
			{
				string query = "SELECT id, ime, prezime " +
								 " FROM clan WHERE id = "
								+ idClan;
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataReader rdr = cmd.ExecuteReader();

				if (rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string prezime = (string)rdr["prezime"];


					clan = new Clan(id, ime, prezime);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return clan;
		}

		//UPDATE DAO
		public static bool Update(SqlConnection conn, Clan clan)
		{
			bool retVal = false;
			try
			{
				string update = "UPDATE clan SET ime=@ime, " +
						"prezime=@prezime WHERE id=@id";
				SqlCommand cmd = new SqlCommand(update, conn);

				cmd.Parameters.AddWithValue("@id", clan.Id);
				cmd.Parameters.AddWithValue("@ime", clan.Ime);
				cmd.Parameters.AddWithValue("@prezime", clan.Prezime);

				if (cmd.ExecuteNonQuery() == 1)
					retVal = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		//BRISANJE DAO
		public static bool Delete(SqlConnection conn, int id)
		{
			bool retVal = false;
			try
			{
				string update = "DELETE FROM clan WHERE " +
						"id = " + id;
				SqlCommand cmd = new SqlCommand(update, conn);

				if (cmd.ExecuteNonQuery() == 1)
					retVal = true;

			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

	}
}
