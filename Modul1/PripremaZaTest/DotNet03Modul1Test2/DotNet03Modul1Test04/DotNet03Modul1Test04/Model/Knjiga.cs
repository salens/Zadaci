﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test04.Model
{
	public class Knjiga
	{
		private int id;
		private string ime;
		private string autor;
		private int godinaIzdavanja;
		private int? clanId;

		public int Id { get => id; set => id = value; }
		public string Ime { get => ime; set => ime = value; }
		public string Autor { get => autor; set => autor = value; }
		public int GodinaIzdavanja { get => godinaIzdavanja; set => godinaIzdavanja = value; }
		public int? ClanId { get => clanId; set => clanId = value; }

		public Knjiga()
		{
		}
		public Knjiga(int id, string ime)
		{
			this.id = id;
			this.ime = ime;

		}

		public Knjiga(int id, string ime, string autor, int godinaIzdavanja, int? clanId)
		{
			this.id = id;
			this.ime = ime;
			this.autor = autor;
			this.godinaIzdavanja = godinaIzdavanja;
			this.clanId = clanId;
		}

		public Knjiga(int id, string ime, string autor, int godinaIzdavanja)
		{
			this.id = id;
			this.ime = ime;
			this.autor = autor;
			this.godinaIzdavanja = godinaIzdavanja;
		}

		public Knjiga(string ime, string autor, int godinaIzdavanja, int clanId)
		{
			this.ime = ime;
			this.autor = autor;
			this.godinaIzdavanja = godinaIzdavanja;
			this.clanId = clanId;
		}

		public Knjiga(string ime, string autor, int godinaIzdavanja)
		{
			this.ime = ime;
			this.autor = autor;
			this.godinaIzdavanja = godinaIzdavanja;
		}

		public Knjiga(int clanId)
		{
			this.clanId = clanId;
		}

		public override string ToString()
		{
			string s = "Id " + Id + ", Ime: " + Ime + ", Autor: " + Autor + " ,Godina Izdavanja: " + GodinaIzdavanja;
			return s;
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
