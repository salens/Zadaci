﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test04.Model
{
	public class Clan
	{
		private int id;
		private string ime;
		private string prezime;

		public int Id { get => id; set => id = value; }
		public string Ime { get => ime; set => ime = value; }
		public string Prezime { get => prezime; set => prezime = value; }

		public Clan()
		{
		}

		public Clan(int id, string ime, string prezime)
		{
			this.id = id;
			this.ime = ime;
			this.prezime = prezime;
		}

		public Clan(string ime, string prezime)
		{
			this.ime = ime;
			this.prezime = prezime;
		}

		public override string ToString()
		{
			string s = "Id " + Id + ", Ime: " + Ime + ", Prezime: " + Prezime;
			return s;
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
