﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
namespace KnjigaUnitTest
{
	[TestClass]
	public class UnitTest1
	{

		// ZA DB KONEKCIJU

		//static string connectionStringNaKursu = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;User ID=sa;Password=SqlServer2016;MultipleActiveResultSets=True";
		static string connectionStringZaPoKuci = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;Integrated Security=True;MultipleActiveResultSets=True";

		private SqlConnection OpenConnection()
		{
			SqlConnection conn = new SqlConnection(connectionStringZaPoKuci);
			conn.Open();
			return conn;
		}


		[TestMethod]
		public void TestDodavanjeKnjige()
		{
			SqlConnection conn = null;
			string testNaziv = "Test_Naziv";
			try
			{
				// Konekcija na bazu
				conn = OpenConnection();

				// Ako slucajno postoji ovakav predmet u bazi, javiti gresku
				// Ne zelimo da test obrise predmet iz baze
				Clan p = 
				if (p != null)
				{
					Assert.Fail("Vec postoji predmet sa nazivom {0}. Promeniti testni naziv!", testNaziv);
				}

				// Probamo da dodamo predmet i proverimo da li funkcija vraca true
				p = new Predmet(-1, testNaziv);
				Assert.IsTrue(PredmetDAO.Add(conn, p), "Dodavanje predmeta nije uspelo!");
			}
			finally
			{
				if (conn != null)
				{
					Predmet p = PredmetDAO.GetPredmetByNaziv(conn, testNaziv);
					PredmetDAO.Delete(conn, p.id);
					conn.Close();
				}
			}
		}
	}
}
