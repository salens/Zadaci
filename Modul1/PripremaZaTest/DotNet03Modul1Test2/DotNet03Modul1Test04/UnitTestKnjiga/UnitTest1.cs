﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNet03Modul1Test04;
using DotNet03Modul1Test04.Model;
using System.Data.SqlClient;
using DotNet03Modul1Test04.Dao;
using System.Collections.Generic;

namespace UnitTestKnjiga
{
	[TestClass]
	public class UnitTest1
	{
		//static string connectionStringNaKursu = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;User ID=sa;Password=SqlServer2016;MultipleActiveResultSets=True";
		static string connectionStringZaPoKuci = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;Integrated Security=True;MultipleActiveResultSets=True";

		private SqlConnection OpenConnection()
		{
			SqlConnection conn = new SqlConnection(connectionStringZaPoKuci);
			conn.Open();
			return conn;
		}


		[TestMethod]
		public void TestKnjigaAdd()
		{
			SqlConnection conn = null;
			string testNaziv = "Ko za inat";

			Knjiga k;

			try
			{
				// Konekcija na bazu
				conn = OpenConnection();

				List<Knjiga> knjige = KnjigaDAO.GetByNaziv(conn, testNaziv);

				foreach (Knjiga knj in knjige)
				{
					if (knj != null)
					{
						Assert.Fail("Vec postoji knjiga sa nazivom {0}. Promeniti testni naziv!", testNaziv);
			
					}

				}
		

				// Probamo da dodamo predmet i proverimo da li funkcija vraca true
				k = new Knjiga(testNaziv, "Bane Banic", 2007);
				Assert.IsTrue(KnjigaDAO.Add(conn, k), "Dodavanje knjige nije uspelo!");
			}
			finally
			{
		
				if (conn != null)
				{
					Knjiga p = KnjigaDAO.GetKnjigaByNaziv(conn, testNaziv);
					if (p != null)
					{
					
						KnjigaDAO.Delete(conn, p.Id);
					}
					conn.Close();
				}
			}
		}
	}
}
