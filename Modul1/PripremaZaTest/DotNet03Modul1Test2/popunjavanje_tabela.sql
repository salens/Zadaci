use DotNetKurs

insert into clan(ime,prezime)
values('Jovana','Serbedzija'),
('Macko','Macic'),
('Rade','Radic')

insert into knjiga(ime, autor,godinaIzdavanja,clanId)
values('Jesen stize dunjo moja','Stanislav Sencanski',1982,1),
('Jesen stize dunjo moja','Stanislav Sencanski',2007,2),
('Kestenje je moje','Bane Banic',1982),
('Varljivo leto','Aleksandar Radovic',1982,3),
('Napacena dusa','Micko Micic',1998),
('Sve mirise na nju','Stanislav Brankovski',1988)
