use DotNetKurs

drop table clan
drop table knjiga

create table clan(
	id int primary key identity(1,1),
	ime nvarchar(50),
	prezime nvarchar(60),

)

create table knjiga(
	id int primary key identity(1,1),
	ime nvarchar(50),
	autor nvarchar(50),
	godinaIzdavanja int,
	clanId int,
	foreign key(clanId) references clan(id) on delete cascade
)
