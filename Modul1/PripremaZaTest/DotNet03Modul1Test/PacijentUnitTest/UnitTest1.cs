﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;

namespace PacijentUnitTest
{
	[TestClass]
	public class UnitTest1
	{
		// ZA DB KONEKCIJU
		public static SqlConnection conn;

		[TestMethod]
		public void TestDodavanjeNovogPacijenta()
		{
			bool retVal = false;

			string connectionStringZaPoKuci = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;Integrated Security=True;MultipleActiveResultSets=True";

			try
			{
				//connection to DB
				conn = new SqlConnection(connectionStringZaPoKuci);
				conn.Open();

				string add = "INSERT INTO pacijent (ime,prezime,pol,brojGodina,lekarId) values(@ime,@prezime,@pol,@brojGodina,@lekarId)";

				SqlCommand cmd = new SqlCommand(add, conn);

				cmd.Parameters.AddWithValue("@ime", "Marko");
				cmd.Parameters.AddWithValue("@prezime", "Makic");
				cmd.Parameters.AddWithValue("@pol", "Musko");
				cmd.Parameters.AddWithValue("@brojGodina", 66);
				cmd.Parameters.AddWithValue("@lekarId", 8);

				if (cmd.ExecuteNonQuery() == 1)
				{
					retVal = true;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}

			Assert.IsTrue(retVal, "Nije uspesno dodat dodat pacijent!");
		}
	}
}
