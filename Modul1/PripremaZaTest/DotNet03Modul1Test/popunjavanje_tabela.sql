use DotNetKurs

insert into lekar(ime,prezime,specijalizacija)
values('Jovana','Serbedzija','Radiolog'),
('Macko','Macic','Pulmolog'),
('Rade','Radic','Ginekolog')

insert into pacijent(ime, prezime,pol,brojGodina,lekarId)
values('Stanislav','Sencanski','Musko',36,1),
('Bane','Banic','Musko',24,1),
('Ratko','Radic','Musko',50,3),
('Sale','Sasic','Musko',42,2),
('Radmila','Karic','Zensko',45,2),
('Vesna','Rodic','Zensko',25,1)
