﻿using DotNet03Modul1Test.Ui;
using DotNet03Modul1Test.Utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test
{
	class Program
	{
		public static SqlConnection conn;

		static void UcitavanjeKonekcije()
		{
			//string connectionStringNaKursu = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;User ID=sa;Password=singidunum;MultipleActiveResultSets=True";
			string connectionStringZaPoKuci = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;Integrated Security=True;MultipleActiveResultSets=True";

			try
			{
				//connection to DB
				conn = new SqlConnection(connectionStringZaPoKuci);
				conn.Open();

			}
			catch (Exception e)
			{

				Console.WriteLine(e.ToString());
			}
		}
		static void Main(string[] args)
		{
			UcitavanjeKonekcije();

			int odluka = -1;

			while(odluka != 0)
			{
				IspisiMeni();

				Console.WriteLine("Opcija: ");

				odluka = IO.OcitajCeoBroj();

				switch (odluka)
				{
					case 0:
						Console.WriteLine("Izlaz iz programa");
						break;
					case 1:
						LekarUI.Menu();
						break;
					case 2:
						PacijentUI.Menu();
						break;

					default:
						Console.WriteLine("Nepostojeca komanda");
						break;
				}
			}
		}

		private static void IspisiMeni()
		{
			Console.WriteLine("Rad bolnice Meni: ");
			Console.WriteLine("\tOpcija broj 1 - rad sa Lekarima");
			Console.WriteLine("\tOpcija broj 2 - rad sa Pacijentima");
			Console.WriteLine("\t\t ...");
			Console.WriteLine("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");

		}
	}
}
