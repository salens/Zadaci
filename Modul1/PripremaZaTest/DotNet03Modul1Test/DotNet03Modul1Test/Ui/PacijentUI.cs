﻿using DotNet03Modul1Test.Dao;
using DotNet03Modul1Test.Model;
using DotNet03Modul1Test.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test.Ui
{
	class PacijentUI
	{
		internal static void Menu()
		{
			int odluka = -1;
			while (odluka != 0)
			{
				IspisiMenu();
				Console.Write("opcija:");
				odluka = IO.OcitajCeoBroj();
				switch (odluka)
				{
					case 0:
						Console.WriteLine("Izlaz");
						break;
					case 1:
						IspisiSvePacijente();
						break;
					case 2:
						IspisiSvePacijenteINjegovogLekara();
						break;
					case 3:
						UnosNovogPacijenta();				
						break;
					case 4:
						IzmenaPodatakaOPacijentu();
						break;
					case 5:
						BrisanjePodatakaOPacijentu();
						break;
					default:
						Console.WriteLine("Nepostojeca komanda");
						break;
				}
			}
		}

		// MENI UI
		public static void IspisiMenu()
		{
			Console.WriteLine("Rad sa pacijentima - opcije:");
			Console.WriteLine("\tOpcija broj 1 - ispis svih Pacijenata");
			Console.WriteLine("\tOpcija broj 2 - ispis svih Pacijenata ukljucujuci i lekara");
			Console.WriteLine("\tOpcija broj 3 - unos novog Pacijenta");
			Console.WriteLine("\tOpcija broj 4 - izmena Pacijenta");
			Console.WriteLine("\tOpcija broj 5 - brisanje Pacijenta");
			Console.WriteLine("\t\t ...");
			Console.WriteLine("\tOpcija broj 0 - IZLAZ");
		}

		// ISPIS SVE UI
		private static void IspisiSvePacijente()
		{
			List<Pacijent> pacijenti = PacijentDAO.GetAll(Program.conn);
			for (int i = 0; i < pacijenti.Count; i++)
			{
				Console.WriteLine(pacijenti[i]);

			}
		}

		// ISPIS SVIH I NJIHOVIH LEKARA
		private static void IspisiSvePacijenteINjegovogLekara()
		{
			List<Pacijent> pacijenti = PacijentDAO.GetAll(Program.conn);
			Lekar lekar;
			
			for (int i = 0; i < pacijenti.Count; i++)
			{
				lekar = LekarDAO.GetLekarById(Program.conn, pacijenti[i].LekarId);
				Console.WriteLine(pacijenti[i] + "\n" + "Lekar: " + lekar.ToString());
		
			}
		}

		// UNOS NOVOG UI
		private static void UnosNovogPacijenta()
		{
			Console.Write("Unesi ime:");
			string Ime = IO.OcitajTekst();
			Console.Write("Unesi prezime:");
			string Prezime = IO.OcitajTekst();
			Console.Write("Unesi pol:");
			string Pol = IO.OcitajTekst();
			Console.Write("Unesi godine:");
			int Godine = IO.OcitajCeoBroj();
			Console.Write("Unesite id lekara iz liste: " + "\n");

			List<Lekar> lekari = LekarDAO.GetAll(Program.conn);

			foreach (Lekar lekar in lekari)
			{
				Console.WriteLine(lekar + "\n");
			}

			int IdLekara = IO.OcitajCeoBroj();

			Pacijent pacijent = new Pacijent(Ime, Prezime, Pol, Godine, IdLekara);
			PacijentDAO.AddPacijent(Program.conn, pacijent);

		}

		// IZMENA UI
		private static void IzmenaPodatakaOPacijentu()
		{
			Pacijent pac = PronadjiPacijenta();
			if (pac != null)
			{
				Console.Write("Unesi novo Ime :");
				string ime = IO.OcitajTekst();
				pac.Ime = ime;

				Console.Write("Unesi Prezime :");
				string prezime = IO.OcitajTekst();
				pac.Prezime = prezime;

				Console.Write("Unesi Pol:");
				string pol = IO.OcitajTekst();
				pac.Pol = pol;

				Console.Write("Unesi Godine:");
				int Godine = IO.OcitajCeoBroj();
				pac.BrojGodina = Godine;

				Console.Write("Unesi id lekara:");
				int idLekar = IO.OcitajCeoBroj();
				pac.LekarId = idLekar;

				PacijentDAO.Update(Program.conn, pac);
			}
		}

		// PRONADJI PO ID UI
		public static Pacijent PronadjiPacijenta()
		{
			Pacijent retVal = null;
			Console.WriteLine("Unesi Id pacijenta: ");
			int idPacijenta = IO.OcitajCeoBroj();
			retVal = PacijentDAO.GetPacijentById(Program.conn, idPacijenta);

			if (retVal == null)
				Console.WriteLine("Pacijent sa id " + idPacijenta
						 + " ne postoji u evidenciji");
			return retVal;
		}

		// BRISANJE UI
		private static void BrisanjePodatakaOPacijentu()
		{
			Pacijent pacijent = PronadjiPacijenta();
			if (pacijent != null)
			{
				PacijentDAO.Delete(Program.conn, pacijent.Id);
			}
		}
		
	}
}
