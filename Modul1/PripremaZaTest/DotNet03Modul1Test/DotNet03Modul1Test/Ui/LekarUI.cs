﻿using DotNet03Modul1Test.Dao;
using DotNet03Modul1Test.Model;
using DotNet03Modul1Test.Utils;
using DotNet03Modul1Test.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test.Ui
{
	class LekarUI
	{
		internal static void Menu()
		{
			int odluka = -1;
			while (odluka != 0)
			{
				IspisiMenu();
				Console.Write("opcija:");
				odluka = IO.OcitajCeoBroj();
				switch (odluka)
				{
					case 0:
						Console.WriteLine("Izlaz");
						break;
					case 1:
						IspisiSveLekare();
						break;
					case 2:
						IspisiSveLekareINjegePacijente();
						break;
					case 3:
						UnosNovogLekara();
						break;
					case 4:
						IzmenaPodatakaOLekaru();
						break;
					case 5:
						BrisanjePodatakaOLekaru();
						break;
					case 6:
						KreiranjeIzvestajaOLekaru();
						break;
					default:
						Console.WriteLine("Nepostojeca komanda");
						break;
				}
			}
		}


		// MENI UI
		public static void IspisiMenu()
		{
			Console.WriteLine("Rad sa pacijentima - opcije:");
			Console.WriteLine("\tOpcija broj 1 - ispis svih Lekara");
			Console.WriteLine("\tOpcija broj 2 - ispis svih Lekara ukljucujuci i Pacijente");
			Console.WriteLine("\tOpcija broj 3 - unos novog Lekara");
			Console.WriteLine("\tOpcija broj 4 - izmena Lekara");
			Console.WriteLine("\tOpcija broj 5 - brisanje Lekara");
			Console.WriteLine("\tOpcija broj 6 - Snimanje Izvestaja Lekara u XML");
			Console.WriteLine("\t\t ...");
			Console.WriteLine("\tOpcija broj 0 - IZLAZ");
		}

		// ISPIH SVIH UI
		private static void IspisiSveLekare()
		{
			List<Lekar> sviLekari = LekarDAO.GetAll(Program.conn);

			for (int i = 0; i < sviLekari.Count; i++)
			{
				Console.WriteLine(sviLekari[i]);
			}
		}
		// ISPIS SVIH I NJIHOVIH PACIJENATA UI
		private static void IspisiSveLekareINjegePacijente()
		{
			List<Lekar> lekari = LekarDAO.GetAll(Program.conn);
			List<Pacijent> sviPacijenti = PacijentDAO.GetAll(Program.conn);

			for (int i = 0; i < lekari.Count; i++)
			{
				Console.WriteLine(lekari[i]);

				for (int j = 0; j < sviPacijenti.Count; j++)
				{
					if(lekari[i].Id == sviPacijenti[j].LekarId)
					{
						Console.WriteLine(sviPacijenti[j]);
					}
				}

			}
		}

		//UNOS NOVOG UI
		private static void UnosNovogLekara()
		{
			Console.Write("Unesi ime:");
			string Ime = IO.OcitajTekst();
			Console.Write("Unesi prezime:");
			string Prezime = IO.OcitajTekst();
			Console.Write("Unesi specijalizaciju:");
			string Specijalizacija = IO.OcitajTekst();


			Lekar lekar = new Lekar(Ime, Prezime, Specijalizacija);
			LekarDAO.AddLekar(Program.conn, lekar);
		}
		// IZMENA UI
		private static void IzmenaPodatakaOLekaru()
		{
			Lekar lekar = PronadjiLekara();
			if (lekar != null)
			{
				Console.Write("Unesi novo Ime :");
				string ime = IO.OcitajTekst();
				lekar.Ime = ime;

				Console.Write("Unesi Prezime :");
				string prezime = IO.OcitajTekst();
				lekar.Prezime = prezime;

				Console.Write("Unesi Specijalizaciju:");
				string specijalizacija = IO.OcitajTekst();
				lekar.Specijalizacija = specijalizacija;

				LekarDAO.Update(Program.conn, lekar);
			}
		}

		// PRONADJI PO ID UI
		public static Lekar PronadjiLekara()
		{
			Lekar retVal = null;
			Console.WriteLine("Unesi Id lekara: ");
			int idLekara= IO.OcitajCeoBroj();
			retVal = LekarDAO.GetLekarById(Program.conn, idLekara);

			if (retVal == null)
				Console.WriteLine("Lekar sa id " + idLekara
						 + " ne postoji u evidenciji");
			return retVal;
		}
		// BRISANJE UI
		private static void BrisanjePodatakaOLekaru()
		{
			Lekar lekar = PronadjiLekara();
			if (lekar != null)
			{
				LekarDAO.Delete(Program.conn, lekar.Id);
			}
		}

		// XML IZVESTAJ
		private static void KreiranjeIzvestajaOLekaru()
		{
			KreiranjeIzvestajaOLekaru kreirajIzvestaj = new KreiranjeIzvestajaOLekaru();
			if(kreirajIzvestaj.SnimiIzvestaj() == true)
			{
				Console.WriteLine("Uspesno snimljen izvestaj!");
			}
			else
			{
				Console.WriteLine("Izvestaj nije snimljen!");
			}
		}

	}
}
