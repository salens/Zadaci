﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test.Model
{
	class Lekar
	{
		int id;
		string ime;
		string prezime;
		string specijalizacija;

		public int Id { get => id; set => id = value; }
		public string Ime { get => ime; set => ime = value; }
		public string Prezime { get => prezime; set => prezime = value; }
		public string Specijalizacija { get => specijalizacija; set => specijalizacija = value; }

		public Lekar()
		{
		}

		public Lekar(int id, string ime, string prezime, string specijalizacija)
		{
			this.id = id;
			this.ime = ime;
			this.prezime = prezime;
			this.specijalizacija = specijalizacija;
		}
		public Lekar(string ime, string prezime, string specijalizacija)
		{
			this.ime = ime;
			this.prezime = prezime;
			this.specijalizacija = specijalizacija;
		}


		public override string ToString()
		{
			string s = "Id " + Id + ", Ime: " + Ime + ", Prezime: " + Prezime + ", Specijalizacija: " + Specijalizacija;
			return s;
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}
	}
}
