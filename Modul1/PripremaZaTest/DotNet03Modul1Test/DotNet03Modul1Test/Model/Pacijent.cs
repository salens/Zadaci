﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test.Model
{
	class Pacijent
	{
		int id;
		string ime;
		string prezime;
		string pol;
		int brojGodina;
		int lekarId;


		public int Id { get => id; set => id = value; }
		public string Ime { get => ime; set => ime = value; }
		public string Prezime { get => prezime; set => prezime = value; }
		public string Pol { get => pol; set => pol = value; }
		public int BrojGodina { get => brojGodina; set => brojGodina = value; }
		public int LekarId { get => lekarId; set => lekarId = value; }

		public Pacijent()
		{
		}

		public Pacijent(int id, string ime, string prezime, string pol, int brojGodina, int lekarID)
		{
			this.id = id;
			this.ime = ime;
			this.prezime = prezime;
			this.pol = pol;
			this.brojGodina = brojGodina;
			this.lekarId = lekarID;
		}

		public Pacijent(string ime, string prezime, string pol, int brojGodina, int lekarID)
		{
			this.ime = ime;
			this.prezime = prezime;
			this.pol = pol;
			this.brojGodina = brojGodina;
			this.lekarId = lekarID;
		}


		public override string ToString()
		{
			string s = "Ime: " + Ime + " Prezime: " + Prezime + " Pol: " + Pol + " Broj Godina: " + BrojGodina;
			return s;
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

	}
}
