﻿using DotNet03Modul1Test.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test.Dao
{
	class PacijentDAO
	{
		// IZLISTAJ SVE DAO
		public static List<Pacijent> GetAll(SqlConnection conn)
		{
			List<Pacijent> retVal = new List<Pacijent>();

			try
			{
				string queryPacijenti = "SELECT id,ime,prezime,pol,brojGodina,lekarId FROM pacijent";
				SqlCommand cmd = new SqlCommand(queryPacijenti, conn);

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string prezime = (string)rdr["prezime"];
					string pol = (string)rdr["pol"];
					int brojGodina = (int)rdr["brojGodina"];
					int lekarID = (int)rdr["lekarId"];

					Pacijent pacijent = new Pacijent(id, ime, prezime, pol, brojGodina, lekarID);
					retVal.Add(pacijent);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;

		}

		// DODAVANJE DAO
		public static bool AddPacijent(SqlConnection conn, Pacijent pacijent)
		{
			bool retVal = false;

			try
			{
				string add = "INSERT INTO pacijent (ime,prezime,pol,brojGodina,lekarId) values(@ime,@prezime,@pol,@brojGodina,@lekarId)";

				SqlCommand cmd = new SqlCommand(add, conn);

				cmd.Parameters.AddWithValue("@ime", pacijent.Ime);
				cmd.Parameters.AddWithValue("@prezime", pacijent.Prezime);
				cmd.Parameters.AddWithValue("@pol", pacijent.Pol);
				cmd.Parameters.AddWithValue("@brojGodina", pacijent.BrojGodina);
				cmd.Parameters.AddWithValue("@lekarId", pacijent.LekarId);

				if (cmd.ExecuteNonQuery() == 1)
				{
					retVal = true;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		//PRETRAGA PO ID DAO
		public static Pacijent GetPacijentById(SqlConnection conn, int idPacijenta)
		{
			Pacijent pacijent = null;
			try
			{
				string query = "SELECT id, ime, prezime, pol, brojGodina, lekarId " +
								 " FROM pacijent WHERE id = "
								+ idPacijenta;
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataReader rdr = cmd.ExecuteReader();

				if (rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string prezime = (string)rdr["prezime"];
					string pol = (string)rdr["pol"];
					int brojGodina = (int)rdr["brojGodina"];
					int lekarID = (int)rdr["lekarId"];


					pacijent = new Pacijent(id, ime, prezime, pol, brojGodina, lekarID);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return pacijent;
		}

		//UPDATE DAO
		public static bool Update(SqlConnection conn, Pacijent pacijent)
		{
			bool retVal = false;
			try
			{
				string update = "UPDATE pacijent SET ime=@ime, " +
						"prezime=@prezime, pol=@pol, brojGodina=@brojGodina, lekarId=@lekarId WHERE id=@id";
				SqlCommand cmd = new SqlCommand(update, conn);

				cmd.Parameters.AddWithValue("@id", pacijent.Id);
				cmd.Parameters.AddWithValue("@ime", pacijent.Ime);
				cmd.Parameters.AddWithValue("@prezime", pacijent.Prezime);
				cmd.Parameters.AddWithValue("@pol", pacijent.Pol);
				cmd.Parameters.AddWithValue("@brojGodina", pacijent.BrojGodina);
				cmd.Parameters.AddWithValue("@lekarId", pacijent.LekarId);

				if (cmd.ExecuteNonQuery() == 1)
					retVal = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		//BRISANJE DAO
		public static bool Delete(SqlConnection conn, int id)
		{
			bool retVal = false;
			try
			{
				string update = "DELETE FROM pacijent WHERE " +
						"id = " + id;
				SqlCommand cmd = new SqlCommand(update, conn);

				if (cmd.ExecuteNonQuery() == 1)
					retVal = true;

			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}
	}
}
