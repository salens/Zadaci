﻿using DotNet03Modul1Test.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet03Modul1Test.Dao
{
	class LekarDAO
	{
		// SVI DAO
		public static List<Lekar> GetAll(SqlConnection conn)
		{
			List<Lekar> retVal = new List<Lekar>();

			try
			{
				string queryLekari = "SELECT id,ime,prezime,specijalizacija FROM lekar";
				SqlCommand cmd = new SqlCommand(queryLekari, conn);

				SqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string prezime = (string)rdr["prezime"];
					string specijalizacija = (string)rdr["specijalizacija"];
				
					Lekar lekar = new Lekar(id, ime, prezime, specijalizacija);
					retVal.Add(lekar);
				}
				rdr.Close();
			}
			catch (Exception e)
			{

				Console.WriteLine(e.ToString());
			}

			return retVal;
		}

		//PRETRAGA PO ID DAO
		public static Lekar GetLekarById(SqlConnection conn, int idLekara)
		{
			Lekar lekar = null;
			try
			{
				string query = "SELECT id, ime, prezime, specijalizacija" +
								 " FROM lekar WHERE id = "
								+ idLekara;
				SqlCommand cmd = new SqlCommand(query, conn);
				SqlDataReader rdr = cmd.ExecuteReader();

				if (rdr.Read())
				{
					int id = (int)rdr["id"];
					string ime = (string)rdr["ime"];
					string prezime = (string)rdr["prezime"];
					string specijalizacija = (string)rdr["specijalizacija"];


					lekar = new Lekar(id, ime, prezime, specijalizacija);
				}
				rdr.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return lekar;
		}

		// DODAVANJE NOVOG DAO
		public static bool AddLekar(SqlConnection conn, Lekar lekar)
		{
			bool retVal = false;

			try
			{
				string add = "INSERT INTO lekar (ime,prezime,specijalizacija) values(@ime,@prezime,@specijalizacija)";

				SqlCommand cmd = new SqlCommand(add, conn);

				cmd.Parameters.AddWithValue("@ime", lekar.Ime);
				cmd.Parameters.AddWithValue("@prezime", lekar.Prezime);
				cmd.Parameters.AddWithValue("@specijalizacija", lekar.Specijalizacija);


				if (cmd.ExecuteNonQuery() == 1)
				{
					retVal = true;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		// UPDATE DAO
		public static bool Update(SqlConnection conn, Lekar lekar)
		{
			bool retVal = false;
			try
			{
				string update = "UPDATE lekar SET ime=@ime, " +
						"prezime=@prezime, specijalizacija=@specijalizacija WHERE id=@id";
				SqlCommand cmd = new SqlCommand(update, conn);

				cmd.Parameters.AddWithValue("@id", lekar.Id);
				cmd.Parameters.AddWithValue("@ime", lekar.Ime);
				cmd.Parameters.AddWithValue("@prezime", lekar.Prezime);
				cmd.Parameters.AddWithValue("@specijalizacija", lekar.Specijalizacija);

				if (cmd.ExecuteNonQuery() == 1)
					retVal = true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}

		// BRISANJE DAO
		internal static bool Delete(SqlConnection conn, int id)
		{
			bool retVal = false;
			try
			{
				string update = "DELETE FROM lekar WHERE " +
						"id = " + id;
				SqlCommand cmd = new SqlCommand(update, conn);

				if (cmd.ExecuteNonQuery() == 1)
					retVal = true;

			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			return retVal;
		}
	}
}
