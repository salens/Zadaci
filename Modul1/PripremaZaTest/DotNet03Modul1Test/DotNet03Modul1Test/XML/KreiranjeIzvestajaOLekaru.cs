﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DotNet03Modul1Test.Dao;
using DotNet03Modul1Test.Model;

namespace DotNet03Modul1Test.XML
{

	class KreiranjeIzvestajaOLekaru
	{

		// Funkcija koja trazi putanju do 'data' direktorijuma unutar projekta
		static string GetDataDirectory()
		{
			// Prvo nalazimo putanju same aplikacije (.exe fajla), sto bi trebalo da je u:
			// (folder projekta)\\bin\\Debug\\
			string trenutnaPutanja = Directory.GetCurrentDirectory();

			// Dva puta menjamo putanju na gore, da bismo izasli iz foldera 'bin\\Debug' i ostali u
			// folderu projekta
			string putanjaProjekta = new DirectoryInfo(trenutnaPutanja).Parent.Parent.FullName;

			// Posle ovoga ulazimo u folder 'data' ciju putanju trazimo
			string DataDirPath = putanjaProjekta + Path.DirectorySeparatorChar + "data" +
				Path.DirectorySeparatorChar;

			return DataDirPath;
		}

		public bool SnimiIzvestaj()
		{
			bool retVal = false;

			try
			{
				List<Lekar> lekari = LekarDAO.GetAll(Program.conn);

				XmlWriterSettings settings = new XmlWriterSettings();
				settings.Indent = true;

				using (XmlWriter writer = XmlWriter.Create(GetDataDirectory() + "izvestaj_lekar.xml", settings))
				{
					writer.WriteStartElement("lekari");
					foreach (var lekar in lekari)
					{
						writer.WriteStartElement("lekar");
						writer.WriteElementString("LekarId", lekar.Id.ToString());
						writer.WriteElementString("LekarIme", lekar.Ime);
						writer.WriteElementString("LekarPrezime", lekar.Prezime);
						writer.WriteElementString("LekarSpecijalizacija", lekar.Specijalizacija);
						writer.WriteEndElement();
					}
					writer.WriteEndElement();
					writer.Flush();
				}

				retVal = true;

			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
		
			return retVal;
		}

	}
}
