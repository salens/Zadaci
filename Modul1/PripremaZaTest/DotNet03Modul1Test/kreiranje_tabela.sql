use DotNetKurs

drop table lekar
drop table pacijent

create table lekar(
	id int primary key identity(1,1),
	ime nvarchar(50),
	prezime nvarchar(50),
	specijalizacija nvarchar(60)
)

create table pacijent(
	id int primary key identity(1,1),
	ime nvarchar(50),
	prezime nvarchar(60),
	pol nvarchar(30),
	brojGodina int,
	lekarId int,
	foreign key(lekarId) references lekar(id) on delete cascade
)