﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termin08Primer02.Studenti
{
    class Studenti
    {
        static void Main(string[] args)
        {
            // Ostvarivanje konekcije; Bitno je obratiti paznju na parametre
            // Vise o parametrima se moze procitati na MSDN web stranici:
            // https://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqlconnection.connectionstring(v=vs.110).aspx

            // Ovaj string za konekciju je jednostavnije da koristite ako isprobavate bazu podataka kod kuce
            // On ne zahteva korisnicko ime i lozinku, vec povlaci prava pristupa od Vaseg Windows naloga na racunaru
            string connectionStringZaPoKuci = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;Integrated Security=True;MultipleActiveResultSets=True";

            // Ovaj connection string cemo koristiti u laboratorijama kursa. U njemu se eksplicitno zadaju korisnicko ime
            // i lozinka za instaliranu bazu podataka
            string connectionStringNaKursu = "Data Source=.\\SQLEXPRESS;Initial Catalog=DotNetKurs;User ID=sa;Password=SqlServer2016";

            // U ovom primeru je ostavljeno da se po defaultu prosledjuje connection string za laboratoriju kursa
            SqlConnection conn = new SqlConnection(connectionStringNaKursu);
            conn.Open();

            // Priprema komande
            SqlCommand cmd = new SqlCommand("select * from Studenti", conn);

            // Izvrsavanje komande
            SqlDataReader rdr = cmd.ExecuteReader();

            // Pregled rezultata. Obratiti paznju na speficicnost rada sa metodom
            // SqlDataReader.Read(). Posle svakog poziva te metode, objekat rdr ce
            // da se prebaci na sledeci red u rezultatima. Ukoliko nema vise redova,
            // rezultat ove metode bice null.
            Console.WriteLine("{0,-8} {1,-15} {2, -15} {3, -7}",
                "Indeks", "Ime", "Prezime", "Grad");
            Console.WriteLine("--------------------------------------------------------------");
            while (rdr.Read())
            {
                // Obratiti paznju da u ovom konkretnom primeru sve pretvaramo u stringove
                // jer nam je potreban samo ispis informacija na ekran
                string br_ind = rdr[0].ToString();
                string ime = rdr[1].ToString();
                string prezime = rdr[2].ToString();
                string pros = rdr[3].ToString();

                Console.WriteLine("{0,-8} {1,-15} {2, -15} {3, -7}",
                    br_ind, ime, prezime, pros);
            }
            Console.WriteLine("--------------------------------------------------------------");

            conn.Close();

            Console.WriteLine("Unesite index studenta: ");
            string br_in = Console.ReadLine();
            Console.WriteLine("Unesite ime studenta: ");
            string imeStudenta = Console.ReadLine();
            Console.WriteLine("Unesite prezime studenta: ");
            string prezimeStudenta = Console.ReadLine();
            Console.WriteLine("Unesite prosek studenta: ");
            string prosek = Console.ReadLine();
            conn.Open();
            Update(conn, br_in, imeStudenta, prezimeStudenta, prosek);



            Console.WriteLine("Program je izvrsen!");
            Console.ReadKey();


        }

        public static void Update(SqlConnection conn, string br_in, string imeStudenta, string prezimeStudenta, string prosek)
        {

            using (SqlCommand cmd =
               
               new SqlCommand("INSERT INTO Studenti" + "(broj_indeksa, ime, prezime, prosek)" + "VALUES(@brind, @ime, @prezime, @prosek)", conn))
            {
                cmd.Parameters.AddWithValue("@brind", br_in);
                cmd.Parameters.AddWithValue("@ime", imeStudenta);
                cmd.Parameters.AddWithValue("@prezime", prezimeStudenta);
                cmd.Parameters.AddWithValue("@prosek", prosek);

                cmd.ExecuteNonQuery();

            }
        }
    }
}
