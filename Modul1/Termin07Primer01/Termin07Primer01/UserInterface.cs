﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Termin07Primer01
{
    // Ovim definisemo 'Del' kao delegat koji ne prima parametre i nema
    // povratnu vrednost. 'Del' i dalje nije promenljiva, nego je tip
    // promenljive
    delegate void Del();

    class UserInterface
    {
        // opcija1 i opcija2 su promenljive tipa Del. Njima je moguce dodeliti
        // vrednosti.
        private Del opcija1;
        private Del opcija2;

        // Metoda kojom mozemo menjati promenljive opcija1 i opcija2
        public void AssignDelegate(int option, Del method)
        {
            if (option == 1)
            {
                opcija1 = method;
            }
            else if (option == 2)
            {
                opcija2 = method;
            }
            else
            {
                Console.WriteLine("Option must be 1 or 2!");
            }
        }

        // Ispis jednostavnog menija, upit za opciju, i izvrsavanje odabrane
        // opcije. Bitno je primetiti da prilikom kompajliranja se ne zna
        // koje tacno metode ce se izvrsiti kada se pozovu promenljive
        // delegati.
        public void Meni()
        {
            
            Console.WriteLine("Izaberite opciju 1 ili 2:");
            string input = Console.ReadLine();
            if (input == "1")
            {
                opcija1();
            }
            else if (input == "2")
            {
                opcija2();
            }
            else
            {
                Console.WriteLine("Unknown option!");
            }
        }
    }
}
