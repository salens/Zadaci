﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termin07Primer01
{
    class Program
    {
        // Dve metode koje zelimo da pridruzimo delegatima u klasi UserInterface
        static void StampanjeVremena()
        {
            Console.WriteLine("Trenutno vreme: " + DateTime.Now);
        }
        static void Pozdravljanje()
        {
            Console.WriteLine("Zdravo iz funkcije za pozdravljanje!");
        }

        static void Main(string[] args)
        {
            UserInterface ui = new UserInterface();

            // Pridruzivanje nasih metoda delegatima u klasi UserInterface
            ui.AssignDelegate(1, StampanjeVremena);
            ui.AssignDelegate(2, Pozdravljanje);

            ui.Meni();

            // Promena vrednosti delegata opcija2. Sada ce oba delegata
            // referencirati istu metodu; to ne predstavlja problem
            ui.AssignDelegate(2, StampanjeVremena);

            ui.Meni();

            Console.WriteLine("Program izvrsen!");
            Console.ReadKey();
        }
    }
}
