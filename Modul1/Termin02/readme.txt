﻿FTN Informatika
Kurs iz .NET Web programiranja
=================================

1. Opis vežbe
-------------------
- Upoznavanje sa formatima ispisa, radom sa nizovima, radom sa Stack i Heap memorijom, korišćenje funkcija, matematičke funkcije - klasa Math, Klase String, StringBuilder, klasama primitivnih tipova.


2. Sadržaj vežbe
-------------------
- formatiranje ispisa
- nizovi – jednodimenzionalni, višedimenzionalni, jagged, kombinovani
- skladištenje vrednosti primitivnitivnih tipova i vrednosti kreiranih sa operatorom new
- rad sa funkcijama
- rad sa stringovima, klasa String, metode String klase, parsiranje stringova, StringBuilder
- korišćenje klasa primitivnih tipova


3. Literatura
-------------------
materijali/Modul1Termin02.pdf
materijali/*.pdf

4. Primeri
-------------------
primeri\Modul1Termin02Primeri

5. Zadatak na času
-------------------
zadaci\Modul1Termin02 - Zadaci.pdf
zadaci\Modul1Termin02 - Dodatni zadaci.pdf 

6. Domaći zadatak
-------------------
Sve što ne stignete na času.


