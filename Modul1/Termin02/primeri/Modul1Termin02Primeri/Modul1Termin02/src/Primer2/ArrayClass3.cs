﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer2
{
    class ArrayClass3
    {
        static void Main(string[] args)
        {
            //definisanje visedimenzionalnih nizova
            int[,] C = new int[4, 3];

            // popunjavanje vrednosti matrice
            /*
             * 0 1 2
             * 1 2 3
             * 2 3 4
             * 3 4 5
             */
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    C[i, j] = i + j;
                }
            }
            
            //ispis clanova matrice C
            Console.WriteLine("Ispis matrice C");
            for (int i = 0; i < C.GetLength(0); i++)
            {
                for (int j = 0; j < C.GetLength(1); j++)
                {
                    Console.Write("\tC [" + i + "," + j + "] = " + C[i, j]);
                }
                Console.WriteLine();
            }

            Console.WriteLine("\nRad sa studentske sluzbe");

            String[] studenti = { "Pera", "Mika", "Laza" };

            //skraceni oblik, deklaracija i inicijalizacija
            //int[,] oceneStudenta = new int[3,3] {{1,2,3},{4,5,6},{7,8,9}};
            int[,] oceneStudenta = {
                { 10, 7 , 8} ,
                { 6 , 6, 8} ,
                { 7, 10, 10}
            };

            for (int i = 0; i < studenti.Length; i++)
            {
                Console.WriteLine("Student " + studenti[i] + " ima ocene ");
                for (int j = 0; j < oceneStudenta.GetLength(1); j++)
                {
                    Console.Write("\t" + oceneStudenta[i, j]);
                }
                Console.WriteLine();
            }

            //ispis svih ocena

            foreach (int el in oceneStudenta)
            {
                Console.Write(el + " ");
            }
            Console.WriteLine();

            System.Console.WriteLine("Ukupan broj elemenata visedimenzionalnog niza: " + oceneStudenta.Length);
            System.Console.WriteLine("Broj dimezija visedimenzionalnog niza: " + oceneStudenta.Rank);

            //definisanje visedimenzionalnih nizova razlicitih dimenzija - jagged nizovi
            int[][] jaggedArray = new int[3][];

            //vise nacina inicijalizacije podnizova
            jaggedArray[0] = new int[5];
            jaggedArray[1] = new int[4];
            jaggedArray[2] = new int[2];

            //postupno kreiranje niza uz pomoć for petlje
            int[][] jaggedArray1 = new int[3][];
            for (int i = 0; i < jaggedArray1.Length; i++)
            {
                jaggedArray1[i] = new int[i + 3]; // proizvoljno izabrane veličine i+3
            }


            jaggedArray[0] = new int[] { 1, 3, 5, 7, 9 };
            jaggedArray[1] = new int[] { 0, 2, 4, 6 };
            jaggedArray[2] = new int[] { 11, 22 };

            int[][] jaggedArray2 = new int[][]
               {
                    new int[] {1,3,5,7,9},
                    new int[] {0,2,4,6},
                    new int[] {11,22}
               };

            int[][] jaggedArray3 =
                {
                    new int[] {1,3,5,7,9},
                    new int[] {0,2,4,6},
                    new int[] {11,22}
                };

            jaggedArray3[0][1] = 88;

            Console.WriteLine("Ispis jagged nizova");
            for (int i = 0; i < jaggedArray.Length; i++)
            {
                for (int j = 0; j < jaggedArray[i].Length; j++)
                {
                    Console.Write(" " + jaggedArray[i][j]);
                }
                Console.WriteLine();
            }

            System.Console.WriteLine("Broj elemenata (podnizova) jagged niza: " + jaggedArray.Length);
            System.Console.WriteLine("Broj dimezija jagged niza: " + jaggedArray.Rank);

            //kombinovanje visedimenzionalnih i jagged nizova
            int[][,] MultiJaggedArray4 = new int[3][,]
                {
                    new int[,] { {1,3}, {5,7} },
                    new int[,] { {0,2}, {4,6}, {8,10} },
                    new int[,] { {11,22,33}, {99,88,77}, {0,9,18} }
                };

            //pristupanje elementu visedimenzionalnog jagged niza
            MultiJaggedArray4[0][1, 0] = 555;

            Console.WriteLine("Ispis kombinovanih nizova");
            for (int i = 0; i < MultiJaggedArray4.Length; i++)
            {
                for (int j = 0; j < MultiJaggedArray4[i].GetLength(0); j++)
                {
                    for (int k = 0; k < MultiJaggedArray4[i].GetLength(1); k++)
                    {
                        Console.Write(" " + MultiJaggedArray4[i][j, k]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine("\n");
            }

            //ispis uz pomoc foreach petlji
            //foreach (int[,] multiArray in MultiJaggedArray4)
            //{
            //    foreach (int num in multiArray)
            //    {
            //        Console.Write(" "+num);
            //    }
            //    Console.WriteLine();
            //}

            System.Console.WriteLine("Broj elemenata (visedimenzionalnih podnizova) jagged niza: " + MultiJaggedArray4.Length);
            System.Console.WriteLine("Broj dimezija kombinovanog niza: " + MultiJaggedArray4.Rank);

            System.Console.WriteLine("Broj elemenata prvog visedimenzionalnog niza u jagged nizu " + MultiJaggedArray4[0].Length);


            Console.ReadKey();
        }
    }
}
