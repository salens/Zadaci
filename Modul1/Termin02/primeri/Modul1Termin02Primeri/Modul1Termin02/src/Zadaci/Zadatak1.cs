﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.Zadatak_1
{
	class Zadatak1
	{
		static void Main(string[] args)
		{
			//Artikli prodavnice, cena artikla
			string kupljeniArtikli = "Prsak za ves,cena;500.34" + "\n" +
							 "Sladoled,cena;130" + "\n" +
							 "cokolada,cena;300" + "\n" +
							 "hleb,cena;30";

			string nazivProdavnice;
			double ukupnaCena = 0;
			double ukupnaCenaSaPDV;


			Console.WriteLine("Unesite naziv prodavnice:");
			nazivProdavnice = Console.ReadLine();

			string[] sviArtikli = kupljeniArtikli.Split('\n');

			for (int i = 0; i < sviArtikli.Length; i++)
			{
				Console.WriteLine("\n************************************");
				Console.WriteLine("Artikal -> " + sviArtikli[i]);

				String[] artikliPodaci = sviArtikli[i].Split(',');

				String nazivArtikla = artikliPodaci[0];
				String cenaArtikla = artikliPodaci[1];

				if (cenaArtikla.Length > 1)
				{
					String[] ceneArt = cenaArtikla.Split(';');
					double pare = 0.0;
					for (int j = 0; j < ceneArt.Length; j++)
					{

						String cena = ceneArt[1];

						pare = Double.Parse(cena);


					}
					ukupnaCena += pare;
				}
			}

			Console.WriteLine("\n");
			Console.WriteLine("Ukupna cena racuna bez PDV: " + ukupnaCena);
			ukupnaCenaSaPDV = ukupnaCena + ukupnaCena * 23 / 100;
			Console.WriteLine("Ukupna cena racuna sa PDV: " + ukupnaCenaSaPDV);

			Console.ReadKey();
		}
	}
}
