﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.StudentskaSluzba
{
	class StudentskaSluzba
	{
		static void Main(string[] args)
		{
			string nastavnici ="1,Petar,Petrović,Docent\n" +
							   "2,Jovan,Jovanović,Docent\n" +
							   "3,Marko,Marković,Asistent\n" +
							   "4,Nikola,Nikolić,Redovni Profesor\n" +
							   "5,Lazar,Lazić,Asistent";

			string predmeti =   "1,Matematika\n" +
								"2,Fizika\n" +
								"3,Elektrotehnika\n" +
								"4,Informatika";

			string studenti = "1,E1 01/2016,Jovanović,Zarko,Loznica\n" +
							  "2,E2 02/2015,Prosinečki,Strahinja,Novi Sad\n" +
						      "3,E2 33/2016,Savić,Nebojša,Inđija\n" +
							  "4,SW 36/2013,Sekulić,Ana,Niš\n" +
							  "5,E2 157/2013,Nedeljković,Vuk,Novi Sad\n" +
							  "6,E1 183/2013,Klainić,Jovana,Sombor\n" +
							  "7,E2 44/2015,Bojana,Panić,Sr.Mitrovica";


			Nastavnik.UcitajPodatke(nastavnici);
			Predmet.UcitajPodatke(predmeti);

			Console.WriteLine("Unesite sifru Nastavnika:");
			string sifraNastavnika = Console.ReadLine();
			Console.WriteLine("Unesite sifru Predmeta:");
			string sifraPredmeta = Console.ReadLine();

			Predaje.IspisiPodatkeUOdnosuNaIndetifikator(sifraNastavnika, sifraPredmeta, nastavnici, predmeti);

			Student.UcitajPodatke(studenti);

			// studenti po smeru
			Console.WriteLine("Da li zelite odredjene studente po Smeru?");
			Console.WriteLine("Za odredjenog pritisnite 'd', za izlazak iz Aplikacije 'n'");

			char odredjeniStudentiPoSmeru = Console.ReadKey().KeyChar;

			Student.OdredjeniStudentiPoSmeru(odredjeniStudentiPoSmeru, studenti);

			// studenti upisani po godini
			Console.WriteLine("Da li zelite studente da vidite upisane studente po Godini?");
			Console.WriteLine("Za ulaz 'd', za izlazak iz Aplikacije 'n'");

			char upisaniStudentiPoGodini = Console.ReadKey().KeyChar;
			Student.ispisUpisanihStudenataPoGodini(upisaniStudentiPoGodini,studenti);

			Console.ReadKey();
		}
	}
}
