﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.StudentskaSluzba
{
	class IspitnePrijave
	{
		int studentId;
		Student student;
		int predmetId;
		Predmet predmet;
		int rokId;
		IspitniRokovi ispitniRokovi;
		int teorija;
		int zadaci;

		public int StudentId { get => studentId; set => studentId = value; }
		public int PredmetId { get => predmetId; set => predmetId = value; }
		public int RokId { get => rokId; set => rokId = value; }
		public int Teorija { get => teorija; set => teorija = value; }
		public int Zadaci { get => zadaci; set => zadaci = value; }
		internal Student Student { get => student; set => student = value; }
		internal Predmet Predmet { get => predmet; set => predmet = value; }
		internal IspitniRokovi IspitniRokovi { get => ispitniRokovi; set => ispitniRokovi = value; }
	}
}
