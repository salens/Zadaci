﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.StudentskaSluzba
{
	class Pohadja
	{
		int studentId;
		Student student;
		int predmetId;
		Predmet predmet;

		public int StudentId { get => studentId; set => studentId = value; }
		public int PredmetId { get => predmetId; set => predmetId = value; }
		internal Student Student { get => student; set => student = value; }
		internal Predmet Predmet { get => predmet; set => predmet = value; }
	}
}
