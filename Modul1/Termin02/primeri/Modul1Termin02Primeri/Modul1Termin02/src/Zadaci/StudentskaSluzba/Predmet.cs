﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.StudentskaSluzba
{
	class Predmet
	{
		int predmetId;
		string naziv;

		public int PredmetId { get => predmetId; set => predmetId = value; }
		public string Naziv { get => naziv; set => naziv = value; }

		public static void UcitajPodatke(string predmeti)
		{
	    	Console.WriteLine("Svi Predmeti:");

			string[] sviPredmeti = predmeti.Split('\n');

			IspisiPodatke(sviPredmeti);
		}

		public static void IspisiPodatke(string[] sviPredmeti)
		{
			Predmet predmet = new Predmet();

			for (int i = 0; i < sviPredmeti.Length; i++)
			{
				
				Console.WriteLine("\n************************************");

				String[] svi = sviPredmeti[i].Split(',');

				predmet.PredmetId = Convert.ToInt32(svi[0]);
				predmet.Naziv = svi[1];


				Console.WriteLine("Predmet Naziv: " + predmet.Naziv);
			}
		}

	}
}
