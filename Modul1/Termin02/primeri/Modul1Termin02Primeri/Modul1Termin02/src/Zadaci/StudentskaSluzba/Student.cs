﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.StudentskaSluzba
{
	class Student
	{
		int studentId;
		string index;
		string ime;
		string prezime;
		string grad;

		public int StudentId { get => studentId; set => studentId = value; }
		public string Index { get => index; set => index = value; }
		public string Ime { get => ime; set => ime = value; }
		public string Prezime { get => prezime; set => prezime = value; }
		public string Grad { get => grad; set => grad = value; }

		public static void UcitajPodatke(string studenti)
		{

			Console.WriteLine("Da li zelite Listu svih studenata ili odredjenog studenta?");
			Console.WriteLine("Za odredjenog pritisnite 'd', za sve studente 'n'");
			char odredjeniStudent = Console.ReadKey().KeyChar;

			string[] sviStudenti = studenti.Split('\n');

			IspisiPodatke(sviStudenti, odredjeniStudent);
		}

		public static void IspisiPodatke(string[] sviStudenti, char odredjeniStudent)
		{
			Student student = new Student();
			if (odredjeniStudent == 'n')
			{
				for (int i = 0; i < sviStudenti.Length; i++)
				{
					Console.WriteLine("\n************************************");

					String[] svi = sviStudenti[i].Split(',');
					student.StudentId = Convert.ToInt32(svi[0]);
					student.Index = svi[1];
					student.Ime = svi[2];
					student.Prezime = svi[3];
					student.Grad = svi[4];

					Console.WriteLine("Student Indext:" + student.index + " Student Ime: " + student.Ime + " ,Student Prezime: " + student.Prezime + " ,Student Grad: " + student.Grad);
				}
			}
			if (odredjeniStudent == 'd')
			{
				Console.WriteLine('\n');

				Console.WriteLine("Unesite sifru studenta: ");
				string sifraStudenta = Console.ReadLine();
				int sifraS = Convert.ToInt32(sifraStudenta);

				for (int i = 0; i < sviStudenti.Length; i++)
				{
					Console.WriteLine("\n************************************");

					String[] svi = sviStudenti[i].Split(',');
					student.StudentId = Convert.ToInt32(svi[0]);
					if (student.StudentId.Equals(sifraS))
					{
						student.Index = svi[1];
						student.Ime = svi[2];
						student.Prezime = svi[3];
						student.Grad = svi[4];

						Console.WriteLine("Student Indext:" + student.Index + " Student Ime: " + student.Ime + " ,Student Prezime: " + student.Prezime + " ,Student Grad: " + student.Grad);
						break;
					}
				}
			}
		}
		public static void OdredjeniStudentiPoSmeru(char daliZelite, string studenti)
		{
			string[] sviStudenti = studenti.Split('\n');

			if (daliZelite == 'd')
			{
				Console.WriteLine('\n');
				Console.WriteLine("Unesite Smer:");
				string smer = Console.ReadLine();

				Student student = new Student();

				for (int i = 0; i < sviStudenti.Length; i++)
				{

					String[] svi = sviStudenti[i].Split(',');
					student.Index = svi[1];
					if (student.Index.Contains(smer.ToUpper()))
					{
						student.StudentId = Convert.ToInt32(svi[0]);
						student.Index = svi[1];
						student.Ime = svi[2];
						student.Prezime = svi[3];
						student.Grad = svi[4];

						Console.WriteLine("Student Indext:" + student.index + " Student Ime: " + student.Ime + " ,Student Prezime: " + student.Prezime + " ,Student Grad: " + student.Grad);
					}
				}

			}
			if (daliZelite == 'n')
			{
				return;
			}
		}

		public static void ispisUpisanihStudenataPoGodini(char daliZelite, string studenti)
		{
			List<string> list = new List<string>();
	
			string[] sviStudenti = studenti.Split('\n');

			if (daliZelite == 'd')
			{
				Student student = new Student();
			
				int count = 0;
				string godina = null;
				for (int i = 0; i < sviStudenti.Length; i++)
				{

					String[] svi = sviStudenti[i].Split(',');
					student.Index = svi[1];

					student.StudentId = Convert.ToInt32(svi[0]);
					student.Index = svi[1];
					student.Ime = svi[2];
					student.Prezime = svi[3];
					student.Grad = svi[4];

					String[] sviUpisaniPoGodini = student.Index.Split('/');
			
					for (int j = 0; j < sviUpisaniPoGodini.Length; j++)
					{
						godina = sviUpisaniPoGodini[1];
						
					}
					list.Add(godina);
				}

				// query koliko je ukupno studenata upisalo
				var query = list.GroupBy(x => x)
				  .Where(g => g.Count() > 1)
				  .Select(y => new { Studenata = y.Key, Ukupno = y.Count() })
				  .ToList();
			
				foreach (var item in query)
				{
					Console.WriteLine("Upisani studenti po godini: " + item.Studenata + " Upisalo studenata: " + item.Ukupno);

				}

			}
			if (daliZelite == 'n')
			{
				return;
			}
		}
	}
}
