﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.StudentskaSluzba
{
	class IspitniRokovi
	{
		int rokId;
		string naziv;
		DateTime pocetak;
		DateTime kraj;

		public int RokId { get => rokId; set => rokId = value; }
		public string Naziv { get => naziv; set => naziv = value; }
		public DateTime Pocetak { get => pocetak; set => pocetak = value; }
		public DateTime Kraj { get => kraj; set => kraj = value; }
	}
}
