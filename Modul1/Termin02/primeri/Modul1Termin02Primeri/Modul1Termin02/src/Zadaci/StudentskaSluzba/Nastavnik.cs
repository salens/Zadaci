﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.StudentskaSluzba
{
	class Nastavnik
	{
		int nastavnikId;
		string ime;
		string prezime;
		string zvanje;


		public int NastavnikId { get => nastavnikId; set => nastavnikId = value; }
		public string Ime { get => ime; set => ime = value; }
		public string Prezime { get => prezime; set => prezime = value; }
		public string Zvanje { get => zvanje; set => zvanje = value; }


		public static void UcitajPodatke(string nastavnici)
		{

			Console.WriteLine("Svi nastavnici:");

			string[] sviNastavnici = nastavnici.Split('\n');

			IspisiPodatke(sviNastavnici);
		}

		public static void IspisiPodatke(string[] sviNastavnici)
		{
			Nastavnik nastavnik = new Nastavnik();

			for (int i = 0; i < sviNastavnici.Length; i++)
			{
				Console.WriteLine("\n************************************");

				String[] svi = sviNastavnici[i].Split(',');

				nastavnik.NastavnikId = Convert.ToInt32(svi[0]);
				nastavnik.Ime = svi[1];
				nastavnik.Prezime = svi[2];
				nastavnik.Zvanje = svi[3];

				Console.WriteLine(" Nastavnik Ime: " + nastavnik.Ime + " ,Nastavnik Prezime: " + nastavnik.Prezime + " ,Nastavnik Zvanje: " + nastavnik.Zvanje);
			}
		}
	}
}
