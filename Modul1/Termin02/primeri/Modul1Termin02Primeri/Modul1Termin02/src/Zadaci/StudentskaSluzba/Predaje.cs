﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.src.Zadaci.StudentskaSluzba
{
	class Predaje
	{
		int nastavnikId;
		Nastavnik nastavnik;
		int predmetId;
		Predmet predmet;

		public int NastavnikId { get => nastavnikId; set => nastavnikId = value; }
		public int PredmetId { get => predmetId; set => predmetId = value; }
		internal Nastavnik Nastavnik { get => nastavnik; set => nastavnik = value; }
		internal Predmet Predmet { get => predmet; set => predmet = value; }

		public static void IspisiPodatkeUOdnosuNaIndetifikator(string sifraNastavnika, string sifraPredmeta, string nastavnici, string predmeti)
		{
			int sifraN = Convert.ToInt32(sifraNastavnika);
			Nastavnik nastavnik = new Nastavnik();

			Console.WriteLine("Svi nastavnici:");

			string[] sviNastavnici = nastavnici.Split('\n');

			for (int i = 0; i < sviNastavnici.Length; i++)
			{
				Console.WriteLine("\n************************************");

				String[] svi = sviNastavnici[i].Split(',');

				nastavnik.NastavnikId = Convert.ToInt32(svi[0]);
				if (nastavnik.NastavnikId.Equals(sifraN))
				{
					nastavnik.Ime = svi[1];
					nastavnik.Prezime = svi[2];
					nastavnik.Zvanje = svi[3];
					Console.WriteLine(" Nastavnik Ime: " + nastavnik.Ime + " ,Nastavnik Prezime: " + nastavnik.Prezime + " ,Nastavnik Zvanje: " + nastavnik.Zvanje);
					break;
				}

			}

			Predmet predmet = new Predmet();
			int sifraP = Convert.ToInt32(sifraPredmeta);
			Console.WriteLine("Svi Predmeti:");

			string[] sviPredmeti = predmeti.Split('\n');

			for (int i = 0; i < sviPredmeti.Length; i++)
			{
				Console.WriteLine("\n************************************");

				String[] svi = sviPredmeti[i].Split(',');

				predmet.PredmetId = Convert.ToInt32(svi[0]);
				if (predmet.PredmetId.Equals(sifraP))
				{
					predmet.PredmetId = Convert.ToInt32(svi[0]);
					predmet.Naziv = svi[1];
					Console.WriteLine("Predmet Naziv: " + predmet.Naziv);
					break;
				}
			}



		}

	}
}
