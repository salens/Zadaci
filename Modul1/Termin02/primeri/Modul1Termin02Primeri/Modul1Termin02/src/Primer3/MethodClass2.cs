﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer3
{
    class MethodClass2
    {
        //izracunavanje kvadrata hipotrenuze pravouglog trougla
        //ulazni parametri su duzine kateta a i b
        static double VrednostHipotenuzePravouglogTrougla(double a, double b)
        {
            double c = 0;
            c = Math.Sqrt(a * a + b * b);
            return c;
            Int32 i = new Int32();
        }

        //izracunavanje kvadrata hipotrenuze pravouglog trougla, koji ima katete istih velicina
        //ulazni parametri je duzina katete a
        static double VrednostHipotenuzePravouglogTrougla(double a)
        {
            double c = 0;
            c = Math.Sqrt(a * a + a * a);

            return c;
            //		a = 5;
        }

        //izmena vrednosti ulaznih parametara
        static void IzmenaVrednostiUlaznihParametara(int a, int[] A)
        {

            a = -100;
            Console.WriteLine("\t\t izmenjena vrednost promenljive a u funkciji je " + a);
            if (A != null)
            {
                A[0] = -200;
                Console.WriteLine("\t\t izmenjena vrednost promenljive A[0] u funkciji je " + A[0]);
            }
        }

        public static void Main(String[] args)
        {

            double vrednost = 0;
            vrednost = VrednostHipotenuzePravouglogTrougla(3, 4);
            Console.WriteLine("Vrednost hipotenuze je:" + vrednost);

            vrednost = VrednostHipotenuzePravouglogTrougla(3);
            Console.WriteLine("Vrednost hipotenuze je:" + vrednost + "\n\n");

            int a = 3;
            int[] A = new int[2];
            A[0] = 10;
            Console.WriteLine("vrednost promenljive a je " + a);
            Console.WriteLine("vrednost promenljive A[0] je " + A[0]);
            //promenljiva vrednosti primenljivih unutar fukcije 
            //ukoliko su promenljive primitivni tipovi ili reference na objekte
            IzmenaVrednostiUlaznihParametara(a, A);
            Console.WriteLine("vrednost promenljive a je " + a);
            Console.WriteLine("vrednost promenljive A[0] je " + A[0]);

            Console.ReadKey();

        }
    }
}
