﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer3
{
    class MethodClass4
    {
        static void Main(string[] args)
        {
            Method(3,3);

            Method(3,3.14f);

            Method(3, Int64.MinValue);

            //greska - dvosmislenost izmedju definicija
            //Method(int c, int a = 3, double f = 4.5f)
            //i
            //Method(int c, long a = 3, float f = 4.5f)
            //double vrednost moze biti i 3.14 i 3.14f
            //Method(3, 3, 3.14f);
            Console.ReadKey();
        }

        public static void Method(int a)
        {
            Console.WriteLine("Method(int a)");
        }

        public static void Method(int c, int a = 3, double f = 4.5f)
        {
            Console.WriteLine("Method(int c, int a = 3, double f = 4.5f)");
        }

        public static void Method(int c, long a = 3, float f = 4.5f)
        {
            Console.WriteLine("Method(int c, long a = 3, float f = 4.5f)");
        }

        public static void Method(int c, float f = 4.5f, long a = 3)
        {
            Console.WriteLine("Method(int c, float f = 4.5f, long a = 3)");
        }
    }
}
