﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin02.Primer3
{
    class MethodClass3
    {
        //prosledjivanje parametra po referenci, ne po vrednosti
        static void MethodRef(ref int i)
        {
            //izmena vrednosti promenljive rezultuje 
            //izmenu prosledjene promeljive prilikom poziva metode
            i = i + 44;
        }

        //prosledjivanje parametra po referenci, ne po vrednosti
        static void MethodOut(out int i)
        {
            //izmena vrednosti promenljive rezultuje 
            //izmenu prosledjene promeljive prilikom poziva metode
            //razlika u odnosu na ključnu reč ref je u tome
            //što ključna reč out zahteva da se promenljiva i
            //inicijalizuje u ovoj metodi
            i = 44;
        }


        public static void MethodParamsIntArray(params int[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                Console.Write(args[i] + " ");
            }
            Console.WriteLine();
        }

        public static void MethodParamsObjectArray(params object[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                Console.Write(args[i] + " ");
            }
            Console.WriteLine();
        }

        static void Main()
        {
            //parametri se prosleđuju kao niz vrednosti određenog tipa 
            //međusobno razdvojenih zarezom
            MethodParamsIntArray(1, 2, 3, 4);
            MethodParamsObjectArray(1, 'a', "test");

            //metoda ne mora imati ni jedan parametar
            MethodParamsObjectArray();

            //parametri se mogu proslediti kao zasebno definisan niz
            int[] myIntArray = { 5, 6, 7, 8, 9 };
            MethodParamsIntArray(myIntArray);

            object[] myObjArray = { 2, 'b', "test", "again" };
            MethodParamsObjectArray(myObjArray);

            //greska - niz objekata se ne može konvertovati u int niz
            //MethodParamsIntArray(myObjArray);

            //int niz postaje prvi element niza objekata 
            MethodParamsObjectArray(myIntArray);

            int p;
            MethodOut(out p);
            Console.WriteLine("Vrednost promenljive p: "+p); // vrednost je 44

            MethodRef(ref p);
            Console.WriteLine("Vrednost promenljive p: " + p); // vrednost je 88

            Console.ReadKey();
        }
    }
}
