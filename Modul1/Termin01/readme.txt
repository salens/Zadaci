﻿FTN Informatika
Kurs iz .NET Web programiranja
=================================

1. Opis vežbe
-------------------
- Upoznavanje sa osnovnim konceptima .NET platforme, Visual Studio okruženjem i konceptima osnova proceduralnog programiranja


2. Sadržaj vežbe
-------------------
- Upoznavanje sa osnovnim konceptima .NET platforme
- Upoznavanje sa Visual Studio alatom 
- Kreiranje i pokretanje prvog projekta 
- Proceduralno programiranje osnove (usning, namespace, definicija klase, main metoda) 
- Programski tipovi u C#
- Osnove proceduralnog programiranja u C#
- - Namespace, class, main()
- - Ispis na ekran
- - Tipovi u .NET platformi
- - Operatori, konverzije, komentari, programski blokovi
- - Kontrola toka programa i ciklusi


3. Literatura
-------------------
materijali/Modul1Termin01.pdf
materijali/Preporuka pravilnog pisanja programskog koda.pdf
materijali/msdn/*.pdf

4. Primeri
-------------------
primeri\Modul1Termin01Primeri


5. Zadatak na času
-------------------
zadaci\Modul1Termin01 - Zadaci.pdf
zadaci\Modul1Termin01 - Dodatni zadaci.pdf 

6. Domaći zadatak
-------------------
Sve što ne stignete na času.


