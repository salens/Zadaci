﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.Primer3
{
    class ControlFlowIfSwitchClass
    {
        static void Main(string[] args)
        {
            //IF naredba
            int ocena;
            int bodovi = 78;
            if (bodovi >= 95)
                ocena = 10;
            else if (bodovi >= 85)
                ocena = 9;
            else if (bodovi >= 75)
                ocena = 8;
            else if (bodovi >= 65)
                ocena = 7;
            else if (bodovi >= 55)
                ocena = 6;
            else
                ocena = 5;

            Console.WriteLine("Ocena je " + ocena);

            switch (ocena)
            {
                case 5:
                    Console.WriteLine("Odlican");
                    break;
                case 4:
                    Console.WriteLine("Vrlo dobar");
                    break;
                default:
                    Console.WriteLine("Nepostojeca");
                    break;
            }

            //SWITCH naredba
            int mesec = 3;
            //menjati vrednost meseca 1..13
            switch (mesec)
            {
                case 1:
                    Console.WriteLine("Januar");
                    break;
                case 2:
                    Console.WriteLine("Februar");
                    break;
                case 3:
                    Console.WriteLine("Mart");
                    break;
                case 4:
                    Console.WriteLine("April");
                    break;
                case 5:
                    Console.WriteLine("Maj");
                    break;
                case 6:
                    Console.WriteLine("Juni");
                    break;
                case 7:
                case 8:
                case 9:
                    Console.WriteLine("Letnji meseci");
                    break;
                case 10:
                    Console.WriteLine("Oktobar");
                    break;
                case 11:
                    Console.WriteLine("November");
                    break;
                case 12:
                    Console.WriteLine("Decembar");
                    break;
                default:
                    Console.WriteLine("Nepostojeci mesec");
                    break;
            }
            //zadatak sa for|while,if,else
            //suma parnih brojeva

            Console.ReadKey();
        }
    }
}
