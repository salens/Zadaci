﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.Primer3
{
    class ControlFlowForWhile
    {
        static void Main(string[] args)
        {
            string tekst = "string=niz karaktera";

            for (int i = 0; i < tekst.Length; ++i) 
            {
                Console.WriteLine(i +" : "+tekst[i]);
            }

            // primer upotrebe break i continue
            int brojSlovaA = 0;
            for (int i = 0; i < tekst.Length; ++i)
            {
                if (tekst[i] == ' ')
                    break;
                else if (tekst[i] != 'a')
                    continue;
                brojSlovaA++;
            }
               
            // upotreba foreach petlje
            //foreach (char c in tekst)
            //{
            //    Console.WriteLine(c);
            //}

            int broj = 101;
            while (broj > 20)
            {
                Console.WriteLine(broj);
                broj /= 2;
            }

            Console.ReadKey();
        }
    }
}
