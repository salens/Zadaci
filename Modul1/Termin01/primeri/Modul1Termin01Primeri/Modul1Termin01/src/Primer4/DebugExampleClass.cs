﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.Primer4
{
    class DebugExampleClass
    {
        static void Main(string[] args)
        {
            int a = 8;
            int b = ++a;

            double c = b / 2;
            double rez = -1;
            if (c > 4)
            {
                rez = 10;
            }
            else
            {
                rez = 20;
            }
            int suma = 0;
            for (int i = 0; i < 2000; i++)
            {
                suma += i;
            }
            Console.WriteLine("Suma je: "+suma);

            Console.WriteLine(rez);
            Console.ReadKey();
        }
    }
}
