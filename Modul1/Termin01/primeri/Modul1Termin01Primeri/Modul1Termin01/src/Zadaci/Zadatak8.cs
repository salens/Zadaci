﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.src.Zadaci
{
	class Zadatak8
	{
		static void Main(string[] args)
		{
			int x, y;

			x = 5;
			y = 6;

			Swap(x, y);

		}

		public static void Min(int x, int y)
		{
			if (x > y)
			{
				Console.WriteLine("Y je manje" + y);
				Console.ReadKey();
			}
			if (x < y)
			{
				Console.WriteLine("X je manje" + x);
				Console.ReadKey();
			}
		}
		public static void Max(int x, int y)
		{
			if (x > y)
			{
				Console.WriteLine("X je vece" + x);
				Console.ReadKey();
			}
			if (x < y)
			{
				Console.WriteLine("Y je vece" + y);
				Console.ReadKey();
			}
		}
		public static void Swap(int x, int y)
		{
			x ^= y;
			y ^= x;
			x ^= y;

			Console.WriteLine("Y = 6 i X = 5 , a sada su zamenjene vrednosti: " + "Y je: " + y + " , " + "X je: " + x);
			Console.ReadKey();
		}
		public static void Equals(int x, int y)
		{
			if (x == y)
			{
				Console.WriteLine("Y i X su jednaki" + "Y je: " + y + " , " + "X je: " + x);
				Console.ReadKey();
			}

		}
	}
}
