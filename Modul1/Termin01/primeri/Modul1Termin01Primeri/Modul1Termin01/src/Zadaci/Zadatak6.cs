﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.src.Zadaci
{
	class Zadatak6
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Unesite godinu: ");
			int godina = 0;
			godina = Convert.ToInt32(Console.ReadLine());

			if (godina / 400 % 2 == 1)
			{
				Console.WriteLine("Godina: " + godina + " prestupna je");
			}
			if (godina / 400 % 2 == 0)
			{
			
				if ((godina / 100) % 2 == 1)
				{
					Console.WriteLine("Godina: " + godina + " nije prestupna");
				}

			}
			Console.ReadKey();
		}
	}
}
