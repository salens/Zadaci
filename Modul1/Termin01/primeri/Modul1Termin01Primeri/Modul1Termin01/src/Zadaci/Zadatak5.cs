﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.src.Zadaci
{
	class Zadatak5
	{
		static void Main(string[] args)
		{
			double UkupnaCena = 0.0;
			double JedinicnaCenaProizvoda;
			int KolicinaProizvoda;
			string akcija = null;
			bool DaliJeProizvodNaAkciji = false;


			Console.WriteLine("Unesite cenu proizvoda:");
			JedinicnaCenaProizvoda = Convert.ToDouble(Console.ReadLine());

			Console.WriteLine("Unesite Kolicinu Proizvoda: ");
			KolicinaProizvoda = Convert.ToInt32(Console.ReadLine());

			Console.WriteLine("Da li je proizvod na akciji:");
			akcija = Console.ReadLine();

			if (akcija.ToLower().Equals("da"))
			{
				DaliJeProizvodNaAkciji = true;
			}
			
			if(DaliJeProizvodNaAkciji == true)
			{
				UkupnaCena = (JedinicnaCenaProizvoda * KolicinaProizvoda);
				UkupnaCena = UkupnaCena - (UkupnaCena * 0.10);
			
				Console.WriteLine("Sa popustom od 10 %: " + UkupnaCena);
			}
			else
			{
				UkupnaCena = (JedinicnaCenaProizvoda * KolicinaProizvoda);

				Console.WriteLine("Bez popusta: " + UkupnaCena);
			}

			

			Console.ReadKey();

		}
	}
}
