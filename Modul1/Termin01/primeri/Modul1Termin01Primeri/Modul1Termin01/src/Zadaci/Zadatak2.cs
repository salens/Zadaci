﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.src.Zadaci
{
	class Zadatak2
	{
		static void Main(string[] args)
		{
			double Celzijus, Farenhajt;
			double mnozioc = 1.8;
			int vrednost = 32;

			Console.WriteLine("Unesite Celzijuse");
			Celzijus = Convert.ToInt32(Console.ReadLine());
			Farenhajt = Celzijus * mnozioc + vrednost;

			Console.WriteLine("Nakon konvertovanja celzijusa u farenhajt, faranhajt iznosi: " + Farenhajt);

			Console.ReadKey();

		}
	}
}
