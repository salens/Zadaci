﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1Termin01.src.Zadaci.ProdavnicaPolovnihVozila.Model;
namespace Modul1Termin01.src.Zadaci.ProdavnicaPolovnihVozila
{
	class ProdavnicaPolovnihVozila
	{
		static void Main(string[] args)
		{
			//ispisa svih oglasa u formi: redni broj, šifra, naslov oglasa i cena

			Oglas oglas1 = new Oglas(1, "Polovni Stojadin", 1500, 1991);
			Oglas oglas2 = new Oglas(2, "Polovni 192", 1500, 1990);
			Oglas oglas3 = new Oglas(3, "Polovni Yugo", 2500, 1990);
			Oglas oglas4 = new Oglas(4, "Polovni Lada", 1500, 1992);
			Oglas oglas5 = new Oglas(5, "Polovni Dzpi pajero", 1500, 1998);
			List<Oglas> listaOglasa = new List<Oglas>();
			listaOglasa.Add(oglas1);
			listaOglasa.Add(oglas2);
			listaOglasa.Add(oglas3);
			listaOglasa.Add(oglas4);
			listaOglasa.Add(oglas5);

			//Auti sa opremom:

			string[] oprema6 = { "ABS", "ESP", "alarm", "airbag", "tempomat" };
			string[] oprema7 = { "klima", "servo", "vola", "putni računar", "tempomat" };
			string[] oprema8 = { "zatamljena stakla", "maglenke", "tempomat", "električna stakla", "ESP" };
			string[] oprema9 = { "CD", "DVD", "parking sentori", "električna stakla", "ESP" };
			string[] oprema10 = { "CD", "alarm", "parking sentori", "električna stakla", "ESP" };

			Oglas oglas6 = new Oglas(6, " Stojadin", 1500, 1995, oprema6);
			Oglas oglas7 = new Oglas(7, " 192", 2800, 1990, oprema7);
			Oglas oglas8 = new Oglas(8, " Yugo", 2500, 1990, oprema8);
			Oglas oglas9 = new Oglas(9, " Lada", 1500, 1992, oprema9);
			Oglas oglas10 = new Oglas(10, " Dzpi pajero", 1500, 1998, oprema10);

			List<Oglas> listaOglasaSaOpremom = new List<Oglas>();
			listaOglasaSaOpremom.Add(oglas6);
			listaOglasaSaOpremom.Add(oglas7);
			listaOglasaSaOpremom.Add(oglas8);
			listaOglasaSaOpremom.Add(oglas9);
			listaOglasaSaOpremom.Add(oglas10);

			//1. ispisa svih oglasa u formi: redni broj, šifra, naslov oglasa i cena.
			foreach (Oglas oglas in listaOglasa)
			{
				Console.WriteLine(oglas.ToString());
			}

			//2. ispis svih oglasa čija su vozila proizvedena u određenoj godini u formi: redni broj, šifra, naslov oglasa i cena.

			Console.WriteLine("Koju godinu zelite: ");
			int sviOglasiPoGodini = Convert.ToInt32(Console.ReadLine());

			foreach (Oglas oglas in listaOglasa)
			{
				if (sviOglasiPoGodini == oglas.GodinaProizvodnje)
				{
					Console.WriteLine(oglas.ToString());

				}

			}


			//3. ispisa svih detalja o odeređenom oglasu u formi šifra, naslov oglasa, cena i oprema.
			Console.WriteLine("ispisa svih detalja o odeređenom oglasu u formi šifra, naslov oglasa, cena i oprema: ");

			foreach (Oglas oglas in listaOglasaSaOpremom)
			{
				{
					Console.WriteLine(oglas.ToStringSaOpremom());
					Console.WriteLine("Oprema Vozila: ");
					for (int i = 0; i < listaOglasaSaOpremom.Count; i++)
					{
						Console.WriteLine(oglas.OpremaVozila[i]);
					}

				}

			}

			//4. izmena oglasa

			//4.1. osnovnih detalja: naslov, cena, godina proizvodnje.

			Console.WriteLine("Koje vozilo iz liste zelite da izmenite? ");

			int oglasZaIzmenu = Convert.ToInt32(Console.ReadLine());

			foreach (Oglas oglas in listaOglasa)
			{
				if (oglasZaIzmenu == oglas.SifraOglasa)
				{
					Console.WriteLine("Unesite Naslov oglasa: ");
					oglas.NaslovOglasa = Console.ReadLine();
					Console.WriteLine("Unesite Cenu Vozila: ");
					oglas.CenaVozila = Convert.ToInt32(Console.ReadLine());
					Console.WriteLine("Unesite Godinu proizvodnje vozila: ");
					oglas.GodinaProizvodnje = Convert.ToInt32(Console.ReadLine());
					Console.WriteLine(oglas.ToString());

				}

			}

			Console.WriteLine("Izmenili ste: ");

			foreach (Oglas oglas in listaOglasa)
			{
				Console.WriteLine(oglas.ToString());

			}

			//4.2. izmena opreme vozila.
			Console.WriteLine("Unesite sifru oglasa za izmenu opreme: ");
			int oglasZaIzmenu2 = Convert.ToInt32(Console.ReadLine());

			string[] izmenaOpreme = new string[5];
			foreach (Oglas oglas in listaOglasaSaOpremom)
			{
				if (oglasZaIzmenu2 == oglas.SifraOglasa)
				{
					Console.WriteLine("Unesite novu opremu oglasa: ");

					for (int i = 0; i < 5; i++)
					{
						Console.WriteLine("Unesite Opremu: " + i);
						string g = Console.ReadLine();
						izmenaOpreme[i] = g;
					}
					oglas.OpremaVozila = izmenaOpreme;
				}

			}

			Console.WriteLine("ispisa svih detalja o odeređenom oglasu izmenjena oprema: ");
			int oglasZaIzmenu3 = Convert.ToInt32(Console.ReadLine());
			foreach (Oglas oglas in listaOglasaSaOpremom)
			{
				if (oglasZaIzmenu3 == oglas.SifraOglasa)
				{
					{
						Console.WriteLine(oglas.ToStringSaOpremom());
						Console.WriteLine("Oprema Vozila: ");
						for (int i = 0; i < listaOglasaSaOpremom.Count; i++)
						{
							Console.WriteLine(oglas.OpremaVozila[i]);
						}

					}
				}

			}

			//5. ispis svih oglasa u opsegu cene u formi: redni broj, šifra, naslov oglasa i cena.

			//5.1. od-do (npr. 2000-3000€)
			Console.WriteLine("ispisa svih vozila od-do (npr. 2000-3000€)");

			Console.WriteLine("Unesite od:");
			int oglasZaIzmenu4 = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Unesite do:");
			int oglasZaIzmenu5 = Convert.ToInt32(Console.ReadLine());
			foreach (Oglas oglas in listaOglasaSaOpremom)
			{
				if (oglas.CenaVozila > oglasZaIzmenu4 && oglas.CenaVozila < oglasZaIzmenu5)
				{
					{
						Console.WriteLine(oglas.ToStringSaOpremom());
						Console.WriteLine("Oprema Vozila: ");
						for (int i = 0; i < listaOglasaSaOpremom.Count; i++)
						{
							Console.WriteLine(oglas.OpremaVozila[i]);
						}

					}
				}

			}

			//5.2. od (skuplji od 5000€)

			Console.WriteLine("ispisa svih vozila skupljih od (npr.3000€)");

			Console.WriteLine("Unesite od:");
			int oglasZaIzmenu6 = Convert.ToInt32(Console.ReadLine());

			foreach (Oglas oglas in listaOglasaSaOpremom)
			{
				if (oglas.CenaVozila > oglasZaIzmenu6)
				{
					{
						Console.WriteLine(oglas.ToStringSaOpremom());
						Console.WriteLine("Oprema Vozila: ");
						for (int i = 0; i < listaOglasaSaOpremom.Count; i++)
						{
							Console.WriteLine(oglas.OpremaVozila[i]);
						}

					}
				}

			}

			//5.3. do (jeftiniji od 5000€)

			Console.WriteLine("ispisa svih vozila jeftinijih do (npr.5000€)");

			Console.WriteLine("Unesite od:");
			int oglasZaIzmenu7 = Convert.ToInt32(Console.ReadLine());

			foreach (Oglas oglas in listaOglasaSaOpremom)
			{
				if (oglas.CenaVozila < oglasZaIzmenu7)
				{
					{
						Console.WriteLine(oglas.ToStringSaOpremom());
						Console.WriteLine("Oprema Vozila: ");
						for (int i = 0; i < listaOglasaSaOpremom.Count; i++)
						{
							Console.WriteLine(oglas.OpremaVozila[i]);
						}

					}
				}

			}

			//6 ispisa svih oglasa čija su vozila imaju određenu opremu u formi: redni broj, šifra, naslov oglasa i cena.

			Console.WriteLine("Pretraga po opremi ");


			string[] izmenaOpreme1 = new string[5];
			foreach (Oglas oglas in listaOglasaSaOpremom)
			{

				for (int i = 0; i < 5; i++)
				{
					Console.WriteLine("Unesite Opremu za pretragu: " + i);
					string g = Console.ReadLine();
					izmenaOpreme1[i] = g;
				}

				for (int i = 0; i < listaOglasaSaOpremom.Count; i++)
				{
					if (oglas.OpremaVozila[i] == izmenaOpreme1[i])
					{
						Console.WriteLine("Oglas sa istom opremom:" + oglas.ToStringSaOpremom() + " Oprema: " + oglas.OpremaVozila[i]);
					}
				}



			}

			Console.ReadKey();
		}
	}
}
