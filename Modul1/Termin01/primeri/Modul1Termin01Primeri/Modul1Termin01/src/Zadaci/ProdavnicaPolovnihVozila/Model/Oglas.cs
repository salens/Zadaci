﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.src.Zadaci.ProdavnicaPolovnihVozila.Model
{
	class Oglas
	{
		int sifraOglasa;
		string naslovOglasa;
		int cenaVozila;
		int godinaProizvodnje;
		string[] opremaVozila = new string[5];
		public Oglas(int sifraOglasa, string naslovOglasa, int cenaVozila, int godinaProizvodnje)
		{
			SifraOglasa = sifraOglasa;
			NaslovOglasa = naslovOglasa;
			CenaVozila = cenaVozila;
			GodinaProizvodnje = godinaProizvodnje;
		}

		public Oglas(int sifraOglasa, string naslovOglasa, int cenaVozila, int godinaProizvodnje, string[] opremaVozila)
		{
			this.SifraOglasa = sifraOglasa;
			this.NaslovOglasa = naslovOglasa;
			this.CenaVozila = cenaVozila;
			this.GodinaProizvodnje = godinaProizvodnje;
			this.OpremaVozila = opremaVozila;
		}

		public int SifraOglasa { get => sifraOglasa; set => sifraOglasa = value; }
		public string NaslovOglasa { get => naslovOglasa; set => naslovOglasa = value; }
		public int CenaVozila { get => cenaVozila; set => cenaVozila = value; }
		public int GodinaProizvodnje { get => godinaProizvodnje; set => godinaProizvodnje = value; }
		public string[] OpremaVozila { get => opremaVozila; set => opremaVozila = value; }

		public override string ToString()
		{
			return "Oglas sa sifrom: " + SifraOglasa + " Naslov Oglasa: " + NaslovOglasa + " Cena Vozila: " + CenaVozila + " Godina Proizvodnje Vozila" + GodinaProizvodnje;
		}

		public string ToStringSaOpremom()
		{
			return "Oglas sa sifrom: " + SifraOglasa + " Naslov Oglasa: " + NaslovOglasa + " Cena Vozila: " + CenaVozila + " Godina Proizvodnje Vozila" + GodinaProizvodnje;

		}
	}
}
