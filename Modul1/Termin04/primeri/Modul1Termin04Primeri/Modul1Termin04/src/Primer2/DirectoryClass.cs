﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer2
{
    class DirectoryClass
    {
        public static void Main(string[] args)
        {

            // separator putanje direktorijuma
            char sep = Path.DirectorySeparatorChar;
            Console.WriteLine("\nSeparator je: " + sep);

            //string putanja = ".";
            //string putanja = "."+sep+".."+sep+"..";
            string putanja = @".\..\..";
            IzlistajSadrzajFolera(putanja);

            Console.WriteLine("Ostale korisne stvari: ");
            Console.WriteLine("Path.AltDirectorySeparatorChar={0}", Path.AltDirectorySeparatorChar);
            Console.WriteLine("Path.PathSeparator={0}", Path.PathSeparator);
            Console.WriteLine("Path.VolumeSeparatorChar={0}", Path.VolumeSeparatorChar);

            Console.WriteLine();

            IzlistajSadrzajFolera();

            //kopiraj direktorijum data pre menjanja istog u narednim primerima
            KopirajDatoteke();

            // TODO prikazati sadrzaj direktorijuma data            

            Console.ReadKey();
        }

        static void IzlistajSadrzajFolera(string putanja)
        {
            // kreiranje DirectoryInfo objekta koji reprezentuje trenutni direktorijum 
            DirectoryInfo di = new DirectoryInfo(putanja);
            // Prikaz absolutne putanje ovog foldera
            Console.WriteLine(di.FullName);

            //provera da li direktorijum postoji
            if (di.Exists)
            {
                Console.WriteLine("\nIzlistavanje direktorijuma\n");
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    Console.WriteLine(dir.Name);

                }
                Console.WriteLine("\nIzlistavanje datoteka\n");
                foreach (FileInfo file in di.GetFiles())
                {
                    Console.WriteLine(file.Name + "[" + file.Length + "b]");

                }
            }


            //korišćenje Directory klase
            //if (Directory.Exists(putanja))
            //{
            //    Console.WriteLine("\nIzlistavanje direktorijuma\n");
            //    foreach (string dir in Directory.GetDirectories(putanja))
            //    {
            //        Console.WriteLine(dir);
            //        //Console.WriteLine(Path.GetFileName(dir));
            //    }
            //    Console.WriteLine("\nIzlistavanje datoteka\n");
            //    foreach (string file in Directory.GetFiles(putanja))
            //    {
            //        Console.WriteLine(file);
            //        //Console.WriteLine(Path.GetFileName(file));
            //    }
            //    Console.WriteLine();
            //}
            Console.WriteLine();
        }

        static void IzlistajSadrzajFolera()
        {
            string trenutnaPutanja = Directory.GetCurrentDirectory();
            Console.WriteLine("\tTrenutna putanja: \n" + trenutnaPutanja);

            //1. način
            DirectoryInfo di = new DirectoryInfo(trenutnaPutanja);
            //pozicionirati se u korenski direktorijum projekta
            di = di.Parent.Parent;
            Console.WriteLine("\tNova putanja: \n" + di.FullName);

            //2. način
            //string projectDir = trenutnaPutanja.Split(new string[] { "bin" }, StringSplitOptions.None)[0];
            //DirectoryInfo di = new DirectoryInfo(projectDir);

            Console.WriteLine("\n\tSadržaj foldera:\t[broj poddirektorijuma, broj poddatoteka]\n\n");
            foreach (var item in di.GetDirectories())
            {
                int subDirs = Directory.GetDirectories(item.FullName).Count();
                int subFiles = Directory.GetFiles(item.FullName).Count();
                Console.WriteLine(item.Name + "[" + subDirs + "d," + subFiles + "f]");
            }
            foreach (var item in di.GetFiles())
            {
                Console.WriteLine(item.Name);
            }

        }

        private static void KopirajDatoteke()
        {
            string dataDir = @".\..\..\data";
            string backupDir = @".\..\..\backup";

            if (!Directory.Exists(dataDir))
            {
                Console.WriteLine("Nedostaje data direktorijum u projektu!");
                return;
            }
            if (!Directory.Exists(backupDir))
            {
                DirectoryInfo di = Directory.CreateDirectory(backupDir);
            }

            //više o try/catch bloku će biti u narednom terminu
            try
            {
                foreach (string currentFile in Directory.EnumerateFiles(dataDir, "*.*"))
                {
                    string nazivDat = Path.GetFileName(currentFile);

                    //premeštanje datoteka u drugi direktorijum            
                    //Directory.Move(currentFile, Path.Combine(backupDir, fileName));

                    //Directory nema Copy metodu pa se koristi specijalizovana klasa File
                    nazivDat = Path.Combine(backupDir, nazivDat);
                    File.Copy(currentFile, nazivDat, true); //true - prepiši postojeće datoteke
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Greska: "+e.Message);
            }
        }
    }
}
