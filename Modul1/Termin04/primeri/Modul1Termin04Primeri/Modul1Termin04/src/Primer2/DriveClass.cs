﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer2
{
    class DriveClass
    {
        static void Main(string[] args)
        {
            foreach (DriveInfo di in DriveInfo.GetDrives())
            {
                if (di.IsReady == true)
                {
                    Console.WriteLine("Disk: {0}", di.Name);
                    Console.WriteLine("Tip: {0}", di.DriveType);
                    Console.WriteLine("Labela: {0}", di.VolumeLabel);
                    Console.WriteLine("Format: {0}", di.DriveFormat);
                    Console.WriteLine("Free space: {0:f3}GB", di.TotalFreeSpace/(1024 * 1024 * 1024.0));
                    Console.WriteLine("Total space: {0:f3}GB", di.TotalSize / (1024 * 1024 * 1024.0));
                    Console.WriteLine(new string('_', 50));
                }
                
            }
            
            Console.ReadKey();
        }
    }
}
