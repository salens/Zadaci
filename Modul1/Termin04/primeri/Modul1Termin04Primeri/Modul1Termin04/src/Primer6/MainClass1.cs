﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer6
{
    class MainClass1
    {
        private static readonly string DataDir = "data";
        private static readonly string StudDat = "studenti.csv";
        private static List<Student> ListaStudenata = new List<Student>();

        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            char sep = Path.DirectorySeparatorChar;
            //string nazivDatoteke = @".\..\..\"+DataDir+sep+StudDat;

            //kada nam je poznata struktura datoteka i direktorijuma 
            //može se koristiti ovakav način pozicioniranja
            string trenutnaPutanja = Directory.GetCurrentDirectory();
            string putanjaProjekta = new DirectoryInfo(trenutnaPutanja).Parent.Parent.FullName;
            string putanjaDataDir = putanjaProjekta + sep + DataDir + sep; 


            CitajIzDatoteke(putanjaDataDir + StudDat);
            
            //ispis svih studenata
            IspisiStudente();

            Student st = ListaStudenata[6];
            Console.WriteLine("Unesi novi grad za studenta " + st.Ime + " " + st.Prezime + " ");
            st.Grad = Console.ReadLine();
            Console.WriteLine(st);

            PisiUDatoteku(putanjaDataDir + StudDat);

            Console.WriteLine("Kraj programa");

            Console.ReadKey();
        }

        //ispis
        public static void IspisiStudente()
        {
            foreach (Student st in ListaStudenata)
            {
                Console.WriteLine(st);
            }
        }

        private static void CitajIzDatoteke(string nazivDatoteke)
        {
            if (File.Exists(nazivDatoteke))
            {
                using (StreamReader reader1 = File.OpenText(nazivDatoteke))
                {
                    //1.način
                    //Console.WriteLine(reader1.ReadToEnd());

                    //2.način
                    string linija = "";
                    while ((linija = reader1.ReadLine()) != null)
                    {
                        string[] tokeni = linija.Split(',');
                        int id = Int32.Parse(tokeni[0]);
                        string indeks = tokeni[1];
                        string prezime = tokeni[2];
                        string ime = tokeni[3];
                        string grad = tokeni[4];

                        Student st = new Student(id, ime, prezime, grad, indeks);
                        ListaStudenata.Add(st);
                    }
                }
            }
            else
            {
                Console.WriteLine("Datoteka ne postoji ili putanja nije ispravna.");
            }

        }

        private static void PisiUDatoteku(string nazivDatoteke)
        {
            if (File.Exists(nazivDatoteke))
            {
                using (StreamWriter writer = new StreamWriter(nazivDatoteke, false, Encoding.UTF8))
                {
                    foreach (Student s in ListaStudenata)
                    {
                        writer.WriteLine(s.PreuzmiTekstualnuReprezentacijuKlaseZaDatoteku());
                    }
                }
            }
            else
            {
                Console.WriteLine("Datoteka ne postoji ili putanja nije ispravna.");
            }
        
        }

    }
}
