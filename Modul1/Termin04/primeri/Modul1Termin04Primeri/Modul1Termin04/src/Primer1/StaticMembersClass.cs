﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer1
{
    class StaticMembersClass
    {
        //staticki atribut koji se moze menjati
        public static int IndexCounter = 1;

        //staticki atribut koji se ne moze menjati nakon inicijalizacije
        //readonly ne mora nužno biti static polje
        public static readonly string ClassMark = "staticCLS";

        //konstante su već statičke tako da se ne može dodatno definisati sa static
        //obicno se konstante pisu velikim slovima
        public const int INDEX_NUMBER = 7;

        public StaticMembersClass()
        {
            Console.WriteLine("Podrazumevani konstruktor (bez parametara)");
            //const - nije dozvoljeno menjanje nakon deklaracije i inicijalizacije
            //INDEX_NUMBER = 7;
            //ako readonly atribut nije i statički onda se može ovde inicijalizovati
            //ClassMark = "stCLS";
        }

        static StaticMembersClass()
        {
            Console.WriteLine("Statički konstruktor");
            IndexCounter = 2;
            //readonly - dozvoljeno je menjanje nakon deklaracije i inicijalizacije
            //ali u okviru konstruktora, ne i kasnije
            ClassMark = "stCLS";
            StaticMethod();
            //nema smisla, lokalna promenljiva postoji samo u statickom bloku (konstruktoru)
            int a = 10;
            a++;
        }

        //statička metoda može da pristupi samo statičkim članovima klase
        public static void StaticMethod()
        {
            Console.WriteLine("\n");
            Console.WriteLine("Poziv staticke metode");
            Console.WriteLine("Vrednost atributa <static int IndexCounter> je: " + IndexCounter);
            Console.WriteLine("Vrednost atributa <static readonly string ClassMark> je: " + ClassMark);
            Console.WriteLine("Vrednost atributa <const int INDEX_NUMBER> je: " + INDEX_NUMBER);
            Console.WriteLine("\n");
            //readonly - nije dozvoljeno menjanje nakon deklaracije i inicijalizacije
            //samo u okviru konstruktora
            //ClassMark = "stCLS";
        }

        //nestatička metoda može da pristupi svim članovima klase
        public void Method()
        {
            Console.WriteLine("\n");
            Console.WriteLine("Poziv metode");
            Console.WriteLine("Vrednost atributa <static int IndexCounter> je: " + IndexCounter);
            Console.WriteLine("Vrednost atributa <static readonly string ClassMark> je: " + ClassMark);
            Console.WriteLine("Vrednost atributa <const int INDEX_NUMBER> je: " + INDEX_NUMBER);
            Console.WriteLine("\n");
        }
    }
}
