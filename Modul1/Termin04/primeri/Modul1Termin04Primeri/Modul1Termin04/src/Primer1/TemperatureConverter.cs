﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer1
{
    //staticka klasa, nema instanciranja i nasleđivanja
    public static class TemperatureConverter
    {
        public const string CELSIUS_MARK = "°C";
        public const string FAHRENHEIT_MARK = "°F";
        public const string KELVIN_MARK = "K";
        private const double KELVIN_CONST = 273.15;

        public static double CelsiusToFahrenheit(double temperatureCelsius)
        {
            // Convert Celsius to Fahrenheit.
            double fahrenheit = (temperatureCelsius * 9 / 5) + 32;
            return fahrenheit;
        }

        public static double FahrenheitToCelsius(double temperatureFahrenheit)
        {
            // Convert Fahrenheit to Celsius.
            double celsius = (temperatureFahrenheit - 32) * 5 / 9;
            return celsius;
        }

        public static double CelsiusToKelvin(double temperatureCelsius)
        {
            // Convert Celsius to Kelvin.
            double kelvin = temperatureCelsius + KELVIN_CONST;
            return kelvin;
        }

        public static double KelvinToCelsius(double temperatureKelvin)
        {
            // Convert Kelvin to Celsius.
            double celsius = temperatureKelvin - KELVIN_CONST;
            return celsius;
        }

        public static double FahrenheitToKelvin(double temperatureFahrenheit)
        {
            // Convert Fahrenheit to Celsius.
            double celsius = FahrenheitToCelsius(temperatureFahrenheit);
            // Convert Celsius to Kelvin.
            return CelsiusToKelvin(celsius);
        }

        public static double KelvinToFahrenheit(double temperatureKelvin)
        {
            // Convert Kelvin to Celsius.
            double celsius = KelvinToCelsius(temperatureKelvin);
            // Convert Celsius to Fahrenheit.
            return CelsiusToFahrenheit(celsius);
        }
    }
}
