﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer1
{
    class MainClass1
    {

        public static void Main(string[] args)
        {
            //pozivanje konstruktora bez parametra
            //pre pozivanja podrazumevanog konstruktora se poziva
            //statički konstruktor
            StaticMembersClass smc = new StaticMembersClass();
            //statički konstruktor se poziva samo tokom prvog pristupa objektu
            StaticMembersClass smc2 = new StaticMembersClass();

            //rad sa static atributima
            //ne može se pristupati putem objekta
            //smc.IndexCounter = 25;

            //statičkim atributima se pristupa isključivo na statički način
            StaticMembersClass.IndexCounter++;
            //ne mora postojati objekat da bi se pristupilo static atributu
            Console.WriteLine(StaticMembersClass.IndexCounter);

            //staticka metoda
            StaticMembersClass.StaticMethod();

            //static readonly atribut se moze procitati, ali se ne moze menjati
            Console.WriteLine(StaticMembersClass.ClassMark);
            //StaticMembersClass.ClassMark = "abc";

            //const atribut se takođe ne može menjati
            Console.WriteLine(StaticMembersClass.INDEX_NUMBER);
            //StaticMembersClass.INDEX_NUMBER = 123;

            //izmena statičkih atributa u jednom objektu je vidljiva u svakom
            smc.Method();

            //rad sa statickom klasom
            //ne može se instancirati statička klasa
            //TemperatureConverter tc = new TemperatureConverter();

            double tempC = 25.5;
            Console.WriteLine("Temperatura od {0}{1} iznosi {2}{3}.",
                tempC,
                TemperatureConverter.CELSIUS_MARK,
                TemperatureConverter.CelsiusToFahrenheit(tempC),
                TemperatureConverter.FAHRENHEIT_MARK);

            Console.WriteLine("Zavrsen rad sa static atributima i metodama");
            Console.WriteLine("-------------------------------------------");

            Console.ReadKey();
        }
    }
}
