﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer9.Samostalni
{
	public class IspitniRok
	{

		//atributi tj property-i
		private static int brojacId = 0;

		internal int BrojacId
		{
			get { return brojacId; }
			set { brojacId = value; }
		}

		internal int Id { get; set; }
		internal string Naziv { get; set; }
		internal string Pocetak { get; set; }
		internal string Kraj { get; set; }
		internal List<IspitnaPrijava> IspitnePrijave { get => ispitnePrijave; set => ispitnePrijave = value; }
		internal List<IspitniRok> IspitniRokovi { get => ispitniRokovi; set => ispitniRokovi = value; }

		private List<IspitniRok> ispitniRokovi;

		//ispitne prijave u isputnom roku
		private List<IspitnaPrijava> ispitnePrijave = new List<IspitnaPrijava>();

		public IspitniRok()
		{
			IspitniRokovi = new List<IspitniRok>();
		}

		public IspitniRok(string naziv, string pocetak, string kraj, int id =-1) : this()
		{
			if (id == -1)
			{
				id = brojacId++;
			}
			this.Id = id;
			this.Naziv = naziv;
			this.Pocetak = pocetak;
			this.Kraj = kraj;
		}


		public IspitniRok(string tekst)
		{
			string[] tokeni = tekst.Split(',');
			//npr. 		1,Januarski,2015-01-15,2015-01-29
			//tokeni 	0		1		2		3		

			//TO DO
			//URADJENO!!!
			if (tokeni.Length != 4)
			{
				Console.WriteLine("Nije dobro ocitavanje Ispitnih Rokova" + tekst);
				Environment.Exit(0);
			}

			Id = Int32.Parse(tokeni[0]);
			Naziv = tokeni[1];
			Pocetak = tokeni[2];
			Kraj = tokeni[3];

		}


		//metode

		//kraci naziv metode PreuzmiTekstualnuReprezentacijuKlaseZaDatoteku
		//implementirati isto ponašanje
		public string ToFileString()
		{
			//TO DO
			//URADJENO!!!
			StringBuilder sb = new StringBuilder();
			sb.Append(Id + "," + Naziv + "," + Pocetak + "," + Kraj);

			return sb.ToString();
		}

		public override string ToString()
		{
			return "Ispitni " + Naziv + " rok [id:" + Id + "] traje od " + Pocetak + " do " + Kraj;
		}

		//ispisati sve podatke sa ispitnim prijavama
		//po ugledu na metodu u klasi Predmet
		public string ToStringAll()
		{
			StringBuilder sb = new StringBuilder("Ispitni Rok [id:" + Id + "] " + Naziv + " " + Pocetak + Kraj + "\n");
			return sb.ToString();
		}

		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is IspitniRok))
				return false;

			IspitniRok ir = (IspitniRok)obj;

			return this.Id.Equals(ir.Id);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode() ^ Naziv.GetHashCode() ^ Pocetak.GetHashCode() ^ Kraj.GetHashCode();
		}
	}
}
