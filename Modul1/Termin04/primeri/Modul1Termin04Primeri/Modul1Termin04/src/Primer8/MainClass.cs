﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer8
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Osoba o = new Osoba();
            Console.WriteLine("Hello " + o.Ime);

            if (args.Length > 0)
            {
                Console.WriteLine("Ukupno je prosledjeno {0} parametara komandne linije",args.Length);
                for (int i = 0; i < args.Length; i++)
                {
                    Console.WriteLine("{0}. argument je: {1}",i+1,args[i]);
                }
            }
            
            Console.ReadKey();
        }

    }
}
