﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer5
{
    class ConsoleClass
    {

        public static int Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            CitajSaTastature();
            Console.WriteLine("Završena operacija čitanja/pisanja iz/u konzolu!\n");

            CitajSaPreusmerenogUlaznogIzlaznogToka(args);
            Console.WriteLine("Završena operacija čitanja/pisanja sa/na preusmereni tok datoteka!\n");

            Console.ReadKey();
            return 0;

        }

        static void CitajSaTastature()
        {
            Console.WriteLine("Čitanje sa tastature:");
            Console.WriteLine("Info: Ctr+Z za kraj\n----");
            string linija;
            int i = 1;

            while ((linija = Console.ReadLine())!= null)
            {
                Console.WriteLine("Linija broj {0}: {1}", i, linija);
                IspisiInfo(linija);
                i++;
                
            }
            Console.WriteLine("---");
        }

        private static void IspisiInfo(string linija)
        {
            long l; double d; char c;

            if (Int64.TryParse(linija, out l))
            {
                Console.WriteLine("INFO: Uneli ste celobrojnu vrednost!");
                //l = Int64.Parse(linija);
                //l = Convert.ToInt64(linija);
            }
            else if (Double.TryParse(linija, out d))
            {
                Console.WriteLine("INFO: Uneli ste decimalnu vrednost!");
                //d = Double.Parse(linija);
                //d = Convert.ToDouble(linija);
            }
            else if (Char.TryParse(linija, out c))
            {
                Console.WriteLine("INFO: Uneli ste karakter!");
                //c = Char.Parse(linija);
                //c = Convert.ToChar(linija);
            }
            else
            {
                Console.WriteLine("INFO: Uneli ste string!");
            }
        }

        static int CitajSaPreusmerenogUlaznogIzlaznogToka(string[] args)
        {
            char sep = Path.DirectorySeparatorChar;

            if (args.Length < 2)
            {
                Console.WriteLine("Čitanje/pisanje sa/na preusmereni tok datoteka zahteva prosleđeivanje parametara komandne linije!");
                Console.WriteLine("Potrebno je proslediti parametre putem komandne linije: inputfile.txt outputfile.txt");
                return 1;
            }

            string dataDirPutanja = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + sep + "data" + sep;
            string ulaznaDatoteka = dataDirPutanja + args[0], izlaznaDatoteka = dataDirPutanja + args[1];

            if (!File.Exists(ulaznaDatoteka))
            {
                Console.WriteLine("Ulazna datoteka ne postoji!");
                return 1;
            }
            else if (!File.Exists(izlaznaDatoteka))
            {
                //ako izlazna nije definisana, kreirati
                File.Create(izlaznaDatoteka);
            }

            //otvoriti stream za pisanje u izlaznu datoteku
            StreamWriter writer = new StreamWriter(izlaznaDatoteka);

            //preusmeriti standardni izlaz sa konzole na izlaznu datoteku
            Console.SetOut(writer);

            //preusmeriti standardni ulaz sa konzole na ulaznu datoteku
            StreamReader reader = new StreamReader(ulaznaDatoteka);
            Console.SetIn(reader);
            
            string linija;
            while ((linija = Console.ReadLine()) != null)
            {
                //može se dodati dodatna obrada podataka i npr. instanciranje objekata tipa Student
                Console.WriteLine(linija);
            }
            
            //kreirati i vratiti preusmereni standardni izlaz na konzolu
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            //automatsko pražnjenje buffer-a da ne bi ručno pozivali Console.Out.Flush();
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);

            Console.SetIn(new StreamReader(Console.OpenStandardInput()));
            
            //test standardnog toka za čitanje/pisanje
            //Console.ReadKey();
            //Console.WriteLine("Zavrseno");

            //bitno je da se prvo izvrši izmena tokova standardnog izlaza/ulaza
            //pa tek onda zatvorti prethodno otvorene tokova za čitanje/pisanje
            writer.Close();
            reader.Close();
            return 0;
        }
    }
}
