﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer4
{
    class IOClass
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            string nazivDatoteke = @".\..\..\data\TextFile1.txt";

            CitajIzDatoteke(nazivDatoteke);
            //PisiUDatoteku(nazivDatoteke);
            //CitajIzDatoteke(nazivDatoteke);

            Console.ReadKey();
        }

        static void CitajIzDatoteke(string nazivDatoteke)
        {
            Console.WriteLine(File.Exists(nazivDatoteke) ? "" : "Datoteka ne postoji ili putanja nije ispravna.");

            //1.način: korišćenje using bloka
            //using(StreamReader reader1 = new StreamReader(nazivDatoteke))
            using (StreamReader reader1 = File.OpenText(nazivDatoteke))
            {
                //metoda ReadToEnd koristi StringBuilder 
                //Console.WriteLine(reader1.ReadToEnd());

                string linija = "";
                while ((linija = reader1.ReadLine()) != null)
                {
                    Console.WriteLine(linija);
                }
            }

            //2. način: try/catch i Dispose()
            //StreamReader reader2 = new StreamReader(nazivDatoteke);
            //try
            //{
            //    string linija = "";
            //    while ((linija = reader2.ReadLine()) != null)
            //    {
            //        Console.WriteLine(linija);
            //    }
            //    reader2.Close();
            //}
            //catch (Exception)
            //{
            //    Console.WriteLine("Doslo je do greške prilikom čitanja!");
            //}
            //finally
            //{
            //    Console.WriteLine("Finally blok");
            //    //ovaj deo je automatski izvršavao using blok
            //    if (reader2 != null)
            //    {
            //        ((IDisposable)reader2).Dispose();
            //    }
            //}

            //primer čitanja podataka studenata iz csv datoteke
            //while ((linija = reader2.ReadLine()) != null)
            //{
            //    string[] tokeni = linija.Split(',');
            //    int id = Int32.Parse(tokeni[0]);
            //    string indeks = tokeni[1];
            //    string prezime = tokeni[2];
            //    string ime = tokeni[3];
            //    string grad = tokeni[4];

            //    Student st = new Student(id, ime, prezime, grad, indeks);
            //    ListaStudenata.Add(st);
            //}

        }

        static void PisiUDatoteku(string nazivDatoteke)
        {
            Console.WriteLine(File.Exists(nazivDatoteke) ? "" : "Datoteka ne postoji ili putanja nije ispravna.");

            //1.način: korišćenje using bloka
            using (StreamWriter writer = new StreamWriter(nazivDatoteke, true, Encoding.UTF8))
            {
                writer.WriteLine(); //dodajemo nov red kako bi pisali u novom redu
                writer.WriteLine("У постојећу датотеку дописујемо још");
                writer.WriteLine("два реда текста.");
            }

            //primer pisanja podataka studenata u csv datoteku
            //foreach (Student s in ListaStudenata)
            //{
            //    writer.WriteLine(s.Id + "," + s.Indeks + ","
            //           + s.Prezime + "," + s.Ime + "," + s.Grad);
            //}
        }
    }
}
