﻿using Modul1Termin04.Primer7.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IspitnaPrijava = Modul1Termin04.Primer9.Samostalni.IspitnaPrijava;

namespace Modul1Termin04.src.Primer7.UI
{
	class IspitnaPrijavaUI
	{
		public static List<IspitnaPrijava> listaIspitnihPrijava { get; set; }
		static IspitnaPrijavaUI()
		{
			listaIspitnihPrijava = new List<IspitnaPrijava>();
		}

		private static void IspisiMeni()
		{
			Console.WriteLine("Rad sa Ispitnim Rokovima studenta - opcije:");
			Console.WriteLine("\tOpcija broj 1 - unos podataka o ispitnoj prijavi"); // DONE
			Console.WriteLine("\tOpcija broj 2 - izmena podataka o ispitnoj prijavi");
			Console.WriteLine("\tOpcija broj 3 - ispis podataka svih ispitnih prijava"); // DONE
			Console.WriteLine("\tOpcija broj 4 - ispis podataka o odredjenoj ispitnoj prijavi");
			Console.WriteLine("\t\t ...");
			Console.WriteLine("\tOpcija broj 0 - POVRATAK NA GLAVNI MENI");
		}

		public static void MeniIspitnaPrijavaUI()
		{
			int odluka = -1;

			while (odluka != 0)
			{
				IspisiMeni();
				Console.Write("Opcije:");
				odluka = IOPomocnaKlasa.OcitajCeoBroj();
				Console.Clear();
				switch (odluka)
				{
					case 1:
						UnosPodatakaOIspitnojPrijavi();
						break;

					case 2:
						IzmenePodatakaOIspitnojPrijavi();
						break;
					case 3:
						IspisiPodatakaSvihIspitnihPrijava();
						break;

					case 4:
						IspitnaPrijava ir = IspisiPodatakaOdredjenomIspitnojPrijavi();
						if (ir != null)
						{
							Console.WriteLine(ir.ToStringAll());
						}
						break;

					default:
						Console.WriteLine("Nepostojeca komanda!\n\n");
						break;
				}


			}
		}

		private static void IspisiPodatakaSvihIspitnihPrijava()
		{
			throw new NotImplementedException();
		}

		private static void IzmenePodatakaOIspitnojPrijavi()
		{
			throw new NotImplementedException();
		}

		private static void UnosPodatakaOIspitnojPrijavi()
		{
			throw new NotImplementedException();
		}

		internal static void SacuvajIspitnePrijaveUDatoteku(string nazivDatoteke)
		{
			if (File.Exists(nazivDatoteke))
			{
				using (StreamWriter writer = new StreamWriter(nazivDatoteke, false, Encoding.UTF8))
				{
					foreach (IspitnaPrijava s in listaIspitnihPrijava)
					{
						writer.WriteLine(s.ToFileString());
					}
				}
			}
			else
			{
				Console.WriteLine("Datoteka ne postoji ili putanja nije ispravna.");
			}
		}

		public static void UcitajIspitnePrijaveIzDatoteke(string nazivDatoteke)
		{
			if (File.Exists(nazivDatoteke))
			{
				using (StreamReader st = File.OpenText(nazivDatoteke))
				{
					string linija = "";
					while ((linija = st.ReadLine()) != null)
					{
						listaIspitnihPrijava.Add(new IspitnaPrijava(linija));
					}
				}
			}
			else
			{
				Console.WriteLine("Datoteka ne postoji ili putanja nije ispravna.");
			}
		}

		/** METODE ZA PRETRAGU ISPITNIH PRIJAVA ****/
		// pronadji Ispitna prijava
		public static IspitnaPrijava IspisiPodatakaOdredjenomIspitnojPrijavi()
		{
			IspitnaPrijava ispitniRok = null;
			Console.WriteLine("Unesi id Ispitne prijave:");
			int id = IOPomocnaKlasa.OcitajCeoBroj();
			ispitniRok = IspisiPodatakaOdredjenomIspitnojPrijavi(id);

			if (ispitniRok == null)
			{
				Console.WriteLine("Ispitna prijava sa id : " + id.ToString() + " Ne postoji u evidenciji!");
			}

			return ispitniRok;
		}


		//pronadji ispitnu prijavu
		public static IspitnaPrijava IspisiPodatakaOdredjenomIspitnojPrijavi(int id)
		{
			IspitnaPrijava ispitnaPrijava = null;

			for (int i = 0; i < listaIspitnihPrijava.Count; i++)
			{
				IspitnaPrijava ir = listaIspitnihPrijava[i];

				if (ir.Id == id)
				{
					ispitnaPrijava = ir;
					break;
				}
			}
			return ispitnaPrijava;
		}
	}
}