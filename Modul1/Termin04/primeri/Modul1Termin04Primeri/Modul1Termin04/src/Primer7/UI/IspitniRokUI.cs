﻿using Modul1Termin04.Primer7.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IspitniRok = Modul1Termin04.Primer9.Samostalni.IspitniRok;

namespace Modul1Termin04.src.Primer7.UI
{
	class IspitniRokUI
	{
		public static List<IspitniRok> listaIspitnihRokova { get; set; }
		static IspitniRokUI()
		{
			listaIspitnihRokova = new List<IspitniRok>();
		}
		private static void IspisiMeni()
		{
			Console.WriteLine("Rad sa Ispitnim Rokovima studenta - opcije:");
			Console.WriteLine("\tOpcija broj 1 - unos podataka o ispitnom roku"); // DONE
			Console.WriteLine("\tOpcija broj 2 - izmena podataka o ispitnom roku");
			Console.WriteLine("\tOpcija broj 3 - ispis podataka svih ispitnih rokova"); // DONE
			Console.WriteLine("\tOpcija broj 4 - ispis podataka o odredjenom ispitnom roku");
			Console.WriteLine("\t\t ...");
			Console.WriteLine("\tOpcija broj 0 - POVRATAK NA GLAVNI MENI");
		}

		public static void MeniIspitniRokUI()
		{
			int odluka = -1;

			while (odluka != 0)
			{
				IspisiMeni();
				Console.Write("Opcije:");
				odluka = IOPomocnaKlasa.OcitajCeoBroj();
				Console.Clear();
				switch (odluka)
				{
					case 1:
						UnosPodatakaOIspitnomRoku();
						break;

					case 2:
						IzmenePodatakaOIspitnomRoku();
						break;
					case 3:
						IspisiPodatakaSvihIspitnihRokova();
						break;

					case 4:
						IspitniRok ir = IspisiPodatakaOdredjenomIspitnomRoku();
						if (ir != null)
						{
							Console.WriteLine(ir.ToStringAll());
						}
						break;

					default:
						Console.WriteLine("Nepostojeca komanda!\n\n");
						break;

				}
			}
		}

		public static void UnosPodatakaOIspitnomRoku()
		{

			Console.WriteLine("Unesi Naziv Ispitnog roka radi provere da li vec postoji:");
			String naziv = IOPomocnaKlasa.OcitajTekst();
			naziv = naziv.ToUpper();
			while (PronadjiIspitniRokPoNazivu(naziv) != null)
			{
				Console.WriteLine("Ispitni rok sa nazivom: " + naziv + " vec postoji");
				naziv = IOPomocnaKlasa.OcitajTekst();
				UnosPodatakaOIspitnomRoku();
			}
			Console.WriteLine("Unesi naziv:");
			String iRokNaziv = IOPomocnaKlasa.OcitajTekst();
			Console.WriteLine("Unesi pocetak:");
			String iRokPocetak = IOPomocnaKlasa.OcitajTekst();
			Console.WriteLine("Unesi kraj:");
			String iRokKraj = IOPomocnaKlasa.OcitajTekst();

			//ID atribut ce se dodeliti automatski
			IspitniRok st = new IspitniRok(naziv, iRokPocetak, iRokKraj);
			listaIspitnihRokova.Add(st);

		}

		//pronadji ispitni rok po nazivu
		public static IspitniRok PronadjiIspitniRokPoNazivu(String naziv)
		{
			IspitniRok retVal = null;
			for (int i = 0; i < listaIspitnihRokova.Count; i++)
			{
				IspitniRok st = listaIspitnihRokova[i];
				if (st.Naziv.Equals(naziv))
				{
					retVal = st;
					break;
				}
			}
			return retVal;
		}

		public static void IzmenePodatakaOIspitnomRoku()
		{
			IspitniRok ispRok = IspisiPodatakaOdredjenomIspitnomRoku();
			if (ispRok != null)
			{
				Console.WriteLine("Unesi naziv roka:");
				String naziv = IOPomocnaKlasa.OcitajTekst();
				Console.WriteLine("Unesi pocetak roka:");
				String pocetakRoka = IOPomocnaKlasa.OcitajTekst();
				Console.WriteLine("Unesi kraj roka:");
				String krajRoka = IOPomocnaKlasa.OcitajTekst();

				ispRok.Naziv = naziv;
				ispRok.Pocetak = pocetakRoka;
				ispRok.Kraj = krajRoka;


			}
		}

		public static void IspisiPodatakaSvihIspitnihRokova()
		{

			if (listaIspitnihRokova != null)
			{
				for (int i = 0; i < listaIspitnihRokova.Count; i++)
				{
					Console.WriteLine(listaIspitnihRokova[i].ToString());
				}
			}
			else
			{
				Console.WriteLine("Ispitni rokovi ne postoje u evidenciji!");
			}

		}

		/** METODE ZA PRETRAGU ISPITNIH ROKOVA ****/
		// pronadji Ispitni Rok
		public static IspitniRok IspisiPodatakaOdredjenomIspitnomRoku()
		{
			IspitniRok ispitniRok = null;
			Console.WriteLine("Unesi id Ispitnog Roka:");
			int id = IOPomocnaKlasa.OcitajCeoBroj();
			ispitniRok = IspisiPodatakaOdredjenomIspitnomRoku(id);

			if (ispitniRok == null)
			{
				Console.WriteLine("Ispitni rok sa id : " + id.ToString() + " Ne postoji u evidenciji!");
			}

			return ispitniRok;
		}


		//pronadji ispitni rok
		public static IspitniRok IspisiPodatakaOdredjenomIspitnomRoku(int id)
		{
			IspitniRok ispitniRok = null;

			for (int i = 0; i < listaIspitnihRokova.Count; i++)
			{
				IspitniRok ir = listaIspitnihRokova[i];

				if (ir.Id == id)
				{
					ispitniRok = ir;
					break;
				}
			}
			return ispitniRok;
		}

		public static void UcitajIspitneRokoveIzDatoteke(string nazivDatoteke)
		{
			if (File.Exists(nazivDatoteke))
			{
				using (StreamReader st = File.OpenText(nazivDatoteke))
				{
					string linija = "";
					while ((linija = st.ReadLine()) != null)
					{
						listaIspitnihRokova.Add(new IspitniRok(linija));
					}
				}
			}
			else
			{
				Console.WriteLine("Datoteka ne postoji ili putanja nije ispravna.");
			}
		}

		internal static void SacuvajIspitneRokoveUDatoteku(string nazivDatoteke)
		{
			if (File.Exists(nazivDatoteke))
			{
				using (StreamWriter writer = new StreamWriter(nazivDatoteke, false, Encoding.UTF8))
				{
					foreach (IspitniRok s in listaIspitnihRokova)
					{
						writer.WriteLine(s.ToFileString());
					}
				}
			}
			else
			{
				Console.WriteLine("Datoteka ne postoji ili putanja nije ispravna.");
			}
		}
	}
}

