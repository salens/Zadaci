﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer3
{
    class FileClass
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            string nazivDatoteke = @".\..\..\data\TextFile1.txt";
            CitajIzDatoteke(nazivDatoteke);
            //PisiUDatoteku(nazivDatoteke);
            Console.ReadKey();

        }


        static void CitajIzDatoteke(string nazivDatoteke)
        {
            //čitanje iz datoteke
            string[] tekstualniRedovi = File.ReadAllLines(nazivDatoteke);
            Console.WriteLine("Broj redova:  " + tekstualniRedovi.Length);
            foreach (string red in tekstualniRedovi)
            {
                Console.WriteLine(red);
            }
            //2. nacin
            //pogodan za velike datoteke čije učitavanje u memoriju izazivaju
            //izuzetak OutOfMemoryException
            //foreach (var linija in File.ReadLines(nazivDatoteke))
            //{
            //    Console.WriteLine(linija);
            //}

            FileInfo fi = new FileInfo(nazivDatoteke);
            if (fi.Exists)
            {
                using (StreamReader sr = fi.OpenText())
                {
                    //Console.WriteLine(">"+(char)sr.Read());
                    Console.WriteLine(sr.ReadLine());
                }
            }

        }
        static void PisiUDatoteku(string nazivDatoteke)
        {
            //ako ne postoji, napraviti datoteku
            if (!File.Exists(nazivDatoteke))
            {
                string[] createText = { "\t\tPrimer", "naslova" };
                File.WriteAllLines(nazivDatoteke, createText);
                //File.WriteAllText(nazivDatoteke, "\t\tPrimer nekog naslova");                
            }

            //"\r\n" za non-Unix platforme,"\n" za Unix platforme.
            string tekst = "Dodajemo još jedan red teksta." + Environment.NewLine;
            File.AppendAllText(nazivDatoteke, tekst);

            FileInfo fi = new FileInfo(nazivDatoteke);
            if (fi.Exists)
            {
                using (StreamWriter sw = fi.AppendText())
                {
                    sw.WriteLine("Podvlačimo crtu za kraj \r\n" + new String('_', 50));
                }
            }
        }

    }
}
