﻿FTN Informatika
Kurs iz .NET Web programiranja
=================================

1. Opis vežbe
-------------------
- Upoznavanje sa IO sistemom, ključna reč static


2. Sadržaj vežbe
-------------------
- Statičke klase, atributi, metode, konstruktori 
- - static vs. const vs. readonly
- Upoznavanje sa IO sistemom (System.IO)
- - rad sa sistemom datoteka i direktorijuma
- - rad sa tokovima podataka
- - čitanje karaktera sa tastature i pisanje karaktera u konzolu
- Pokretanje aplikacije putem komandne linije
- Aplikacija za evidentiranje studenata koja ima spregu sa sistemom datoteka
- - čuvanje podataka u .csv datotekama

3. Literatura
-------------------
materijali/Modul1Termin04.pdf
materijali/msdn/*.pdf
materijali/*.pdf

4. Primeri
-------------------
primeri\Modul1Termin04Primeri

5. Zadatak na času
-------------------
zadaci\Modul1Termin04 - Zadaci.pdf
zadaci\Modul1Termin04 - Dodatni zadaci.pdf 

6. Domaći zadatak
-------------------
Sve što ne stignete na času.


