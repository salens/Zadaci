﻿using Modul3Vezbe1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
namespace Modul3Vezbe1.Controllers
{
    public class CarsController : Controller
    {
        CarContext db = new CarContext();
        // GET: Cars
        public ActionResult Index()
        {
  
            return View(db.Cars.Include(m=>m.Engine).ToList());
        }

        // GET: Cars/Details/5
        public ActionResult Details(int id)
        {

            return View(db.Cars.Find(id));
        }

        // GET: Cars/Create
        public ActionResult Create()
        {
           ViewBag.Engines = db.Engines.ToList();

            return View();
        }

        // POST: Cars/Create
        [HttpPost]
        public ActionResult Create(Car car) // menjamo iz Form collection u Car objekat
        {
            try
            {
                if (ModelState.IsValid) // dodajemo model state
                {

                    db.Cars.Add(car);  // Dodavanje u bazu
                    db.SaveChanges();// SNIMANJE U BAZU
                    return RedirectToAction("Index");
                }
              
            }
            catch
            {
              
            }

            ViewBag.Engines = db.Engines.ToList();
            return View(car);
        }

        // GET: Cars/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Engines = db.Engines.ToList();
            return View(db.Cars.Find(id));
        }

        // POST: Cars/Edit/5
        [HttpPost]
        public ActionResult Edit(Car car)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry<Car>(car).State = EntityState.Modified; // ovde da ne bi dodavao novi entitet vec da izmeni taj odredjeni UPDATE
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }

            }
            catch
            {
            
            }
            ViewBag.Engines = db.Engines.ToList();
            return View(car);
        }

        // GET: Cars/Delete/5
        public ActionResult Delete(int id)
        {

            var car = db.Cars
                .Include(c => c.Engine)
                .Where(c=>c.Id == id)
                .FirstOrDefault();
            return View(car);
        }

        // POST: Cars/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var car = db.Cars.Find(id);

                if (car != null)
                {
                    db.Cars.Remove(car);
                    db.SaveChanges();
                }
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
