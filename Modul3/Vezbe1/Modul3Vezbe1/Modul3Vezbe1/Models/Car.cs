﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Modul3Vezbe1.Models
{
    [Table("Cars")]
    public class Car
    {  //za table atribut treba using System.ComponentModel.DataAnnotations.Schema
        public int Id { get; set; } // ili CarId - nije bitna konvencija CaseSensitive bice automatski identity po konvenciji i ovo ce biti privatni kljuc

        [Required]
        [StringLength(50)]
        public string Model { get; set; }

        [Required]
        [StringLength(50)]
        public string Proizvodjac { get; set; }

        [Range(750, 7000)]
        public int Kubikaza { get; set; }

        [Range(1950, int.MaxValue)]
        [Column("Year")]
        public int Godiste { get; set; }

        [Required]
        [StringLength(20)]
        public string Boja { get; set; }

   
        public int EngineId { get; set; }
        [ForeignKey("EngineId")] // ovde pisemo od klase u ovom slucaju "Engine" kao sto je dole, ako namapiramo na klasu a ne na Id onda naziv ID-ja ovog ispod
        [Column("EngineName")]
        public Engine Engine { get; set; }
    }
}