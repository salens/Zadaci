namespace Modul3Vezbe1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCar : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(),
                        Proizvodjac = c.String(),
                        Kubikaza = c.Int(nullable: false),
                        Godiste = c.Int(nullable: false),
                        Boja = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Cars");
        }
    }
}
