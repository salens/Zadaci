namespace Modul3Vezbe1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedCarConstraints : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Cars", name: "Godiste", newName: "Year");
            AlterColumn("dbo.Cars", "Model", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Cars", "Proizvodjac", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Cars", "Boja", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cars", "Boja", c => c.String(maxLength: 20));
            AlterColumn("dbo.Cars", "Proizvodjac", c => c.String(maxLength: 50));
            AlterColumn("dbo.Cars", "Model", c => c.String(maxLength: 50));
            RenameColumn(table: "dbo.Cars", name: "Year", newName: "Godiste");
        }
    }
}
