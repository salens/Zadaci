namespace Modul3Vezbe1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCarConstraints : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cars", "Model", c => c.String(maxLength: 50));
            AlterColumn("dbo.Cars", "Proizvodjac", c => c.String(maxLength: 50));
            AlterColumn("dbo.Cars", "Boja", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cars", "Boja", c => c.String());
            AlterColumn("dbo.Cars", "Proizvodjac", c => c.String());
            AlterColumn("dbo.Cars", "Model", c => c.String());
        }
    }
}
