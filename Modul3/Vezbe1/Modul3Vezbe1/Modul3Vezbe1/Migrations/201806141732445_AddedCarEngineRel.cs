namespace Modul3Vezbe1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCarEngineRel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cars", "EngineId", c => c.Int(nullable: false));
            CreateIndex("dbo.Cars", "EngineId");
            AddForeignKey("dbo.Cars", "EngineId", "dbo.Engines", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "EngineId", "dbo.Engines");
            DropIndex("dbo.Cars", new[] { "EngineId" });
            DropColumn("dbo.Cars", "EngineId");
        }
    }
}
