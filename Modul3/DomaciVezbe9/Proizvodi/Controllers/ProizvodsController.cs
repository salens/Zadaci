﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Proizvodi.Models;

namespace Proizvodi.Controllers
{
    public class ProizvodsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET api/products
        public IQueryable<Proizvod> GetProizvod()
        {
            return db.Proizvods;
        }

        // GET api/products/1
        [Authorize]
        public IHttpActionResult GetProizvod(int id)
        {
            Proizvod proizvod = db.Proizvods.Find(id);
            if (proizvod == null)
            {
                return NotFound();
            }
            return Ok(proizvod);
        }

        [Authorize]
        public IHttpActionResult Post(Proizvod proizvod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Proizvods.Add(proizvod);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = proizvod.Id }, proizvod);
        }

        [Authorize]
        public IHttpActionResult Put(int id, Proizvod proizvod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != proizvod.Id)
            {
                return BadRequest();
            }

            db.Entry(proizvod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch
            {
                return BadRequest();
            }

            return Ok(proizvod);
        }

        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            var drzava = db.Proizvods.FirstOrDefault(p => p.Id == id);
            if (drzava == null)
            {
                return NotFound();
            }

            db.Proizvods.Remove(drzava);
            db.SaveChanges();
            return Ok();
        }

    }
}
