namespace Proizvodi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Proizvodi.Models;
    internal sealed class Configuration : DbMigrationsConfiguration<Proizvodi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Proizvodi.Models.ApplicationDbContext context)
        {
            context.Proizvods.AddOrUpdate(x => x.Id,
                 new Proizvod() { Id = 1, Naziv = "Product 1", Cena = 10.87, DatumProizvodnje = new DateTime(2015, 6, 1, 12, 32, 30) },
                 new Proizvod() { Id = 2, Naziv = "Product 2", Cena = 20.56, DatumProizvodnje = new DateTime(2016, 6, 1, 12, 32, 30) },
                 new Proizvod() { Id = 3, Naziv = "Product 3", Cena = 30.34, DatumProizvodnje = new DateTime(2014, 6, 1, 12, 32, 30) }
                 );
        }
    }
}
