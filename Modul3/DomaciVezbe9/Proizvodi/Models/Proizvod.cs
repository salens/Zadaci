﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proizvodi.Models
{
    public class Proizvod
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public double Cena { get; set; }
        public DateTime DatumProizvodnje { get; set; }

    }
}