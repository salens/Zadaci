﻿$(document).ready(function () {

    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var formAction = "Create"; // dodavanje proizvoda
    var editingId; // id za PUT

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display", "none");

    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");

        }).fail(function (data) {
            alert(data);
        });


    });


    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");
            $(".proizvodi").css("display", "block");

        }).fail(function (data) {
            alert(data);
        });
    });

    // odjava korisnika sa sistema
    $("#odjavise").click(function () {
        token = null;
        headers = {};

        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $(".proizvodi").css("display", "none");
        $("#info").empty();
        $("#sadrzaj").empty();

    })

    // pripremanje dogadjaja za brisanje
    $("body").on("click", "#btnDelete", deleteProizvod);

    // priprema dogadjaja za izmenu
    $("body").on("click", "#btnEdit", editProizvod);

    // ucitavanje proizvoda
    $("#proizvodi").click(function () {

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/proizvods",
            "headers": headers

        }).done(function (data , status) {
            clearForm();
            var $container = $("#data");
            $container.empty();
           
            if (status == "success") {
                console.log(data);
                // ispis Drzava

                var div = $("<div></div>");
                var h1 = $("<h1>Proizvodi</h1>");
                div.append(h1);

                // ispis tabele
                var table = $("<table class='table table-hover table-bordered'></table>");
                var header = $("<tr><td>Id</td><td>Proizvod Naziv</td><td>Proizvod Cena</td><td>Proizvod DatumProizvodnje</td><td>Delete</td><td>Edit</td></tr>");
                table.append(header);

                for (i = 0; i < data.length; i++) {
                    // prikazujemo novi red u tabeli
                    var row = "<tr>";
                    // prikaz podataka
                    var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Naziv + "</td>" + "</td><td>" + data[i].Cena + "</td>"+ "</td><td>" + data[i].DatumProizvodnje + "</td>";
                    // prikaz dugmadi za izmenu i brisanje
                    var stringId = data[i].Id.toString();
                    var displayDelete = "<td><button class='btn-danger' id=btnDelete name=" + stringId + ">Delete</button></td>";
                    var displayEdit = "<td><button class='btn-primary' id=btnEdit name=" + stringId + ">Edit</button></td>";
                    row += displayData + displayDelete + displayEdit + "</tr>";
                    table.append(row);
                    newId = data[i].Id;

                    $('#drzavaId').append('<option value="' + data[i].Id + '">' + data[i].Ime + '</option>');
                    console.log(data);
                }

                div.append(table);

                // prikaz forme
                $("#formDiv").css("display", "block");

                // ispis novog sadrzaja
                $container.append(div);

            }
            else {
                var div = $("<div></div>");
                var h1 = $("<h1>Greška prilikom preuzimanja Drzave!</h1>");
                div.append(h1);
                $container.append(div);
            }

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });


    });

    // Brisanje Proizvoda
    function deleteProizvod() {

        // izvlacimo {id}
        var deleteID = this.name;
        // saljemo zahtev 
        $.ajax({
            "type": "DELETE",
            "url": "http://" + host + "/api/proizvods/" + deleteID.toString(),
            "headers": headers
        }).done(function (data, status) {
                refreshTable();
        }).fail(function (data, status) {
                alert("Desila se greska!");
        });

    };

    // Izmena Proizvoda
    function editProizvod() {
        // izvlacimo id
        var editId = this.name;
        // saljemo zahtev da dobavimo te drzave
        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/proizvods/" + editId.toString(),
            "headers": headers
        }).done(function (data, status) {
            $("#proizvodIme").val(data.Naziv);
            $("#proizvodCena").val(data.Cena);
            $("#proizvodDatum").val(data.DatumProizvodnje);
            editingId = data.Id;
            formAction = "Update";
        }).fail(function (data, status) {
            formAction = "Create";
            alert("Desila se greska!");
        });

    };

    // dodavanje novog proizvoda i edit proizvoda
    $("#proizvodForm").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        var proizvodIme = $("#proizvodIme").val();
        var proizvodCena = $("#proizvodCena").val();
        var proizvodDatum = $("#proizvodDatum").val();
        var httpAction;
        var sendData;
        var url;

        // u zavisnosti od akcije pripremam objekat
        if (formAction === "Create") {
            httpAction = "POST";
            url = "http://" + host + "/api/proizvods/";
            sendData = {
                "Naziv": proizvodIme,
                "Cena": proizvodCena,
                "DatumProizvodnje": proizvodDatum
            };
        }
        else {
            httpAction = "PUT";
            url = "http://" + host + "/api/proizvods/" + editingId.toString();
            sendData = {
                "Id": editingId,
                "Naziv": proizvodIme,
                "Cena": proizvodCena,
                "DatumProizvodnje": proizvodDatum
            };
        }

        console.log("Objekat za slanje");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            "headers": headers,
            data: sendData
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            })

    });

    // osvezi prikaz tabele
    function refreshTable() {
        // cistim formu
        $("#proizvodIme").val('');
        $("#proizvodCena").val('');
        $("#proizvodDatum").val('');

        // osvezavam
        $("#proizvodi").trigger("click");
    };

    // osvezi form inputa
    function clearForm() {
        // cistim formu
        $("#proizvodIme").val('');
        $("#proizvodCena").val('');
        $("#proizvodDatum").val('');

    };

});