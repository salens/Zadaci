﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Skola.Models
{
    public class School
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Mesto { get; set; }
        public int GodinePocetkaRada { get; set; }

        public School(int id, string ime, string mesto, int godinePocetkaRada)
        {
            Id = id;
            Ime = ime;
            Mesto = mesto;
            GodinePocetkaRada = godinePocetkaRada;
        }
  
    }
}