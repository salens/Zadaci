﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Skola.Models;
namespace Skola.Controllers
{
    public class SchoolController : ApiController
    {
       static List<School> skole = new List<School>()
        {
            new School(0, "Tesla","Novi Sad", 1945),
            new School(1, "Mihajlo Pupin","Novi Sad", 1995),
            new School(2, "Elektro","Beograd", 1955)
        };

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, skole);
        }

        public HttpResponseMessage Get(int id, string name)
        {
            foreach (School rad in skole)
            {
                if (name.ToLower().Equals(rad.Ime.ToLower()))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, rad);
                }
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, id);
        }

        public School Post(School skola)
        {
            if (skola != null)
            {
                skole.Add(skola);
                return skola;
            }
            return skola;
        }

        public School Put(int id, School rad)
        {

            if (id == rad.Id)
            {
                //skole.RemoveAt(id);
                skole.Insert(id, rad);
                return rad;
            }
            return rad;
        }

        public HttpResponseMessage Delete(int id)
        {
            foreach (School rad in skole)
            {
                if (rad.Id == id)
                {
                    skole.RemoveAt(rad.Id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, id);
        }
    }
}
