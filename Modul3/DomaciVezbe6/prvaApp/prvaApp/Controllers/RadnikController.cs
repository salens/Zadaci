﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using prvaApp.Models;
namespace prvaApp.Controllers
{
    public class RadnikController : ApiController
    {
        List<Radnik> radnici = new List<Radnik>()
        {
            new Radnik(1, "Stanislav","Senc", 45),
            new Radnik(2, "Savko","Senc", 95),
            new Radnik(3, "Marko","Senc", 55),
            new Radnik(4, "Bane","Banic",35)
        };
        
        public HttpResponseMessage Get()
        {          
            return Request.CreateResponse(HttpStatusCode.OK, radnici);
        }

        public HttpResponseMessage Get(int id, string name)
        {
            foreach (Radnik rad in radnici)
            {
                if (name.Equals(rad.Ime))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, rad);
                }
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, id);
        }

        public Radnik Post(Radnik radnik)
        {
            if(radnik != null)
            {
                radnici.Add(radnik);
                return radnik;
            }
            return radnik;
        }

        public Radnik Put(int id, Radnik rad)
        {

            if(id == rad.Id)
            {
                return rad;
            }
            return rad;
        }

        public HttpResponseMessage Delete(int id)
        {
            foreach (Radnik rad in radnici)
            {
                if (rad.Id == id)
                {
                    radnici.RemoveAt(rad.Id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, id);
        }

    }
}
