﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace prvaApp.Models
{
    public class Radnik
    {
        private Radnik radnik;

        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public int Godine { get; set; }

        public Radnik(int id, string ime, string prezime, int godine)
        {
            Id = id;
            Ime = ime;
            Prezime = prezime;
            Godine = godine;
        }
    }
}
