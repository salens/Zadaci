﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Hotel.Models;
using PagedList;

namespace Hotel.Controllers
{
    public class SobasController : Controller
    {
        private HotelContext db = new HotelContext();

        public enum SortTypes
        {
            BrojKreveta,
            BrojKrevetaDesc,
            CenaNocenja,
            CenaNocenjaDesc
        };

        public static Dictionary<string, SortTypes> SortTypeDict = new Dictionary<string, SortTypes>
        {
            {"BrojKreveta" , SortTypes.BrojKreveta },
            {"BrojKreveta Descending" , SortTypes.BrojKrevetaDesc },
            {"CenaNocenja" , SortTypes.CenaNocenja },
            {"CenaNocenja Descending" , SortTypes.CenaNocenjaDesc }

        };

        private int elementsPerPage = 2;

        // GET: Sobas
        public ActionResult Index(string sortType = "BrojKreveta", int page = 1)
        {
            SortTypes sortBy = SortTypeDict[sortType];

            var sobas = db.Sobas.Include(s => s.Smestaj);

            switch (sortBy)
            {
                case SortTypes.BrojKreveta:
                    sobas = sobas.OrderBy(x => x.BrojKreveta);
                    break;
                case SortTypes.BrojKrevetaDesc:
                    sobas = sobas.OrderByDescending(x => x.BrojKreveta);
                    break;
                case SortTypes.CenaNocenja:
                    sobas = sobas.OrderBy(x => x.CenaPoNocenju);
                    break;
                case SortTypes.CenaNocenjaDesc:
                    sobas = sobas.OrderByDescending(x => x.CenaPoNocenju);
                    break;
               
            }

            ViewBag.sortTypes = new SelectList(SortTypeDict, "Key", "Key", sortType);
            ViewBag.CurrentSortType = sortType;


            return View(sobas.ToPagedList(page, elementsPerPage));

        }

        // GET: Sobas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Sobas.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // GET: Sobas/Create
        public ActionResult Create()
        {
            ViewBag.SmestajId = new SelectList(db.Smestajs, "Id", "Naziv");
            return View();
        }

        // POST: Sobas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BrojSobe,BrojKreveta,CenaPoNocenju,SmestajId")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Sobas.Add(soba);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SmestajId = new SelectList(db.Smestajs, "Id", "Naziv", soba.SmestajId);
            return View(soba);
        }

        // GET: Sobas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Sobas.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            ViewBag.SmestajId = new SelectList(db.Smestajs, "Id", "Naziv", soba.SmestajId);
            return View(soba);
        }

        // POST: Sobas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BrojSobe,BrojKreveta,CenaPoNocenju,SmestajId")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Entry(soba).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SmestajId = new SelectList(db.Smestajs, "Id", "Naziv", soba.SmestajId);
            return View(soba);
        }

        // GET: Sobas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Sobas.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // POST: Sobas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Soba soba = db.Sobas.Find(id);
            db.Sobas.Remove(soba);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Filter(SobaFilter filter)
        {
            var sobas = db.Sobas.Include(s => s.Smestaj);

            if (filter.BrojKreveta != null)
            {
                sobas = sobas.Where(p => p.BrojKreveta == filter.BrojKreveta);
            }

            if (!filter.SmestajNaziv.IsNullOrWhiteSpace())
            {
                sobas = sobas.Where(p => p.Smestaj.Naziv.Contains(filter.SmestajNaziv));
            }

            return View(sobas.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
