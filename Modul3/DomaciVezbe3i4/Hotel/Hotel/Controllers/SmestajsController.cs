﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hotel.Models;
using Microsoft.Ajax.Utilities;
using PagedList;

namespace Hotel.Controllers
{
    public class SmestajsController : Controller
    {
        private HotelContext db = new HotelContext();

        private int elementsPerPage = 2;


        public enum SortTypes
        {
            Naziv,
            NazivDesc,
            Ocena,
            OcenaDesc
        };

        public static Dictionary<string, SortTypes> SortTypeDict = new Dictionary<string, SortTypes>
        {
            {"Naziv" , SortTypes.Naziv },
            {"Naziv Descending" , SortTypes.NazivDesc },
            {"Ocena" , SortTypes.Ocena },
            {"Ocena Descending" , SortTypes.OcenaDesc }

        };

        // GET: Smestajs
        public ActionResult Index(string sortType = "Naziv", int page = 1)
        {
            SortTypes sortBy = SortTypeDict[sortType];

            IQueryable<Smestaj> smestajs = db.Smestajs;

            switch (sortBy)
            {
                case SortTypes.Naziv:
                    smestajs = smestajs.OrderBy(x => x.Naziv);
                    break;
                case SortTypes.NazivDesc:
                    smestajs = smestajs.OrderByDescending(x => x.Naziv);
                    break;
                case SortTypes.Ocena:
                    smestajs = smestajs.OrderBy(x => x.Ocena);
                    break;
                case SortTypes.OcenaDesc:
                    smestajs = smestajs.OrderByDescending(x => x.Ocena);
                    break;

            }

            ViewBag.sortTypes = new SelectList(SortTypeDict, "Key", "Key", sortType);
            ViewBag.CurrentSortType = sortType;

            return View(smestajs.ToPagedList(page, elementsPerPage));
        }

        // GET: Smestajs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Smestaj smestaj = db.Smestajs.Find(id);
            if (smestaj == null)
            {
                return HttpNotFound();
            }
            return View(smestaj);
        }

        // GET: Smestajs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Smestajs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Naziv,Opis,Adresa,Ocena")] Smestaj smestaj)
        {
            if (ModelState.IsValid)
            {
                db.Smestajs.Add(smestaj);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(smestaj);
        }

        // GET: Smestajs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Smestaj smestaj = db.Smestajs.Find(id);
            if (smestaj == null)
            {
                return HttpNotFound();
            }
            return View(smestaj);
        }

        // POST: Smestajs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Naziv,Opis,Adresa,Ocena")] Smestaj smestaj)
        {
            if (ModelState.IsValid)
            {
                db.Entry(smestaj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(smestaj);
        }

        // GET: Smestajs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Smestaj smestaj = db.Smestajs.Find(id);
            if (smestaj == null)
            {
                return HttpNotFound();
            }
            return View(smestaj);
        }

        // POST: Smestajs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Smestaj smestaj = db.Smestajs.Find(id);
            db.Smestajs.Remove(smestaj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Filter(SmestajFilter filter)
        {
            IQueryable<Smestaj> smestajs = db.Smestajs;

            if (!filter.SmestajAdresa.IsNullOrWhiteSpace())
            {
                smestajs = smestajs.Where(s => s.Adresa.Contains(filter.SmestajAdresa));
            }

            if (!filter.SmestajNaziv.IsNullOrWhiteSpace())
            {
                smestajs = smestajs.Where(s => s.Naziv.Contains(filter.SmestajNaziv));
            }

            return View(smestajs.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
