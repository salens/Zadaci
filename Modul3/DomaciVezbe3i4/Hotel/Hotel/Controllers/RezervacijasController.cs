﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hotel.Models;
using PagedList;

namespace Hotel.Controllers
{
    public class RezervacijasController : Controller
    {
        private HotelContext db = new HotelContext();


        public enum SortTypes
        {
            BrojKreveta,
            BrojKrevetaDesc,
            CenaNocenja,
            CenaNocenjaDesc
        };

        public static Dictionary<string, SortTypes> SortTypeDict = new Dictionary<string, SortTypes>
        {
            {"BrojKreveta" , SortTypes.BrojKreveta },
            {"BrojKreveta Descending" , SortTypes.BrojKrevetaDesc },
            {"CenaNocenja" , SortTypes.CenaNocenja },
            {"CenaNocenja Descending" , SortTypes.CenaNocenjaDesc }

        };

        private int elementsPerPage = 3;

        // GET: Rezervacijas
        public ActionResult Index(string sortType = "BrojKreveta", int page = 1)
        {
            SortTypes sortBy = SortTypeDict[sortType];

            var rezervacijas = db.Rezervacijas.Include(r => r.Soba).Where(x=>x.Otkazana == false);

            switch (sortBy)
            {
                case SortTypes.BrojKreveta:
                    rezervacijas = rezervacijas.OrderBy(x => x.Soba.BrojKreveta);
                    break;
                case SortTypes.BrojKrevetaDesc:
                    rezervacijas = rezervacijas.OrderByDescending(x => x.Soba.BrojKreveta);
                    break;
                case SortTypes.CenaNocenja:
                    rezervacijas = rezervacijas.OrderBy(x => x.Soba.CenaPoNocenju);
                    break;
                case SortTypes.CenaNocenjaDesc:
                    rezervacijas = rezervacijas.OrderByDescending(x => x.Soba.CenaPoNocenju);
                    break;

            }

            ViewBag.sortTypes = new SelectList(SortTypeDict, "Key", "Key", sortType);
            ViewBag.CurrentSortType = sortType;

            //Iz filtera ako kriterijum ne postoji u bazi vraca poruku da se trazi po drugom kriterijumu
            if(TempData["NemaZadatogKriterijuma"] !=null)
            ViewBag.NemaZadatogKriterijuma = TempData["NemaZadatogKriterijuma"].ToString();


            return View(rezervacijas.ToPagedList(page, elementsPerPage));
        }

        // GET: Rezervacijas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacijas.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        // GET: Rezervacijas/Create
        public ActionResult Create()
        {
            ViewBag.SobaId = new SelectList(db.Sobas, "Id", "Id");
            return View();
        }

        // POST: Rezervacijas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ImeIPrezime,DatumOd,DatumDo,Otkazana,SobaId")] Rezervacija rezervacija)
        {
            if (ModelState.IsValid)
            {
                var proveraDaliPostoji = db.Rezervacijas.ToList().Where(x => x.DatumOd == rezervacija.DatumOd && x.SobaId == rezervacija.SobaId && x.Otkazana != true || x.DatumDo == rezervacija.DatumDo && x.SobaId == rezervacija.SobaId && x.Otkazana != true).FirstOrDefault();
                if(proveraDaliPostoji != null)
                {
                    ViewBag.SobaId = new SelectList(db.Sobas, "Id", "Id", rezervacija.SobaId);
                    ViewBag.SobaJeZauzeta = "Soba sa ID: " + rezervacija.SobaId +" je zauzeta u tom terminu!Izaberite drugi termin.";
                    return View(rezervacija);
                }
                else
                {
                    db.Rezervacijas.Add(rezervacija);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
              
            }

            ViewBag.SobaId = new SelectList(db.Sobas, "Id", "Id", rezervacija.SobaId);
            return View(rezervacija);
        }

        // GET: Rezervacijas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacijas.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            ViewBag.SobaId = new SelectList(db.Sobas, "Id", "Id", rezervacija.SobaId);
            return View(rezervacija);
        }

        // POST: Rezervacijas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ImeIPrezime,DatumOd,DatumDo,Otkazana,SobaId")] Rezervacija rezervacija)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rezervacija).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SobaId = new SelectList(db.Sobas, "Id", "Id", rezervacija.SobaId);
            return View(rezervacija);
        }

        // GET: Rezervacijas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacijas.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        // POST: Rezervacijas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rezervacija rezervacija = db.Rezervacijas.Find(id);
            db.Rezervacijas.Remove(rezervacija);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        // GET: Rezervacijas/Edit/5
        public ActionResult Otkazivanje(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacijas.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            ViewBag.SobaId = new SelectList(db.Sobas, "Id", "Id", rezervacija.SobaId);
            return View(rezervacija);
        }

        // POST: Rezervacijas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Otkazivanje(Rezervacija rezervacija)
        {

            if (rezervacija.Otkazana == true)
            {
                var result = db.Rezervacijas.SingleOrDefault(b => b.Id == rezervacija.Id);
                if (result != null)
                {
                    result.Otkazana = rezervacija.Otkazana;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ViewBag.SobaId = new SelectList(db.Sobas, "Id", "Id", rezervacija.SobaId);
            return View(rezervacija);
        }

        [HttpPost]
        public ActionResult Filter(RezervacijaFilter filter)
        {
            IQueryable<Rezervacija> rezervacijas = db.Rezervacijas;

            if (filter.DatumOd != null && filter.DatumOd != DateTime.MinValue)
            {
                rezervacijas = rezervacijas.Where(p => p.DatumOd == filter.DatumOd);
            }

            if (filter.DatumDo != null && filter.DatumDo != DateTime.MinValue)
            {
                rezervacijas = rezervacijas.Where(p => p.DatumDo == filter.DatumDo);
            }

            if (filter.SobaId != null)
            {
                rezervacijas = rezervacijas.Where(p => p.SobaId == filter.SobaId);
            }

            var rezervacijeKojeNisuOtkazane = rezervacijas.Where(x => x.Otkazana == false); // ako nisu soft deleted

            if(rezervacijeKojeNisuOtkazane.Any()) // ukoliko ih ima da nisu otkazane u listi vraca view sa listom
            {
                return View(rezervacijeKojeNisuOtkazane.ToList());
            }
            TempData["NemaZadatogKriterijuma"] = "Izaberite drugacije vrednosti u filteru, padaci po ovom kriterijumu ne postoje u bazi!";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
