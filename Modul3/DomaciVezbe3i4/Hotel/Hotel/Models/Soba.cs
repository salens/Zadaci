﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace Hotel.Models
{
    public class Soba
    {
        public int Id { get; set; }

        [Required]
        public int BrojSobe { get; set; }
        [Required]
        public int BrojKreveta { get; set; }
        [Required]
        public int CenaPoNocenju { get; set; }


        public int SmestajId { get; set; }

        [ForeignKey("SmestajId")]
        public Smestaj Smestaj { get; set; }


        public List<Rezervacija> Rezervacije { get; set; }
    }
}