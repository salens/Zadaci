﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class HotelContext : DbContext
    {
        public DbSet<Soba> Sobas { get; set; }
        public DbSet<Smestaj> Smestajs { get; set; }
        public DbSet<Rezervacija> Rezervacijas { get; set; }

        public HotelContext():base("name=HotelContext")
        {

        }
    }
}