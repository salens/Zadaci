﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class SmestajFilter
    {
        public string SmestajAdresa { get; set; }
        public string SmestajNaziv { get; set; }
    }
}