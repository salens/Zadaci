﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace Hotel.Models
{
    public class Smestaj
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Naziv { get; set; }

        [Required]
        [StringLength(180)]
        public string Opis { get; set; }

        [Required]
        [StringLength(140)]
        public string Adresa { get; set; }

        [Required]
        [Range(1, 5)]
        public int Ocena { get; set; }

        public List<Soba> Sobe { get; set; }
    }
}