﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class SobaFilter
    {
        public int? BrojKreveta { get; set; }
        public int? CenaNocenja { get; set; }
        public string SmestajNaziv { get; set; }

    }
}