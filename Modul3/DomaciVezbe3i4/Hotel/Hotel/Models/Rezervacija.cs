﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace Hotel.Models
{
    public class Rezervacija
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string ImeIPrezime { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DatumOd { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DatumDo { get; set; }
        public bool Otkazana { get; set; }


        public int SobaId { get; set; }

        [ForeignKey("SobaId")]
        public Soba Soba { get; set; }
    }
}