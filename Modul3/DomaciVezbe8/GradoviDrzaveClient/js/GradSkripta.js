﻿$(document).ready(function(){

	// podaci
	var host = "http://localhost:";
	var port = "2759/";
	var gradEndpoint = "api/grads/";
	var drzavaEndpoint = "api/drzavas/";
	var formAction = "Create";
	var editingId;

	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDeleteGrad", deleteGrad);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEditGrad", editGrad);
	
	// prikaz grad
	$("#btnGrads").click(function(){
		// ucitavanje gradova
		var requestUrl = host + port + gradEndpoint;
		var requestUrlDrzavas = host + port + drzavaEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setGrads);
		$.getJSON(requestUrlDrzavas, setDrzavas);
	});
	
	// prikaz gradova od do broja stanovnika
	$("#btnGradsOdDoStanovnika").click(function(){
		
		// OD DO Stanovnika
		var stanovnikaOd = "?brojStanovnikaOd=" +  $("#stanovnikaOd").val(); 
		var stanovnikaDo = "&brojStanovnikaDo=" +  $("#stanovnikaDo").val();
	
		// ucitavanje gradova
		var requestUrlStanovnici = host + port + gradEndpoint + stanovnikaOd + stanovnikaDo;
		var requestUrlDrzavas = host + port + drzavaEndpoint;
		console.log("URL zahteva: " + requestUrlStanovnici);
		$.getJSON(requestUrlStanovnici, setGrads);
	});
	
	
	function setGrads(data, status){
		console.log("Status:" + status);
		
		var $container = $("#dataGrads");
		$container.empty();
		if(status == "success")
		{
				console.log(data);
				// ispis gradova
				
				var div = $("<div></div>");
				var h1  = $("<h1>Gradovi</h1>");
				div.append(h1);
				
				// ispis tabele
				var table = $("<table class='table table-hover table-bordered'></table>");
				var header = $("<tr><td>Id</td><td>Ime Grada</td><td>Grad Postanski Kod</td><td>Grad Broj Stanovnika</td><td>Drzava Ime</td><td>Delete</td><td>Edit</td></tr>");
				table.append(header);
			
				for(i=0; i < data.length; i++)
				{
					// prikazujemo novi red u tabeli
					var row = "<tr>";
					// prikaz podataka
					var displayData = "<td>" + data[i].Id + "</td>" + "<td>" + data[i].Ime + "</td>" + "<td>" + data[i].PostanskiKod + "</td>" + "<td>" + data[i].BrojStanovnika + "</td>" + "<td>" + data[i].Drzava.Ime + "</td>";
					// prikaz dugmadi za izmenu i brisanje
					var stringId = data[i].Id.toString();
					var displayDelete = "<td><button class='btn-danger' id=btnDeleteGrad name=" + stringId + ">Delete</button></td>";
					var displayEdit = "<td><button class='btn-primary' id=btnEditGrad name=" + stringId + ">Edit</button></td>";
					row += displayData + displayDelete + displayEdit + "</tr>";  
					table.append(row);
					newId = data[i].Id;
					
					console.log(data);
				}
			
				div.append(table);

				// prikaz forme
				$("#formDiv2").css("display","block");

				// ispis novog sadrzaja
				$container.append(div);
					
			}
			else 
			{
				var div = $("<div></div>");
				var h1 = $("<h1>Greška prilikom preuzimanja Gradova!</h1>");
				div.append(h1);
				$container.append(div);
			}
	};
	
	function setDrzavas(data, status){
		console.log("Status:" + status);
		
		var $container = $("#data");
		$('#drzavaId').empty();
		if(status == "success")
		{
				console.log(data);
				// ispis Drzava
									
				for(i=0; i < data.length; i++)
				{
					$('#drzavaId').append('<option value="' + data[i].Id + '">' + data[i].Ime + '</option>');
					console.log(data);
				}			
					
			}
			else 
			{
				var h1 = $("<h1>Greška prilikom preuzimanja Drzave!</h1>");
			}
	};
	
	// dodavanje novog grada
	$("#gradForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var gradIme = $("#gradIme").val();
		var gradPostanskiKod = $("#gradPostanskiKod").val();
		var gradBrojStanovnika = $("#gradBrojStanovnika").val();
		var drzavaId = $("#drzavaId option:selected").val();
		
		console.log(drzavaId);
		
		var gradIme = $("#gradIme").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + gradEndpoint;
			sendData = {
				"Ime": gradIme,
				"PostanskiKod" : gradPostanskiKod,
				"BrojStanovnika" : gradBrojStanovnika,
			    "DrzavaId" : drzavaId
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + gradEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Ime": gradIme,
				"PostanskiKod" : gradPostanskiKod,
				"BrojStanovnika" : gradBrojStanovnika,
				"DrzavaId" : drzavaId				
			};
		}	

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});
	
	// Brisanje Grada
	function deleteGrad() {
		
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + gradEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};
	
	// Izmena Grada
	function editGrad(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tog grada
		$.ajax({
			url: host + port + gradEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#gradIme").val(data.Ime);
			$("#gradPostanskiKod").val(data.PostanskiKod);
			$("#gradBrojStanovnika").val(data.BrojStanovnika);
			$("#drzavaId").val(data.DrzavaId);
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};	
	
	// osvezi prikaz tabele
	function refreshTable() {
		// cistim formu
		$("#gradIme").val('');
		$("#gradPostanskiKod").val('');
		$("#gradBrojStanovnika").val('');
		$("#drzavaId").val('');
		
		$("#drzavaIme").val('');
		// osvezavam
		$("#btnGrads").trigger("click");
	};
	
});