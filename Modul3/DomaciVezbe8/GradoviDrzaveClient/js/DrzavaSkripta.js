﻿$(document).ready(function(){

	// podaci
	var host = "http://localhost:";
	var port = "2759/";
	var drzavaEndpoint = "api/drzavas/";
	var formAction = "Create";
	var editingId;
	
	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDelete", deleteDrzava);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEdit", editDrzava);
	
	// prikaz drzava
	$("#btnDrzavas").click(function(){
		// ucitavanje drzava
		var requestUrl = host + port + drzavaEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setDrzavas);
	});
	
	function setDrzavas(data, status){
		console.log("Status:" + status);
		
		var $container = $("#data");
		$container.empty();
	    $('#drzavaId').empty();
		if(status == "success")
		{
				console.log(data);
				// ispis Drzava
				
				var div = $("<div></div>");
				var h1  = $("<h1>Drzave</h1>");
				div.append(h1);
				
				// ispis tabele
				var table = $("<table class='table table-hover table-bordered'></table>");
				var header = $("<tr><td>Id</td><td>Ime Drzave</td><td>Internacionalni Kod</td><td>Delete</td><td>Edit</td></tr>");
				table.append(header);
			
				for(i=0; i < data.length; i++)
				{
					// prikazujemo novi red u tabeli
					var row = "<tr>";
					// prikaz podataka
					var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Ime + "</td>" + "</td><td>" + data[i].InternacionalniKod + "</td>";
					// prikaz dugmadi za izmenu i brisanje
					var stringId = data[i].Id.toString();
					var displayDelete = "<td><button class='btn-danger' id=btnDelete name=" + stringId + ">Delete</button></td>";
					var displayEdit = "<td><button class='btn-primary' id=btnEdit name=" + stringId + ">Edit</button></td>";
					row += displayData + displayDelete + displayEdit + "</tr>";  
					table.append(row);
					newId = data[i].Id;
				
					$('#drzavaId').append('<option value="' + data[i].Id + '">' + data[i].Ime + '</option>');
					console.log(data);
				}
			
				div.append(table);

				// prikaz forme
				$("#formDiv").css("display","block");

				// ispis novog sadrzaja
				$container.append(div);
					
			}
			else 
			{
				var div = $("<div></div>");
				var h1 = $("<h1>Greška prilikom preuzimanja Drzave!</h1>");
				div.append(h1);
				$container.append(div);
			}
	};
	
	// dodavanje nove drzave
	$("#drzavaForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var drzavaIme = $("#drzavaIme").val();
		var InternacionalniKod = $("#InternacionalniKod").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + drzavaEndpoint;
			sendData = {
				"Ime": drzavaIme,
				"InternacionalniKod" : InternacionalniKod
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + drzavaEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Ime": drzavaIme,
				"InternacionalniKod" : InternacionalniKod			
			};
		}	

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});
	
	// Brisanje Drzava
	function deleteDrzava() {
		
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + drzavaEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};
	
	// Izmena Drzava
	function editDrzava(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo te drzave
		$.ajax({
			url: host + port + drzavaEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#drzavaIme").val(data.Ime);
			$("#InternacionalniKod").val(data.InternacionalniKod);
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};	
	
	// osvezi prikaz tabele
	function refreshTable() {
		// cistim formu
		$("#drzavaIme").val('');
		$("#InternacionalniKod").val('');

		// osvezavam
		$("#btnDrzavas").trigger("click");
	};
	
});