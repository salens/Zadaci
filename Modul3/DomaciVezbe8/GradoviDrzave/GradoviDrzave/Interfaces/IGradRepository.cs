﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GradoviDrzave.Models;
namespace GradoviDrzave.Interfaces
{
    public interface IGradRepository
    {
        IEnumerable<Grad> GetAll();
        IEnumerable<Grad> GetAllBetweenStanovnici(int brojStanovnikaOd, int brojStanovnikaDo);
        Grad GetById(int id);
        void Add(Grad grad);
        void Update(Grad grad);
        void Delete(Grad grad);
    }
}
