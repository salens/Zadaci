﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity;
using GradoviDrzave.Repository;
using GradoviDrzave.Resolver;
using GradoviDrzave.Models;
using GradoviDrzave.Interfaces;
using Unity.Lifetime;
using System.Web.Http.Cors;

namespace GradoviDrzave
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // CORS
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Unity Grad
            var container = new UnityContainer();
            container.RegisterType<IGradRepository, GradRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            // Unity Drzava
            container.RegisterType<IDrzavaRepository, DrzavaRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);
        }
    }
}
