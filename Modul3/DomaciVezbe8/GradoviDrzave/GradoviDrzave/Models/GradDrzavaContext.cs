﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace GradoviDrzave.Models
{
    public class GradDrzavaContext : DbContext
    {
        public GradDrzavaContext() : base("name=GradDrzavaContext")
        {
        }

        public DbSet<Grad> Grads { get; set; }
        public DbSet<Drzava> Drzavas { get; set; }

    }
}