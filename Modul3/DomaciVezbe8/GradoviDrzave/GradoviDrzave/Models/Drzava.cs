﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GradoviDrzave.Models
{
    public class Drzava
    {
        public int Id { get; set; }
        [Required]
        public string Ime { get; set; }
        public int InternacionalniKod { get; set; }

    }
}