namespace GradoviDrzave.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Drzavas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ime = c.String(nullable: false),
                        InternacionalniKod = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Grads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ime = c.String(nullable: false),
                        PostanskiKod = c.Int(nullable: false),
                        BrojStanovnika = c.Int(nullable: false),
                        DrzavaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drzavas", t => t.DrzavaId, cascadeDelete: true)
                .Index(t => t.DrzavaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Grads", "DrzavaId", "dbo.Drzavas");
            DropIndex("dbo.Grads", new[] { "DrzavaId" });
            DropTable("dbo.Grads");
            DropTable("dbo.Drzavas");
        }
    }
}
