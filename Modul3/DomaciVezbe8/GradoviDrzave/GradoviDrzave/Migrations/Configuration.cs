namespace GradoviDrzave.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using GradoviDrzave.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<GradoviDrzave.Models.GradDrzavaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GradoviDrzave.Models.GradDrzavaContext context)
        {

            context.Drzavas.AddOrUpdate(x => x.Id,
                new Drzava() { Id = 1, Ime = "Srbija", InternacionalniKod = 123 },
                new Drzava() { Id = 2, Ime = "USA", InternacionalniKod = 1234 },
                new Drzava() { Id = 3, Ime = "UK", InternacionalniKod = 1235 }
                );

            context.Grads.AddOrUpdate(x => x.Id,
                new Grad()
                {
                    Id = 1,
                    Ime = "Beograd",
                    PostanskiKod = 11000,
                    BrojStanovnika = 9034,
                    DrzavaId = 1
                },
                new Grad()
                {
                    Id = 2,
                    Ime = "London",
                    PostanskiKod = 2713,
                    BrojStanovnika = 634434,
                    DrzavaId = 3
                },
                new Grad()
                {
                    Id = 3,
                    Ime = "Novi Sad",
                    PostanskiKod = 21000,
                    BrojStanovnika = 4034,
                    DrzavaId = 1
                },
                new Grad()
                {
                    Id = 4,
                    Ime = "New York",
                    PostanskiKod = 1813,
                    BrojStanovnika = 32343,
                    DrzavaId = 2
                }
                );
        }
    }
}
