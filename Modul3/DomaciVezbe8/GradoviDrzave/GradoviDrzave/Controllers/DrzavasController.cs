﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradoviDrzave.Models;
using GradoviDrzave.Repository;
using GradoviDrzave.Interfaces;

namespace GradoviDrzave.Controllers
{
    public class DrzavasController : ApiController
    {
        public IDrzavaRepository _repository { get; set; }


        public DrzavasController(IDrzavaRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Drzava> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var drzava = _repository.GetById(id);
            if (drzava == null)
            {
                return NotFound();
            }
            return Ok(drzava);
        }

        public IHttpActionResult Post(Drzava drzava)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(drzava);
            return CreatedAtRoute("DefaultApi", new { id = drzava.Id }, drzava);
        }

        public IHttpActionResult Put(int id, Drzava drzava)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != drzava.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(drzava);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(drzava);
        }

        public IHttpActionResult Delete(int id)
        {
            var drzava = _repository.GetById(id);
            if (drzava == null)
            {
                return NotFound();
            }

            _repository.Delete(drzava);
            return Ok();
        }
    }
}
