﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradoviDrzave.Models;
using GradoviDrzave.Repository;
using GradoviDrzave.Interfaces;

namespace GradoviDrzave.Controllers
{
    public class GradsController : ApiController
    {
        public IGradRepository _repository { get; set; }


        public GradsController(IGradRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Grad> Get()
        {
            return _repository.GetAll();
        }

        public IEnumerable<Grad> Get(int brojStanovnikaOd, int brojStanovnikaDo)
        {
            return _repository.GetAllBetweenStanovnici(brojStanovnikaOd, brojStanovnikaDo);
        }

        public IHttpActionResult Get(int id)
        {
            var grad = _repository.GetById(id);
            if (grad == null)
            {
                return NotFound();
            }
            return Ok(grad);
        }

        public IHttpActionResult Post(Grad grad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(grad);
            return CreatedAtRoute("DefaultApi", new { id = grad.Id }, grad);
        }

        public IHttpActionResult Put(int id, Grad grad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != grad.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(grad);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(grad);
        }

        public IHttpActionResult Delete(int id)
        {
            var grad = _repository.GetById(id);
            if (grad == null)
            {
                return NotFound();
            }

            _repository.Delete(grad);
            return Ok();
        }
    }
}
