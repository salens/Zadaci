﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using GradoviDrzave.Models;
using GradoviDrzave.Interfaces;

namespace GradoviDrzave.Repository
{
    public class GradRepository : IDisposable, IGradRepository
    {
        private GradDrzavaContext db = new GradDrzavaContext();


        public IEnumerable<Grad> GetAll()
        {
            return db.Grads.Include(r => r.Drzava).OrderBy(x => x.PostanskiKod);
        }

        public IEnumerable<Grad> GetAllBetweenStanovnici(int brojStanovnikaOd, int brojStanovnikaDo)
        {
            return db.Grads.Include(r => r.Drzava).Where(x => x.BrojStanovnika >= brojStanovnikaOd).Where(x => x.BrojStanovnika <= brojStanovnikaDo);
        }

        public Grad GetById(int id)
        {
            return db.Grads.FirstOrDefault(p => p.Id == id);
        }

        public void Add(Grad grad)
        {
            db.Grads.Add(grad);
            db.SaveChanges();
        }

        public void Update(Grad grad)
        {
            db.Entry(grad).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Grad grad)
        {
            db.Grads.Remove(grad);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}