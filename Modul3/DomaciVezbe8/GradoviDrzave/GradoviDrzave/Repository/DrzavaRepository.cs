﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using GradoviDrzave.Models;
using GradoviDrzave.Interfaces;

namespace GradoviDrzave.Repository
{
    public class DrzavaRepository : IDisposable, IDrzavaRepository
    {
        private GradDrzavaContext db = new GradDrzavaContext();


        public IEnumerable<Drzava> GetAll()
        {
            return db.Drzavas;
        }

        public Drzava GetById(int id)
        {
            return db.Drzavas.FirstOrDefault(p => p.Id == id);
        }

        public void Add(Drzava drzava)
        {
            db.Drzavas.Add(drzava);
            db.SaveChanges();
        }

        public void Update(Drzava drzava)
        {
            db.Entry(drzava).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Drzava drzava)
        {
            db.Drzavas.Remove(drzava);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}