namespace SalonAutomobila.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSalonCarProizvodjac : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Automobil",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(nullable: false, maxLength: 70),
                        GodinaProizvodnje = c.Int(nullable: false),
                        Kubikaza = c.Int(nullable: false),
                        Boja = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Proizvodjac",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(nullable: false, maxLength: 70),
                        Drzava = c.String(nullable: false, maxLength: 50),
                        Grad = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Salon",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PIB = c.Int(nullable: false),
                        Naziv = c.String(nullable: false, maxLength: 70),
                        Drzava = c.String(nullable: false, maxLength: 50),
                        Grad = c.String(nullable: false, maxLength: 50),
                        Adresa = c.String(nullable: false, maxLength: 80),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Salon");
            DropTable("dbo.Proizvodjac");
            DropTable("dbo.Automobil");
        }
    }
}
