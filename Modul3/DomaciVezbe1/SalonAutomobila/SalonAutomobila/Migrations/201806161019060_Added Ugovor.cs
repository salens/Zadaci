namespace SalonAutomobila.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUgovor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ugovor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProizvodjacId = c.Int(nullable: false),
                        SalonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Proizvodjac", t => t.ProizvodjacId, cascadeDelete: true)
                .ForeignKey("dbo.Salon", t => t.SalonId, cascadeDelete: true)
                .Index(t => t.ProizvodjacId)
                .Index(t => t.SalonId);
            
            AddColumn("dbo.Automobil", "ProizvodjacId", c => c.Int(nullable: false));
            AddColumn("dbo.Automobil", "SalonId", c => c.Int(nullable: false));
            CreateIndex("dbo.Automobil", "ProizvodjacId");
            CreateIndex("dbo.Automobil", "SalonId");
            AddForeignKey("dbo.Automobil", "ProizvodjacId", "dbo.Proizvodjac", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Automobil", "SalonId", "dbo.Salon", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ugovor", "SalonId", "dbo.Salon");
            DropForeignKey("dbo.Automobil", "SalonId", "dbo.Salon");
            DropForeignKey("dbo.Ugovor", "ProizvodjacId", "dbo.Proizvodjac");
            DropForeignKey("dbo.Automobil", "ProizvodjacId", "dbo.Proizvodjac");
            DropIndex("dbo.Ugovor", new[] { "SalonId" });
            DropIndex("dbo.Ugovor", new[] { "ProizvodjacId" });
            DropIndex("dbo.Automobil", new[] { "SalonId" });
            DropIndex("dbo.Automobil", new[] { "ProizvodjacId" });
            DropColumn("dbo.Automobil", "SalonId");
            DropColumn("dbo.Automobil", "ProizvodjacId");
            DropTable("dbo.Ugovor");
        }
    }
}
