﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    [Table("Proizvodjac")]
    public class Proizvodjac
    {
        public int Id { get; set; }

        [Required]
        [StringLength(70)]
        public string Naziv { get; set; }

        [Required]
        [StringLength(50)]
        public string Drzava { get; set; }

        [Required]
        [StringLength(50)]
        public string Grad { get; set; }


        public List<Automobil> Automobili { get; set; }

        public List<Ugovor> Ugovori { get; set; }

    }
}