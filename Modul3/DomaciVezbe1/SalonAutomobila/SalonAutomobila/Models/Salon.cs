﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    //za table atribut treba using System.ComponentModel.DataAnnotations.Schema

    [Table("Salon")]
    public class Salon
    {       
        public int Id { get; set; } // ili CarId - nije bitna konvencija CaseSensitive bice automatski identity po konvenciji i ovo ce biti privatni kljuc

        [Required]
        public int PIB { get; set; }

        [Required]
        [StringLength(70)]
        public string Naziv { get; set; }

        [Required]
        [StringLength(50)]
        public string Drzava { get; set; }

        [Required]
        [StringLength(50)]
        public string Grad { get; set; }

        [Required]
        [StringLength(80)]
        public string Adresa { get; set; }

        public List<Automobil> Automobili { get; set; }

        public List<Ugovor> Ugovori { get; set; }
    }
}