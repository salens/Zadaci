﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    [Table("Automobil")]
    public class Automobil
    {
        public int Id { get; set; }

        [Required]
        [StringLength(70)]
        public string Model { get; set; }

        [Required]
        [Range(1950, int.MaxValue)]
        public int GodinaProizvodnje { get; set; }

        [Required]
        [Range(750, 7000)]
        public int Kubikaza { get; set; }

        [Required]
        [StringLength(20)]
        public string Boja { get; set; }

        public int ProizvodjacId { get; set; }
        [ForeignKey("ProizvodjacId")] // ovde pisemo od klase u ovom slucaju "Engine" kao sto je dole, ako namapiramo na klasu a ne na Id onda naziv ID-ja ovog ispod
        [Column("ProizvodjacName")]
        public Proizvodjac Proizvodjac { get; set; }

        public int SalonId { get; set; }
        [ForeignKey("SalonId")] // ovde pisemo od klase u ovom slucaju "Engine" kao sto je dole, ako namapiramo na klasu a ne na Id onda naziv ID-ja ovog ispod
        [Column("SalonName")]
        public Salon Salon { get; set; }
    }
}