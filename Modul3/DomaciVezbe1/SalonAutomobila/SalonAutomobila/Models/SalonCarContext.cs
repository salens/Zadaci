﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    public class SalonCarContext : DbContext
    {
        public DbSet<Salon> Salons { get; set; }
        public DbSet<Automobil> Atomobils { get; set; }
        public DbSet<Proizvodjac> Proizvodjacs { get; set; }
        public DbSet<Ugovor> Ugovors { get; set; }

        public SalonCarContext():base("name=SalonCarContext")
        {
        }
    }
}