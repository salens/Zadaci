﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SalonAutomobila.Models
{
    [Table("Ugovor")]
    public class Ugovor
    {
        public int Id { get; set; }

        public int ProizvodjacId { get; set; }
        [ForeignKey("ProizvodjacId")] // ovde pisemo od klase u ovom slucaju "Engine" kao sto je dole, ako namapiramo na klasu a ne na Id onda naziv ID-ja ovog ispod
        [Column("ProizvodjacName")]
        public Proizvodjac Proizvodjac { get; set; }

        public int SalonId { get; set; }
        [ForeignKey("SalonId")] // ovde pisemo od klase u ovom slucaju "Engine" kao sto je dole, ako namapiramo na klasu a ne na Id onda naziv ID-ja ovog ispod
        [Column("SalonName")]
        public Salon Salon { get; set; }

    }
}