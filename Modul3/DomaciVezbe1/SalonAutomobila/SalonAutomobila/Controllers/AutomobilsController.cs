﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SalonAutomobila.Models;

namespace SalonAutomobila.Controllers
{
    public class AutomobilsController : Controller
    {
        private SalonCarContext db = new SalonCarContext();

        // GET: Automobils
        public ActionResult Index()
        {
            var atomobils = db.Atomobils.Include(a => a.Proizvodjac).Include(a => a.Salon);
            return View(atomobils.ToList());
        }

        // GET: Automobils/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Automobil automobil = db.Atomobils.Find(id);
            if (automobil == null)
            {
                return HttpNotFound();
            }
            ViewBag.Proizvodjaci = db.Proizvodjacs.Find(automobil.ProizvodjacId);
            ViewBag.Saloni = db.Salons.Find(automobil.ProizvodjacId);
            return View(automobil);
        }

        // GET: Automobils/Create
        public ActionResult Create()
        {
            ViewBag.Proizvodjaci = db.Proizvodjacs.ToList();
            ViewBag.Saloni = db.Salons.ToList();
            return View();
        }

        // POST: Automobils/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Automobil automobil)  // menjamo iz Form collection u Car objekat
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Atomobils.Add(automobil);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {

            }

            ViewBag.Proizvodjaci = db.Proizvodjacs.ToList();
            ViewBag.Saloni = db.Salons.ToList();
            
            return View(automobil);
        }

        // GET: Automobils/Edit/5
        public ActionResult Edit(int id) // brisemo znak ?
        {
            ViewBag.Proizvodjaci = db.Proizvodjacs.ToList();
            ViewBag.Saloni = db.Salons.ToList();

            return View(db.Atomobils.Find(id));
        }

        // POST: Automobils/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Automobil automobil)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry<Automobil>(automobil).State = EntityState.Modified; // ovde da ne bi dodavao novi entitet vec da izmeni taj odredjeni UPDATE
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {

            }

            ViewBag.Proizvodjaci = db.Proizvodjacs.ToList();
            ViewBag.Saloni = db.Salons.ToList();
            return View(automobil);
        }

        // GET: Automobils/Delete/5
        public ActionResult Delete(int id) // brisemo znak ?
        {
            var automobil = db.Atomobils // izvlacimo iz baze auto i njegovog proizvodjaca i u kom se salonu nalazi
                .Include(c => c.Proizvodjac)
                .Include(c => c.Salon)
                .Where(c => c.Id == id)
                .FirstOrDefault();

            return View(automobil);
        }

        // POST: Automobils/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var automobil = db.Atomobils.Find(id);
                if(automobil != null)
                {
                    db.Atomobils.Remove(automobil);
                    db.SaveChanges();
                  
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        
       
        }
    }
}
