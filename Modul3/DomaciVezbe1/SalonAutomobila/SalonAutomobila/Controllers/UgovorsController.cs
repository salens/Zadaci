﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SalonAutomobila.Models;

namespace SalonAutomobila.Controllers
{
    public class UgovorsController : Controller
    {
        private SalonCarContext db = new SalonCarContext();

        // GET: Ugovors
        public ActionResult Index()
        {
            var ugovors = db.Ugovors.Include(u => u.Proizvodjac).Include(u => u.Salon);
            return View(ugovors.ToList());
        }

        // GET: Ugovors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ugovor ugovor = db.Ugovors.Find(id);
            if (ugovor == null)
            {
                return HttpNotFound();
            }

            ViewBag.Proizvodjaci = db.Proizvodjacs.Find(ugovor.ProizvodjacId);
            ViewBag.Saloni = db.Salons.Find(ugovor.ProizvodjacId);
            return View(ugovor);
        }

        // GET: Ugovors/Create
        public ActionResult Create()
        {
            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv");
            ViewBag.SalonId = new SelectList(db.Salons, "Id", "Naziv");
            return View();
        }

        // POST: Ugovors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProizvodjacId,SalonId")] Ugovor ugovor)
        {
            if (ModelState.IsValid)
            {
                db.Ugovors.Add(ugovor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv", ugovor.ProizvodjacId);
            ViewBag.SalonId = new SelectList(db.Salons, "Id", "Naziv", ugovor.SalonId);
            return View(ugovor);
        }

        // GET: Ugovors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ugovor ugovor = db.Ugovors.Find(id);
            if (ugovor == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv", ugovor.ProizvodjacId);
            ViewBag.SalonId = new SelectList(db.Salons, "Id", "Naziv", ugovor.SalonId);
            return View(ugovor);
        }

        // POST: Ugovors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProizvodjacId,SalonId")] Ugovor ugovor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ugovor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv", ugovor.ProizvodjacId);
            ViewBag.SalonId = new SelectList(db.Salons, "Id", "Naziv", ugovor.SalonId);
            return View(ugovor);
        }

        // GET: Ugovors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ugovor ugovor = db.Ugovors.Find(id);

            if (ugovor == null)
            {
                return HttpNotFound();
            }

            ViewBag.Proizvodjaci = db.Proizvodjacs.Find(ugovor.ProizvodjacId);
            ViewBag.Saloni = db.Salons.Find(ugovor.ProizvodjacId);
            return View(ugovor);
        }

        // POST: Ugovors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ugovor ugovor = db.Ugovors.Find(id);
            db.Ugovors.Remove(ugovor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
