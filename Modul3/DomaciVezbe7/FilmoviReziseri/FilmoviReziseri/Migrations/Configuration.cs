namespace FilmoviReziseri.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using FilmoviReziseri.Models;
    internal sealed class Configuration : DbMigrationsConfiguration<FilmoviReziseri.Models.FilmReziserContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FilmoviReziseri.Models.FilmReziserContext context)
        {

            context.Rezisers.AddOrUpdate(x => x.Id,
                    new Reziser()
                    {
                        Id = 1,
                        Ime = "Jane",
                        Prezime = "Austen",
                        Starost = 23
                    },
                    new Reziser()
                    {
                        Id = 2,
                        Ime = "Charles",
                        Prezime = "Dickens",
                        Starost = 56
                    },
                    new Reziser()
                    {
                        Id = 3,
                        Ime = "Miguel",
                        Prezime = "de Cervantes",
                        Starost = 70
                    }
                );

            context.Films.AddOrUpdate(x => x.Id,
                    new Film()
                    {
                        Id = 1,
                        Ime = "Neka knjiga 1",
                        Zanr = "Comedija",
                        Godina = 2001,
                        ReziserId = 1
                    },
                    new Film()
                    {
                        Id = 2,
                        Ime = "Neka knjiha 2",
                        Zanr = "Sci FI",
                        Godina = 2004,
                        ReziserId = 1

                    },
                    new Film()
                    {
                        Id = 3,
                        Ime = "Neka knjiha 3",
                        Zanr = "Akcija",
                        Godina = 2011,
                        ReziserId = 2
                    },
                    new Film()
                    {
                        Id = 4,
                        Ime = "Neka knjiha 3",
                        Zanr = "Avantura",
                        Godina = 1998,
                        ReziserId = 3
                    }
                );
        }
    }
}
