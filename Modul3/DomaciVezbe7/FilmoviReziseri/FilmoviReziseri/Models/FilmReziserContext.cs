﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FilmoviReziseri.Models
{
    public class FilmReziserContext : DbContext
    {
        public FilmReziserContext() : base("name=FilmReziserContext")
        {
        }

        public DbSet<Film> Films { get; set; }
        public DbSet<Reziser> Rezisers { get; set; }

    }
}