﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FilmoviReziseri.Models
{
    public class Reziser
    {
        public int Id { get; set; }
        [Required]
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public int Starost { get; set; }

    }
}