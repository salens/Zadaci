﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FilmoviReziseri.Models
{
    public class Film
    {
        public int Id { get; set; }
        [Required]
        public string Ime { get; set; }
        public string Zanr { get; set; }
        public int Godina { get; set; }

        // Foreign Key
        public int ReziserId { get; set; }
        // Navigation property
        public Reziser Reziser { get; set; }
    }
}