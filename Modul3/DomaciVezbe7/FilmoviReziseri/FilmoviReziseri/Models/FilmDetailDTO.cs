﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmoviReziseri.Models
{
    public class FilmDetailDTO
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Zanr { get; set; }
        public int Godina { get; set; }
        public string ReziserIme { get; set; }

    }
}