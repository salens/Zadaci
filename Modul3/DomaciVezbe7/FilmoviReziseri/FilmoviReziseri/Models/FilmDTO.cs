﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmoviReziseri.Models
{
    public class FilmDTO
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string ReziserIme { get; set; }
    }
}