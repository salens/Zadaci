﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FilmoviReziseri.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web.Http.Tracing;

namespace FilmoviReziseri.Controllers
{
    public class FilmsController : ApiController
    {
        FilmReziserContext db = new FilmReziserContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET api/films
        public IQueryable<Film> GetFilms()
        {
            /* var films = from f in db.Films
                    select new FilmDTO()
                    {
                        Id = f.Id,
                        Ime = f.Ime,
                        ReziserIme = f.Reziser.Ime
                    };

            return films;*/


            Configuration.Services.GetTraceWriter().Info(
                Request,
                "FilmsController",
                "Dobavi sve filmove."
                );

            return db.Films.Include(r => r.Reziser);
        }
        /*[ResponseType(typeof(Film))]*/
        [ResponseType(typeof(FilmDetailDTO))]
        public IHttpActionResult GetFilm(int id)
        {
            /* Film film = db.Films.Find(id);

             if (film == null)
             {
                 return NotFound();
             }

             return Ok(film); */

            var film = db.Films.Include(r => r.Reziser).Select(f =>
              new FilmDetailDTO()
              {
                  Id = f.Id,
                  Ime = f.Ime,
                  Godina = f.Godina,
                  Zanr = f.Zanr,
                  ReziserIme = f.Reziser.Ime
              }).SingleOrDefault(f => f.Id == id);

            if (film == null)
            {
                return NotFound();
            }

            return Ok(film);
        }

        // POST api/films
        [ResponseType(typeof(Film))]
        public IHttpActionResult PostFilm(Film film)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Films.Add(film);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = film.Id }, film);
        }

        // Proverava da li postoji film u bazi

        private bool FilmExists(int id)
        {
            return db.Films.Count(e => e.Id == id) > 0;
        }

        // PUT api/films/1
        [ResponseType(typeof(Film))]
        public IHttpActionResult PutFilm(int id, Film film)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != film.Id)
            {
                return BadRequest();
            }

            db.Entry(film).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FilmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(film);
        }

        // DELETE api/films/1
        [ResponseType(typeof(Film))]
        public IHttpActionResult DeleteFilm(int id)
        {
            Film film = db.Films.Find(id);

            if (film == null)
            {
                return NotFound();
            }

            db.Films.Remove(film);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
