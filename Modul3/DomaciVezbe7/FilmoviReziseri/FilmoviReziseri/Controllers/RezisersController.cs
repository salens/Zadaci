﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FilmoviReziseri.Models;
using System.Data.Entity;

namespace FilmoviReziseri.Controllers
{
    public class RezisersController : ApiController
    {
        FilmReziserContext db = new FilmReziserContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET api/rezisers
        public IQueryable<Reziser> GetRezisers()
        {
            return db.Rezisers;
        }

        [ResponseType(typeof(Reziser))]
        public IHttpActionResult GetReziser(int id)
        {
            Reziser reziser = db.Rezisers.Find(id);

            if (reziser == null)
            {
                return NotFound();
            }

            return Ok(reziser);
        }

        // POST api/rezisers
        [ResponseType(typeof(Reziser))]
        public IHttpActionResult PostReziser(Reziser reziser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Rezisers.Add(reziser);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = reziser.Id }, reziser);
        }

        // Proverava da li postoji reziser u bazi
        private bool ReziserExists(int id)
        {
            return db.Rezisers.Count(e => e.Id == id) > 0;
        }

        // PUT api/rezisers/1
        [ResponseType(typeof(Reziser))]
        public IHttpActionResult PutReziser(int id, Reziser reziser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(id != reziser.Id)
            {
                return BadRequest();
            }

            db.Entry(reziser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                if (!ReziserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
                
            }

            return Ok(reziser);
        }

        // DELETE api/rezisers/1
        [ResponseType(typeof(Reziser))]
        public IHttpActionResult DeleteReziser(int id)
        {
            Reziser reziser = db.Rezisers.Find(id);

            if(reziser == null)
            {
                return NotFound();
            }

            db.Rezisers.Remove(reziser);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}