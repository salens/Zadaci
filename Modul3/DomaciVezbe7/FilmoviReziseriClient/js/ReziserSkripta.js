﻿$(document).ready(function(){

	// podaci
	var host = "http://localhost:";
	var port = "1133/";
	var reziserEndpoint = "api/rezisers/";
	var formAction = "Create";
	var editingId;
	
	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDelete", deleteReziser);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEdit", editReziser);
	
	// prikaz rezisera
	$("#btnRezisers").click(function(){
		// ucitavanje rezisera
		var requestUrl = host + port + reziserEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setRezisers);
	});
	
	function setRezisers(data, status){
		console.log("Status:" + status);
		
		var $container = $("#data");
		$container.empty();
		if(status == "success")
		{
				console.log(data);
				// ispis Rezisera
				
				var div = $("<div></div>");
				var h1  = $("<h1>Reziseri</h1>");
				div.append(h1);
				
				// ispis tabele
				var table = $("<table class='table table-hover table-bordered'></table>");
				var header = $("<tr><td>Id</td><td>Ime</td><td>Prezime</td><td>Godine</td><td>Delete</td><td>Edit</td></tr>");
				table.append(header);
			
				for(i=0; i < data.length; i++)
				{
					// prikazujemo novi red u tabeli
					var row = "<tr>";
					// prikaz podataka
					var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Ime + "</td>" + "</td><td>" + data[i].Prezime + "</td>" + "</td><td>" + data[i].Starost + "</td>";
					// prikaz dugmadi za izmenu i brisanje
					var stringId = data[i].Id.toString();
					var displayDelete = "<td><button class='btn-danger' id=btnDelete name=" + stringId + ">Delete</button></td>";
					var displayEdit = "<td><button class='btn-primary' id=btnEdit name=" + stringId + ">Edit</button></td>";
					row += displayData + displayDelete + displayEdit + "</tr>";  
					table.append(row);
					newId = data[i].Id;
					
					$('#reziserId').append('<option value="' + data[i].Id + '">' + data[i].Ime + '</option>');
					console.log(data);
				}
			
				div.append(table);

				// prikaz forme
				$("#formDiv").css("display","block");

				// ispis novog sadrzaja
				$container.append(div);
					
			}
			else 
			{
				var div = $("<div></div>");
				var h1 = $("<h1>Greška prilikom preuzimanja Rezisera!</h1>");
				div.append(h1);
				$container.append(div);
			}
	};
	
	// dodavanje novog rezisera
	$("#reziserForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var reziserIme = $("#reziserIme").val();
		var reziserPrezime = $("#reziserPrezime").val();
		var reziserStarost = $("#reziserStarost").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + reziserEndpoint;
			sendData = {
				"Ime": reziserIme,
				"Prezime" : reziserPrezime,
				"Starost" : reziserStarost	
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + reziserEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Ime": reziserIme,
				"Prezime" : reziserPrezime,
				"Starost" : reziserStarost				
			};
		}	

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});
	
	// Brisanje Rezisera
	function deleteReziser() {
		
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + reziserEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};
	
	// Izmena Rezisera
	function editReziser(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tog rezisera
		$.ajax({
			url: host + port + reziserEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#reziserIme").val(data.Ime);
			$("#reziserPrezime").val(data.Prezime);
			$("#reziserStarost").val(data.Starost);
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};	
	
	// osvezi prikaz tabele
	function refreshTable() {
		// cistim formu
		$("#reziserIme").val('');
		$("#reziserPrezime").val('');
		$("#reziserStarost").val('');
		// osvezavam
		$("#btnRezisers").trigger("click");
	};
	
});