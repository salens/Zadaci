﻿$(document).ready(function(){

	// podaci
	var host = "http://localhost:";
	var port = "1133/";
	var filmEndpoint = "api/films/";
	var formAction = "Create";
	var editingId;
	
	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDeleteFilm", deleteFilm);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEditFilm", editFilm);
	
	// prikaz filma
	$("#btnFilms").click(function(){
		// ucitavanje rezisera
		var requestUrl = host + port + filmEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setFilms);
	});
	
	function setFilms(data, status){
		console.log("Status:" + status);
		
		var $container = $("#dataFilms");
		$container.empty();
		if(status == "success")
		{
				console.log(data);
				// ispis filmova
				
				var div = $("<div></div>");
				var h1  = $("<h1>Filmovi</h1>");
				div.append(h1);
				
				// ispis tabele
				var table = $("<table class='table table-hover table-bordered'></table>");
				var header = $("<tr><td>Id</td><td>Ime</td><td>Zanr</td><td>Godina</td><td>Reziser Ime</td><td>Delete</td><td>Edit</td></tr>");
				table.append(header);
			
				for(i=0; i < data.length; i++)
				{
					// prikazujemo novi red u tabeli
					var row = "<tr>";
					// prikaz podataka
					var displayData = "<td>" + data[i].Id + "</td>" + "<td>" + data[i].Ime + "</td>" + "<td>" + data[i].Zanr + "</td>" + "<td>" + data[i].Godina + "</td>" + "<td>" + data[i].Reziser.Ime + "</td>";
					// prikaz dugmadi za izmenu i brisanje
					var stringId = data[i].Id.toString();
					var displayDelete = "<td><button class='btn-danger' id=btnDeleteFilm name=" + stringId + ">Delete</button></td>";
					var displayEdit = "<td><button class='btn-primary' id=btnEditFilm name=" + stringId + ">Edit</button></td>";
					row += displayData + displayDelete + displayEdit + "</tr>";  
					table.append(row);
					newId = data[i].Id;
					
					console.log(data);
				}
			
				div.append(table);

				// prikaz forme
				$("#formDiv2").css("display","block");

				// ispis novog sadrzaja
				$container.append(div);
					
			}
			else 
			{
				var div = $("<div></div>");
				var h1 = $("<h1>Greška prilikom preuzimanja Filmova!</h1>");
				div.append(h1);
				$container.append(div);
			}
	};
	
	// dodavanje novog filma
	$("#filmForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var filmIme = $("#filmIme").val();
		var filmZanr = $("#filmZanr").val();
		var filmGodina = $("#filmGodina").val();
		var reziserId = $("#reziserId option:selected").val();
		
		console.log(reziserId);
		
		var reziserIme = $("#reziserIme").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + filmEndpoint;
			sendData = {
				"Ime": filmIme,
				"Zanr" : filmZanr,
				"Godina" : filmGodina,
			    "ReziserId" : reziserId
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + filmEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Ime": filmIme,
				"Zanr" : filmZanr,
				"Godina" : filmGodina,
				"ReziserId" : reziserId				
			};
		}	

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});
	
	// Brisanje Filma
	function deleteFilm() {
		
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + filmEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};
	
	// Izmena Filma
	function editFilm(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tog filma
		$.ajax({
			url: host + port + filmEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#filmIme").val(data.Ime);
			$("#filmZanr").val(data.Zanr);
			$("#filmGodina").val(data.Godina);
			$("#reziserId").val(data.ReziserId);
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};	
	
	// osvezi prikaz tabele
	function refreshTable() {
		// cistim formu
		$("#filmIme").val('');
		$("#filmZanr").val('');
		$("#filmGodina").val('');
		$("#reziserId").val('');
		
		$("#reziserIme").val('');
		// osvezavam
		$("#btnFilms").trigger("click");
	};
	
});